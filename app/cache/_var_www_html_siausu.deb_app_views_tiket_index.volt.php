<div class="row">
    <div class="col-xs-12">
        <div class="box box-success">
            <div class="box-header">
                <h3 class="box-title">Daftar Tiket</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
            <?php echo $this->getContent(); ?>
            <div class="box-body clearfix">
                <?php echo Phalcon\Tag::linkTo(array('tiket/new', 'Tambah', 'class' => 'btn btn-success btn-xs pull-right')); ?>
            </div>
                <div class="box-body table-responsive">
                <table class="table table-bordered table-hover">
                    <colgroup>
                        <col width="2%"></col>
                        <col width="80%"></col>
                        <col width="10%"></col>
                    </colgroup>
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Nama Tiket</th>
                            <th>Proses</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if (isset($page->items)) { ?>
                        <?php foreach ($page->items as $tiket) { ?>
                            <?php $counter += 1; ?>
                            <tr>
                                <td><?php echo $counter; ?></td>
                                <td><?php echo $tiket->getNamaTiket(); ?></td>
                                <td align="center">
                                    <?php echo Phalcon\Tag::linkTo(array('tiket/detail/' . $tiket->getIdTiket(), '<i class="fa fa-fw fa-list-alt"></i>', 'data-toggle' => 'tooltip', 'title' => 'Detail', 'class' => 'btn btn-xs btn-warning')); ?> 

                                    <?php echo Phalcon\Tag::linkTo(array('tiket/edit/' . $tiket->getIdTiket(), '<i class="fa fa-fw fa-edit"></i>', 'data-toggle' => 'tooltip', 'title' => 'Edit', 'class' => 'btn btn-xs btn-primary')); ?> 

                                    <?php echo Phalcon\Tag::linkTo(array('tiket/delete/' . $tiket->getIdTiket(), '<i class="fa fa-fw fa-minus-circle"></i>', 'data-toggle' => 'tooltip', 'title' => 'Delete', 'class' => 'btn btn-xs btn-danger', 'onclick' => 'javascript:if(confirm(\'Anda yakin menghapus item ini?\')==false)return false;')); ?>
                                </td>
                            </tr>
                        <?php } ?>
                        <?php } ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="3" align="center">
                            <?php echo Phalcon\Tag::linkTo(array('jurusan/', 'First')); ?> | 
                            <?php echo Phalcon\Tag::linkTo(array('jurusan/?page=' . $page->before, 'Previous')); ?> | 
                            <?php echo Phalcon\Tag::linkTo(array('jurusan/?page=' . $page->next, 'Next')); ?> | 
                            <?php echo Phalcon\Tag::linkTo(array('jurusan/?page=' . $page->last, 'Last')); ?> | 
                            <?php echo $page->current . '/' . $page->total_pages; ?>
                            </td>
                        </tr>
                    </tfoot>
                </table>
                </div>
                
            </div>
        </div>
    </div>
</div>