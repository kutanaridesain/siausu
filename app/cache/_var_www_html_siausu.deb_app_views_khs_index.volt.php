
<?php echo $this->getContent(); ?>

<div align="right">
    <?php echo Phalcon\Tag::linkTo(array('khs/new', 'Create khs')); ?>
</div>

<?php echo Phalcon\Tag::form(array('khs/search', 'method' => 'post', 'autocomplete' => 'off')); ?>

<div align="center">
    <h1>Search khs</h1>
</div>

<table>
    <tr>
        <td align="right">
            <label for="id_khs">Id Of Khs</label>
        </td>
        <td align="left">
            <?php echo Phalcon\Tag::textField(array('id_khs', 'type' => 'numeric')); ?>
        </td>
    </tr>
    <tr>
        <td align="right">
            <label for="ta">Ta</label>
        </td>
        <td align="left">
            <?php echo Phalcon\Tag::textField(array('ta', 'size' => 30)); ?>
        </td>
    </tr>
    <tr>
        <td align="right">
            <label for="semester">Semester</label>
        </td>
        <td align="left">
                <?php echo Phalcon\Tag::textField(array('semester')); ?>
        </td>
    </tr>
    <tr>
        <td align="right">
            <label for="mahasiswa_nim">Mahasiswa Of Nim</label>
        </td>
        <td align="left">
            <?php echo Phalcon\Tag::textField(array('mahasiswa_nim', 'size' => 30)); ?>
        </td>
    </tr>
    <tr>
        <td align="right">
            <label for="jadwal_id_jadwal">Jadwal Of Id Of Jadwal</label>
        </td>
        <td align="left">
            <?php echo Phalcon\Tag::textField(array('jadwal_id_jadwal', 'type' => 'numeric')); ?>
        </td>
    </tr>

    <tr>
        <td></td>
        <td><?php echo Phalcon\Tag::submitButton(array('Search')); ?></td>
    </tr>
</table>

</form>
