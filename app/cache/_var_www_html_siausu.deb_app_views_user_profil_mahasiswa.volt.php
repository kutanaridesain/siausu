
<?php echo $this->getContent(); ?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-warning ">
            <div class="box-header">
                <h3 class="box-title">Info mahasiswa</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <div class="clearfix">
                    <table class="detail-info pull-left">
                        <colgroup>
                            <col width="50%"></col>
                            <col width="50%"></col>
                        </colgroup>
                        <tr>
                            <th>Nama </th>
                            <td><strong>:</strong> <?php echo $mahasiswa->getNama(); ?></td>
                        </tr>
                        <tr>
                            <th>NIM </th>
                            <td><strong>:</strong> <?php echo $mahasiswa->getNim(); ?></td>
                        </tr>
                        <tr>
                            <th>Tempat, Tanggal Lahir </th>
                            <td><strong>:</strong> <?php echo $mahasiswa->getTempatLahir(); ?>, <?php echo $mahasiswa->getTanggalLahir(); ?></td>
                        </tr>
                        <tr>
                            <th>Jenis Kelamin </th>
                            <td><strong>:</strong> <?php echo $mahasiswa->getJenisKelamin(); ?></td>
                        </tr>
                        <tr>
                            <th>Agama </th>
                            <td><strong>:</strong> <?php echo $mahasiswa->agama->getNamaAgama(); ?></td>
                        </tr>
                        <tr>
                            <th>Konsentrasi </th>
                            <td><strong>:</strong> <?php echo $mahasiswa->konsentrasi->getNamaKonsentrasi(); ?></td>
                        </tr>
                        <tr>
                            <th>Angkatan </th>
                            <td><strong>:</strong> <?php echo $mahasiswa->getAngkatan(); ?></td>
                        </tr>
                        <tr>
                            <th>Alamat </th>
                            <td><strong>:</strong> <?php echo $mahasiswa->getAlamat(); ?></td>
                        </tr>
                        <tr>
                            <th>Kota </th>
                            <td><strong>:</strong> <?php echo $mahasiswa->getKota(); ?></td>
                        </tr>
                        <tr>
                            <th>Kodepos </th>
                            <td><strong>:</strong> <?php echo $mahasiswa->getKodepos(); ?></td>
                        </tr>
                        <tr>
                            <th>Email </th>
                            <td><strong>:</strong> <?php echo $mahasiswa->getEmail(); ?></td>
                        </tr>
                        <tr>
                            <th>Telepon </th>
                            <td><strong>:</strong> <?php echo $mahasiswa->getNoTelp(); ?></td>
                        </tr>
                        <tr>
                            <th>Asal sekolah </th>
                            <td><strong>:</strong> <?php echo $mahasiswa->getAsalSekolah(); ?></td>
                        </tr>
                        <tr>
                            <th>Tahun lulus </th>
                            <td><strong>:</strong> <?php echo $mahasiswa->getTahunLulus(); ?></td>
                        </tr>
                        <tr>
                            <th>Jenis kuliah </th>
                            <td><strong>:</strong> <?php echo $mahasiswa->getJenisKuliah(); ?></td>
                        </tr>
                        <tr>
                            <th>Status </th>
                            <td><strong>:</strong> <?php echo $mahasiswa->status->getNamaStatus(); ?></td>
                        </tr>
                    </table>    
                </div>
            </div>
        </div>
        <div class="box box-warning ">
            <div class="box-header">
                <h3 class="box-title">Info Orangtua</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <div class="clearfix">
                    <table class="detail-info pull-left">
                        <colgroup>
                            <col width="50%"></col>
                            <col width="50%"></col>
                        </colgroup>
                        <tr>
                            <th>Nama </th>
                            <td><strong>:</strong> <?php echo $mahasiswa->getNamaOrangtua(); ?></td>
                        </tr>
                        <tr>
                            <th>Pekerjaan </th>
                            <td><strong>:</strong> <?php echo $mahasiswa->getPekerjaanOrangtua(); ?></td>
                        </tr>
                        <tr>
                            <th>Alamat </th>
                            <td><strong>:</strong> <?php echo $mahasiswa->getAlamatOrangtua(); ?></td>
                        </tr>
                        <tr>
                            <th>Kota </th>
                            <td><strong>:</strong> <?php echo $mahasiswa->getKotaOrangtua(); ?></td>
                        </tr>
                        <tr>
                            <th>Kodepos </th>
                            <td><strong>:</strong> <?php echo $mahasiswa->getKodeposOrangtua(); ?></td>
                        </tr>
                        <tr>
                            <th>No. Telepon </th>
                            <td><strong>:</strong> <?php echo $mahasiswa->getNoTelpOrangtua(); ?></td>
                        </tr>
                    </table>    
                </div>
            </div>
        </div>
    </div>
</div>
