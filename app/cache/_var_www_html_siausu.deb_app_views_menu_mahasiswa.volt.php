<ul class="sidebar-menu">
    <li><?php echo Phalcon\Tag::linkTo(array('matakuliah', '<i class="fa fa-angle-double-right"></i> Informasi Mata Kuliah')); ?></li>
    <li><?php echo Phalcon\Tag::linkTo(array('krs', '<i class="fa fa-angle-double-right"></i> KRS')); ?></li>
    <li><?php echo Phalcon\Tag::linkTo(array('khs/info', '<i class="fa fa-angle-double-right"></i> KHS')); ?></li>
    <li><?php echo Phalcon\Tag::linkTo(array('jadwal/kuliah', '<i class="fa fa-angle-double-right"></i> Jadwal Kuliah')); ?></li>
    <li><?php echo Phalcon\Tag::linkTo(array('tiket', '<i class="fa fa-angle-double-right"></i> Pengaduan')); ?></li>
    <li><?php echo Phalcon\Tag::linkTo(array('user/password', '<i class="fa fa-angle-double-right"></i> Ganti Password')); ?></li>
</ul>