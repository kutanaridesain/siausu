
<?php echo $this->getContent(); ?>

<div class="jumbotron alert alert-danger">
    <h2>Tidak ada hak akses !</h2>
    <p>Anda tidak punya hak akses ke halaman ini, silahkan hubungi administrator.</p>
    <p><?php echo Phalcon\Tag::linkTo(array('index', 'Home', 'class' => 'btn btn-primary')); ?></p>
</div>