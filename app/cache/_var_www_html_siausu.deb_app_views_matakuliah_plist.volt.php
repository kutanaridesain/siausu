
<div ng-controller="MatakuliahController as matakuliah">    
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-success">
                <div class="box-header">
                    <h3 class="box-title">Daftar Matakuliah</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                <?php echo $this->getContent(); ?>
                <!-- <a class="btn btn-primary" ng-mousedown="csv.generate()" ng-href="[[ csv.link() ]]" download="test.csv">Export to CSV</a> -->
                    <div class="box-body table-responsive">
                    <table ng-table="tableParams" show-filter="true" export-csv="csv" class="table table-bordered table-hover" ng-table>
                        <colgroup>
                            <col width="2%"></col>
                            <col width="30%"></col>
                            <col width="40%"></col>
                            <col width="10%"></col>
                            <col width="10%"></col>
                        </colgroup>
                        <!-- <thead>
                            <tr>
                                <th>No.</th>
                                <th data-title="'Kode Mata Kuliah'" sortable="'kode_matakuliah'">Kode Mata Kuliah</th>
                                <th data-title="'Nama Mata Kuliah'" sortable="'nama_matakuliah'">Nama Mata Kuliah</th>
                                <th>SKS</th>
                                <th>Semester</th>
                            </tr>
                        </thead> -->
                        <tbody>
                            <tr ng-repeat="itemMatakuliah in $data">
                                <td data-title="'No.'">[[$index+1]]</td>
                                <td data-title="'Kode Mata Kuliah'" sortable="'kode_matakuliah'" filter="{ 'kode_matakuliah': 'text' }">[[itemMatakuliah.kode_matakuliah]]</td>
                                <td data-title="'Nama Mata Kuliah'" sortable="'nama_matakuliah'" filter="{ 'nama_matakuliah': 'text' }">[[itemMatakuliah.nama_matakuliah]]</td>
                                <td data-title="'SKS'" sortable="'sks'" filter="{ 'sks': 'text' }">[[itemMatakuliah.sks]]</td>
                                <td data-title="'Semester'" sortable="'semester'" filter="{ 'semester': 'text' }">[[itemMatakuliah.semester]]</td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="6" align="center"></td>
                            </tr>
                        </tfoot>
                    </table>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
