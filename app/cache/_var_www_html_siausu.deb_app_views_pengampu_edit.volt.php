
<?php echo Phalcon\Tag::form(array('pengampu/save', 'method' => 'post', 'autocomplete' => 'off', 'role' => 'form')); ?>
<?php if ($this->getContent() != '') { ?>
<div class="alert alert-danger alert-dismissable">
    <i class="fa fa-ban"></i>
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
<?php echo $this->getContent(); ?>
</div>
<?php } ?>
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Ubah Nama Pengampu</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group">
                        <label for="dosen_nip">Mata Kuliah</label>
                        <?php echo Phalcon\Tag::select(array('matakuliah_id_matakuliah', $matakuliah, 'using' => array('id_matakuliah', 'nama_matakuliah'), 'class' => 'form-control')); ?>
                    </div>
                    <div class="form-group">
                        <label for="dosen_nip">Dosen Pengampu</label>
                        <?php echo Phalcon\Tag::select(array('dosen_nip', $dosen, 'using' => array('nip', 'nama_dosen'), 'class' => 'form-control')); ?>
                    </div>
                </div>
                <div class="box-footer clearfix">
                    <?php echo Phalcon\Tag::hiddenField(array('id_pengampu')); ?>
                    <?php echo Phalcon\Tag::linkTo(array('pengampu', 'Batal', 'class' => 'btn btn-default pull-left')); ?>
                    <?php echo Phalcon\Tag::submitButton(array('Simpan', 'class' => 'btn btn-primary pull-right')); ?>
                </div>
            </div>
        </div>
    </div>
</form>