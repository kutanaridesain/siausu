<?php
/**
 * @author Fery Putra Tarigan <kutanaridesain@gmail.com>
 * @link http://kutanari.com, http://kutanaridesain.com
 * @since 2015
 * @license MIT License
 */

 
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;

class KrsController extends ControllerBase
{

    /**
     * Index action
     */
    public function indexAction()
    {
        $parameters = null;
        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, "Krs", $_POST);
            $parameters = $query->getParams();
            if (!is_array($parameters)) {
                $parameters = array();
            }
            $parameters["order"] = "id_krs";
        }
        
        $krs = Krs::find($parameters);
        if (count($krs) == 0) {
            $this->flash->notice("Data KRS tidak ditemukan !");
        }

        $paginator = new Paginator(array(
            "data" => $krs,
            "limit"=> 10,
            "page" => $numberPage
        ));

        $this->view->page = $paginator->getPaginate();
        $numberPage = $this->request->getQuery("page", "int");
    }

    /**
     * Searches for krs
     */
    public function searchAction()
    {

        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, "Krs", $_POST);
            $this->persistent->parameters = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = $this->persistent->parameters;
        if (!is_array($parameters)) {
            $parameters = array();
        }
        $parameters["order"] = "id_krs";

        $krs = Krs::find($parameters);
        if (count($krs) == 0) {
            $this->flash->notice("The search did not find any krs");

            return $this->dispatcher->forward(array(
                "controller" => "krs",
                "action" => "index"
            ));
        }

        $paginator = new Paginator(array(
            "data" => $krs,
            "limit"=> 10,
            "page" => $numberPage
        ));

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Displayes the creation form
     */
    public function newAction()
    {
        $ta = $this->getTA();
        $this->view->ta = $ta;
        $this->view->mahasiswa = Mahasiswa::find();
        $modelJadwalKuliah = JadwalKuliah::find();
        $this->view->jadwal = $modelJadwalKuliah;
        $jk = array();
        foreach ($modelJadwalKuliah as $jadwalItem) {
            $jadwal = new stdClass();
            $jadwal->id_jadwal = $jadwalItem->getIdJadwal();
            $jadwal->nama_matakuliah = $jadwalItem->getNamaMatakuliah();
            $jadwal->kode_matakuliah = $jadwalItem->getKodeMatakuliah();
            $jadwal->nama_dosen = $jadwalItem->getNamaDosen();
            $jadwal->mulai = $jadwalItem->getMulai();
            $jadwal->selesai = $jadwalItem->getSelesai();
            $jadwal->nama_ruangan = $jadwalItem->getNamaRuangan();
            $jadwal->hari = $jadwalItem->getHari();
            $jadwal->sks = $jadwalItem->getSks();
            $jadwal->semester = $jadwalItem->getSemester();
            $jk[] = $jadwal;
        }
        $this->view->jk = $jk;
        $this->view->arrHari = array("1"=>"Senin", "2"=>"Selasa", "3"=>"Rabu", "4"=>"Kamis", "5"=>"Jumat", "6"=>"Sabtu", "7"=>"Minggu");
        $this->assets->addJs('public/js/plugins/select/bootstrap-select.min.js');
        $this->assets->addCss('public/js/plugins/select/bootstrap-select.min.css');
    }

    /**
     * Edits a kr
     *
     * @param string $id_krs
     */
    public function editAction($id_krs)
    {
        $ta = $this->getTA();
        $this->view->ta = $ta;
        $this->view->mahasiswa = Mahasiswa::find();

        $this->view->jadwal = JadwalKuliah::find();
        $this->assets->addJs('public/js/bootstrap/select.js');
        
        if (!$this->request->isPost()) {

            $kr = Krs::findFirstByid_krs($id_krs);
            if (!$kr) {
                $this->flash->error("kr was not found");

                return $this->dispatcher->forward(array(
                    "controller" => "krs",
                    "action" => "index"
                ));
            }

            $this->view->id_krs = $kr->id_krs;

            $this->tag->setDefault("id_krs", $kr->getIdKrs());
            $this->tag->setDefault("ta", $kr->getTa());
            $this->tag->setDefault("semester", $kr->getSemester());
            $this->tag->setDefault("mahasiswa_nim", $kr->getMahasiswaNim());
            $this->tag->setDefault("jadwal_id_jadwal", $kr->getJadwalIdJadwal());
            
        }
    }

    /**
     * Creates a new kr
     */
    public function createAction()
    {

        if (!$this->request->isPost()) {
            return $this->dispatcher->forward(array(
                "controller" => "krs",
                "action" => "index"
            ));
        }

        $kr = new Krs();

        $kr->setTa($this->request->getPost("ta"));
        $kr->setSemester($this->request->getPost("semester"));
        $kr->setMahasiswaNim($this->request->getPost("mahasiswa_nim"));
        $kr->setJadwalIdJadwal($this->request->getPost("jadwal_id_jadwal"));
        

        if (!$kr->save()) {
            foreach ($kr->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "krs",
                "action" => "new"
            ));
        }

        $this->flash->success("kr was created successfully");

        return $this->dispatcher->forward(array(
            "controller" => "krs",
            "action" => "index"
        ));

    }

    /**
     * Saves a kr edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            return $this->dispatcher->forward(array(
                "controller" => "krs",
                "action" => "index"
            ));
        }

        $id_krs = $this->request->getPost("id_krs");

        $kr = Krs::findFirstByid_krs($id_krs);
        if (!$kr) {
            $this->flash->error("kr does not exist " . $id_krs);

            return $this->dispatcher->forward(array(
                "controller" => "krs",
                "action" => "index"
            ));
        }

        $kr->setTa($this->request->getPost("ta"));
        $kr->setSemester($this->request->getPost("semester"));
        $kr->setMahasiswaNim($this->request->getPost("mahasiswa_nim"));
        $kr->setJadwalIdJadwal($this->request->getPost("jadwal_id_jadwal"));
        

        if (!$kr->save()) {

            foreach ($kr->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "krs",
                "action" => "edit",
                "params" => array($kr->id_krs)
            ));
        }

        $this->flash->success("kr was updated successfully");

        return $this->dispatcher->forward(array(
            "controller" => "krs",
            "action" => "index"
        ));

    }

    /**
     * Deletes a kr
     *
     * @param string $id_krs
     */
    public function deleteAction($id_krs)
    {

        $kr = Krs::findFirstByid_krs($id_krs);
        if (!$kr) {
            $this->flash->error("kr was not found");

            return $this->dispatcher->forward(array(
                "controller" => "krs",
                "action" => "index"
            ));
        }

        if (!$kr->delete()) {

            foreach ($kr->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "krs",
                "action" => "search"
            ));
        }

        $this->flash->success("kr was deleted successfully");

        return $this->dispatcher->forward(array(
            "controller" => "krs",
            "action" => "index"
        ));
    }

    private function getTA()
    {
        $tahunAwal = '2000';
        $tahunSaatIni = date('Y');
        $tahunAjaran = array();
        for ($tahun = $tahunAwal; $tahun  <= $tahunSaatIni; $tahun ++) { 
            $tahunAjaran[($tahun-1).'/'.$tahun] = ($tahun-1).'/'.$tahun;
        }
        return $tahunAjaran;
    }

}
