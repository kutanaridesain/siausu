<?php
/**
 * @author Fery Putra Tarigan <kutanaridesain@gmail.com>
 * @link http://kutanari.com, http://kutanaridesain.com
 * @since 2015
 * @license MIT License
 */

use Phalcon\Mvc\View;

class UserController extends ControllerBase
{

    public function indexAction()
    {

    }

    public function profilAction()
    {
        $this->view->namaAction = "Profil";
        $role = $this->session->get('auth')['role'];
        
        switch ($role) {
            case 'mahasiswa':
                $this->_profileMahasiswa();
                break;
            
            case 'dosen':
                $this->_profileDosen();
                break;
            
            case 'pegawai':
                $this->_profilePegawai();
                break;
            
            default:
                $this->_profileAdmin();
                break;
        }
    }

    private function _profileMahasiswa()
    {
        $mahasiswa = Mahasiswa::findFirstBynim($this->session->get('auth')['id']);
        $this->view->pick('user/profil/mahasiswa');
        $this->view->mahasiswa = $mahasiswa;
    }

    private function _profileDosen()
    {
        
    }

    private function _profilePegawai()
    {

    }

    private function _profileAdmin()
    {

    }

    public function gantipasswordAction()
    {
        $this->view->namaAction = "Profil";
        $role = $this->session->get('auth')['role'];
        
        switch ($role) {
            case 'mahasiswa':
                $this->_gantipasswordMahasiswa();
                break;
            
            case 'dosen':
                $this->_gantipasswordDosen();
                break;
            
            case 'pegawai':
                $this->_gantipasswordPegawai();
                break;
            
            default:
                $this->_gantipasswordAdmin();
                break;
        }    
    }

    private function _gantipasswordMahasiswa()
    {

    }

    private function _gantipasswordDosen()
    {

    }

    private function _gantipasswordPegawai()
    {

    }

    private function _gantipasswordAdmin()
    {

    }

}

