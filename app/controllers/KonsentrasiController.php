<?php
/**
 * @author Fery Putra Tarigan <kutanaridesain@gmail.com>
 * @link http://kutanari.com, http://kutanaridesain.com
 * @since 2015
 * @license MIT License
 */

 
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;

class KonsentrasiController extends ControllerBase
{

    /**
     * Index action konsentrasi
     */
    public function indexAction()
    {
        $this->persistent->parameters = null;
        $numberPage = 1;
        $parameters["order"] = "id_konsentrasi";

        $konsentrasi = Konsentrasi::find($parameters);
        if (count($konsentrasi) == 0) {
            $this->flash->notice("The search did not find any konsentrasi");
        }

        $paginator = new Paginator(array(
            "data" => $konsentrasi,
            "limit"=> 10,
            "page" => $numberPage
        ));

        $this->view->page = $paginator->getPaginate();
        $this->assets->addJs('public/js/angular.js');
        $this->assets->addJs('public/js/siausu.js');
    }

    /**
     * Searches for konsentrasi
     */
    public function searchAction()
    {

        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, "Konsentrasi", $_POST);
            $this->persistent->parameters = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = $this->persistent->parameters;
        if (!is_array($parameters)) {
            $parameters = array();
        }
        $parameters["order"] = "id_konsentrasi";

        $konsentrasi = Konsentrasi::find($parameters);
        if (count($konsentrasi) == 0) {
            $this->flash->notice("The search did not find any konsentrasi");

            return $this->dispatcher->forward(array(
                "controller" => "konsentrasi",
                "action" => "index"
            ));
        }

        $paginator = new Paginator(array(
            "data" => $konsentrasi,
            "limit"=> 10,
            "page" => $numberPage
        ));

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Displayes the creation form
     */
    public function newAction()
    {

    }

    /**
     * Edits a konsentrasi
     *
     * @param string $id_konsentrasi
     */
    public function editAction($id_konsentrasi)
    {

        if (!$this->request->isPost()) {

            $konsentrasi = Konsentrasi::findFirstByid_konsentrasi($id_konsentrasi);
            if (!$konsentrasi) {
                $this->flash->error("konsentrasi was not found");

                return $this->dispatcher->forward(array(
                    "controller" => "konsentrasi",
                    "action" => "index"
                ));
            }

            $this->view->id_konsentrasi = $konsentrasi->id_konsentrasi;

            $this->tag->setDefault("id_konsentrasi", $konsentrasi->getIdKonsentrasi());
            $this->tag->setDefault("nama_konsentrasi", $konsentrasi->getNamaKonsentrasi());
            
        }
    }

    /**
     * Creates a new konsentrasi
     */
    public function createAction()
    {

        if (!$this->request->isPost()) {
            return $this->dispatcher->forward(array(
                "controller" => "konsentrasi",
                "action" => "index"
            ));
        }

        $konsentrasi = new Konsentrasi();

        $konsentrasi->setNamaKonsentrasi($this->request->getPost("nama_konsentrasi"));
        

        if (!$konsentrasi->save()) {
            foreach ($konsentrasi->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "konsentrasi",
                "action" => "new"
            ));
        }

        $this->flash->success("konsentrasi was created successfully");

        return $this->dispatcher->forward(array(
            "controller" => "konsentrasi",
            "action" => "index"
        ));

    }

    /**
     * Saves a konsentrasi edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            return $this->dispatcher->forward(array(
                "controller" => "konsentrasi",
                "action" => "index"
            ));
        }

        $id_konsentrasi = $this->request->getPost("id_konsentrasi");

        $konsentrasi = Konsentrasi::findFirstByid_konsentrasi($id_konsentrasi);
        if (!$konsentrasi) {
            $this->flash->error("konsentrasi does not exist " . $id_konsentrasi);

            return $this->dispatcher->forward(array(
                "controller" => "konsentrasi",
                "action" => "index"
            ));
        }

        $konsentrasi->setNamaKonsentrasi($this->request->getPost("nama_konsentrasi"));
        

        if (!$konsentrasi->save()) {

            foreach ($konsentrasi->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "konsentrasi",
                "action" => "edit",
                "params" => array($konsentrasi->id_konsentrasi)
            ));
        }

        $this->flash->success("konsentrasi was updated successfully");

        return $this->dispatcher->forward(array(
            "controller" => "konsentrasi",
            "action" => "index"
        ));

    }

    /**
     * Deletes a konsentrasi
     *
     * @param string $id_konsentrasi
     */
    public function deleteAction($id_konsentrasi)
    {

        $konsentrasi = Konsentrasi::findFirstByid_konsentrasi($id_konsentrasi);
        if (!$konsentrasi) {
            $this->flash->error("konsentrasi was not found");

            return $this->dispatcher->forward(array(
                "controller" => "konsentrasi",
                "action" => "index"
            ));
        }

        if (!$konsentrasi->delete()) {

            foreach ($konsentrasi->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "konsentrasi",
                "action" => "search"
            ));
        }

        $this->flash->success("konsentrasi was deleted successfully");

        return $this->dispatcher->forward(array(
            "controller" => "konsentrasi",
            "action" => "index"
        ));
    }

    /**
    * handler untuk reques ajax dari angular js
    **/
    public function ajaxAction($id_konsentrasi = null)
    {
        $this->view->disable();
        $messages = array();
        $data = array();

        if ($this->request->isPost()) {
            $Konsentrasi = new Konsentrasi();

            $post = $this->request->getJsonRawBody();
            $Konsentrasi->setNamaKonsentrasi($post->nama_konsentrasi);
        

            if (!$Konsentrasi->save()) {
                foreach ($Konsentrasi->getMessages() as $message) {
                    $messages[] = $message;
                }
                $data = $messages;
            }
        }elseif($this->request->isPut()){
            $put = $this->request->getJsonRawBody();

            $Konsentrasi = Konsentrasi::findFirstByid_konsentrasi($put->id);
            if (!$Konsentrasi) {
                $messages[] = "Konsentrasi does not exist " . $put->id;
            }

            $Konsentrasi->setNamaKonsentrasi($put->nama_konsentrasi);

            if (!$Konsentrasi->save()) {

                foreach ($Konsentrasi->getMessages() as $message) {
                    $messages[] = $message;
                }
            }
            $data = $messages;
        }elseif($this->request->isDelete()){

            $delete = $this->request->getJsonRawBody();
            $Konsentrasi = Konsentrasi::findFirstByid_konsentrasi($id_konsentrasi);
            if (!$Konsentrasi) {
                $data = "data Konsentrasi tidak ditemukan";
            }

            if (!$Konsentrasi->delete()) {
                foreach ($Konsentrasi->getMessages() as $message) {
                    $messages[] = $message;
                }
                $data = $messages;
            }
        }else{
            $Konsentrasi = Konsentrasi::find();
            foreach ($Konsentrasi as $item) {
                $data[] = array(
                    'id' => $item->getIdKonsentrasi(),
                    'nama_konsentrasi' => $item->getNamaKonsentrasi()
                    );
            }
        }
        $this->response->setContentType('application/json', 'UTF-8');
        $this->response->setContent(json_encode($data));
        $this->response->send();
    }

}
