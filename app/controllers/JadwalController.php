<?php
/**
 * @author Fery Putra Tarigan <kutanaridesain@gmail.com>
 * @link http://kutanari.com, http://kutanaridesain.com
 * @since 2015
 * @license MIT License
 */

 
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;

class JadwalController extends ControllerBase
{

    /**
     * Index action
     */
    public function indexAction()
    {
        $modelJadwalKuliah = JadwalKuliah::find();
        if (count($modelJadwalKuliah) == 0) {
            $this->flash->notice("Data jadwal masih kosong");
        }
        $jk = array();
        foreach ($modelJadwalKuliah as $jadwalItem) {
            $jadwal = new stdClass();
            $jadwal->id_jadwal = $jadwalItem->getIdJadwal();
            $jadwal->nama_matakuliah = $jadwalItem->getNamaMatakuliah();
            $jadwal->kode_matakuliah = $jadwalItem->getKodeMatakuliah();
            $jadwal->nama_dosen = $jadwalItem->getNamaDosen();
            $jadwal->mulai = $jadwalItem->getMulai();
            $jadwal->selesai = $jadwalItem->getSelesai();
            $jadwal->nama_ruangan = $jadwalItem->getNamaRuangan();
            $jadwal->hari = $jadwalItem->getHari();
            $jadwal->sks = $jadwalItem->getSks();
            $jadwal->semester = $jadwalItem->getSemester();
            $jk[] = $jadwal;
        }
        $this->view->jk = $jk;

        $this->view->arrHari = array("1"=>"Senin", "2"=>"Selasa", "3"=>"Rabu", "4"=>"Kamis", "5"=>"Jumat", "6"=>"Sabtu", "7"=>"Minggu");
    }

    /**
     * Searches for jadwal
     */
    public function searchAction()
    {

        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, "Jadwal", $_POST);
            $this->persistent->parameters = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = $this->persistent->parameters;
        if (!is_array($parameters)) {
            $parameters = array();
        }
        $parameters["order"] = "id_jadwal";

        $jadwal = Jadwal::find($parameters);
        if (count($jadwal) == 0) {
            $this->flash->notice("The search did not find any jadwal");

            return $this->dispatcher->forward(array(
                "controller" => "jadwal",
                "action" => "index"
            ));
        }

        $paginator = new Paginator(array(
            "data" => $jadwal,
            "limit"=> 10,
            "page" => $numberPage
        ));

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Displayes the creation form
     */
    public function newAction()
    {
        $this->setingHalaman();   
    }

    /**
     * Edits a jadwal
     *
     * @param string $id_jadwal
     */
    public function editAction($id_jadwal)
    {
        $this->setingHalaman();
        if (!$this->request->isPost()) {

            $jadwal = Jadwal::findFirstByid_jadwal($id_jadwal);
            if (!$jadwal) {
                $this->flash->error("data jadwal tidak ditemukan");

                return $this->dispatcher->forward(array(
                    "controller" => "jadwal",
                    "action" => "index"
                ));
            }

            $this->view->id_jadwal = $id_jadwal;

            $this->tag->setDefault("id_jadwal", $jadwal->getIdJadwal());
            $this->tag->setDefault("hari", $jadwal->getHari());
            $this->tag->setDefault("mulai", $jadwal->getMulai());
            $this->tag->setDefault("selesai", $jadwal->getSelesai());
            $this->tag->setDefault("pengampu_id_pengampu", $jadwal->getPengampuIdPengampu());
            $this->tag->setDefault("ruangan_id_ruangan", $jadwal->getRuanganIdRuangan());
            
        }
    }

    /**
     * Creates a new jadwal
     */
    public function createAction()
    {

        if (!$this->request->isPost()) {
            return $this->dispatcher->forward(array(
                "controller" => "jadwal",
                "action" => "index"
            ));
        }

        $jadwal = new Jadwal();

        $jadwal->setHari($this->request->getPost("hari"));
        $jadwal->setMulai($this->request->getPost("mulai"));
        $jadwal->setSelesai($this->request->getPost("selesai"));
        $jadwal->setPengampuIdPengampu($this->request->getPost("pengampu_id_pengampu"));
        $jadwal->setRuanganIdRuangan($this->request->getPost("ruangan_id_ruangan"));
        

        if (!$jadwal->save()) {
            foreach ($jadwal->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "jadwal",
                "action" => "new"
            ));
        }

        $this->flash->success("data jadwal sukses ditambah");

        return $this->dispatcher->forward(array(
            "controller" => "jadwal",
            "action" => "index"
        ));

    }

    /**
     * Saves a jadwal edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            return $this->dispatcher->forward(array(
                "controller" => "jadwal",
                "action" => "index"
            ));
        }

        $id_jadwal = $this->request->getPost("id_jadwal");

        $jadwal = Jadwal::findFirstByid_jadwal($id_jadwal);
        if (!$jadwal) {
            $this->flash->error("data jadwal tidak ditemukan " . $id_jadwal);

            return $this->dispatcher->forward(array(
                "controller" => "jadwal",
                "action" => "index"
            ));
        }

        $jadwal->setHari($this->request->getPost("hari"));
        $jadwal->setMulai($this->request->getPost("mulai"));
        $jadwal->setSelesai($this->request->getPost("selesai"));
        $jadwal->setPengampuIdPengampu($this->request->getPost("pengampu_id_pengampu"));
        $jadwal->setRuanganIdRuangan($this->request->getPost("ruangan_id_ruangan"));
        

        if (!$jadwal->save()) {

            foreach ($jadwal->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "jadwal",
                "action" => "edit",
                "params" => array($jadwal->id_jadwal)
            ));
        }

        $this->flash->success("data jadwal sukses diubah");

        return $this->dispatcher->forward(array(
            "controller" => "jadwal",
            "action" => "index"
        ));

    }

    /**
     * Deletes a jadwal
     *
     * @param string $id_jadwal
     */
    public function deleteAction($id_jadwal)
    {

        $jadwal = Jadwal::findFirstByid_jadwal($id_jadwal);
        if (!$jadwal) {
            $this->flash->error("data jadwal tidak ditemukan");

            return $this->dispatcher->forward(array(
                "controller" => "jadwal",
                "action" => "index"
            ));
        }

        if (!$jadwal->delete()) {

            foreach ($jadwal->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "jadwal",
                "action" => "search"
            ));
        }

        $this->flash->success("data jadwal sukses dihapus");

        return $this->dispatcher->forward(array(
            "controller" => "jadwal",
            "action" => "index"
        ));
    }

    private function setingHalaman()
    {
        $modelPengampu = Pengampu::find();
        $arrPengampu = array();
        foreach ($modelPengampu as $pengampu) {
            $arrPengampu[$pengampu->getIdPengampu()] = sprintf('%s - %s', $pengampu->matakuliah->getNamaMatakuliah(), $pengampu->dosen->getNamaDosen());
        }
        $this->view->arrPengampu = $arrPengampu;
        $this->assets->addCss( "public/css/timepicker/bootstrap-timepicker.min.css");
        $this->assets->addJs( "public/js/plugins/timepicker/bootstrap-timepicker.min.js");
        $this->assets->addJs( "public/js/plugins/input-mask/jquery.inputmask.js");
        $this->assets->addJs( "public/js/plugins/input-mask/jquery.inputmask.extensions.js");
        $this->assets->addJs( "public/js/plugins/input-mask/jquery.inputmask.date.extensions.js");

        $this->view->ruangan = Ruangan::find();
        $this->view->customScript = '$(".timepicker").timepicker({showInputs: false, showMeridian:false});';
    }

    public function kuliahAction()
    {
        
    }

}
