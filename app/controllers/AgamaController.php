<?php
/**
 * @author Fery Putra Tarigan <kutanaridesain@gmail.com>
 * @link http://kutanari.com, http://kutanaridesain.com
 * @since 2015
 * @license MIT License
 */

 
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;

class AgamaController extends ControllerBase
{
    public function initialize()
    {
        parent::initialize();
        $this->tag->appendTitle(' | Agama');
    }

    /**
     * Index action
     */
    public function indexAction()
    {
        $this->persistent->parameters = null;
        $numberPage = 1;

        $parameters["order"] = "id_agama";

        $agama = Agama::find($parameters);
        if (count($agama) == 0) {
            $this->flash->notice("Data agama masih kosong !");
        }

        $paginator = new Paginator(array(
            "data" => $agama,
            "limit"=> 10,
            "page" => $numberPage
        ));
        

        $this->assets->addJs('public/js/angular.js');
        $this->assets->addJs('public/js/siausu.js');
        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Searches for agama
     */
    public function searchAction()
    {
        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, "Agama", $_POST);
            $this->persistent->parameters = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = $this->persistent->parameters;
        if (!is_array($parameters)) {
            $parameters = array();
        }
        $parameters["order"] = "id_agama";

        $agama = Agama::find($parameters);
        if (count($agama) == 0) {
            $this->flash->notice("The search did not find any agama");

            return $this->dispatcher->forward(array(
                "controller" => "agama",
                "action" => "index"
            ));
        }

        $paginator = new Paginator(array(
            "data" => $agama,
            "limit"=> 10,
            "page" => $numberPage
        ));

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Displayes the creation form
     */
    public function newAction()
    {

    }

    /**
     * Edits a agama
     *
     * @param string $id_agama
     */
    public function editAction($id_agama)
    {

        if (!$this->request->isPost()) {

            $agama = Agama::findFirstByid_agama($id_agama);
            if (!$agama) {
                $this->flash->error("agama was not found");

                return $this->dispatcher->forward(array(
                    "controller" => "agama",
                    "action" => "index"
                ));
            }

            $this->view->id_agama = $agama->id_agama;

            $this->tag->setDefault("id_agama", $agama->getIdAgama());
            $this->tag->setDefault("nama_agama", $agama->getNamaAgama());
            
        }
    }

    /**
     * Creates a new agama
     */
    public function createAction()
    {

        if (!$this->request->isPost()) {
            return $this->dispatcher->forward(array(
                "controller" => "agama",
                "action" => "index"
            ));
        }

        $agama = new Agama();

        $agama->setNamaAgama($this->request->getPost("nama_agama"));
        

        if (!$agama->save()) {
            foreach ($agama->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "agama",
                "action" => "new"
            ));
        }

        $this->flash->success("agama was created successfully");

        return $this->dispatcher->forward(array(
            "controller" => "agama",
            "action" => "index"
        ));

    }

    /**
     * Saves a agama edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            return $this->dispatcher->forward(array(
                "controller" => "agama",
                "action" => "index"
            ));
        }

        $id_agama = $this->request->getPost("id_agama");

        $agama = Agama::findFirstByid_agama($id_agama);
        if (!$agama) {
            $this->flash->error("agama does not exist " . $id_agama);

            return $this->dispatcher->forward(array(
                "controller" => "agama",
                "action" => "index"
            ));
        }

        $agama->setNamaAgama($this->request->getPost("nama_agama"));
        

        if (!$agama->save()) {

            foreach ($agama->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "agama",
                "action" => "edit",
                "params" => array($agama->id_agama)
            ));
        }

        $this->flash->success("agama was updated successfully");

        return $this->dispatcher->forward(array(
            "controller" => "agama",
            "action" => "index"
        ));

    }

    /**
     * Deletes a agama
     *
     * @param string $id_agama
     */
    public function deleteAction($id_agama)
    {

        $agama = Agama::findFirstByid_agama($id_agama);
        if (!$agama) {
            $this->flash->error("agama was not found");

            return $this->dispatcher->forward(array(
                "controller" => "agama",
                "action" => "index"
            ));
        }

        if (!$agama->delete()) {

            foreach ($agama->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "agama",
                "action" => "search"
            ));
        }

        $this->flash->success("agama was deleted successfully");

        return $this->dispatcher->forward(array(
            "controller" => "agama",
            "action" => "index"
        ));
    }

    /**
    * handler untuk reques ajax dari angular js
    **/
    public function ajaxAction($id_agama = null)
    {
        $this->view->disable();
        $messages = array();
        $data = array();

        if ($this->request->isPost()) {
            $agama = new Agama();

            $post = $this->request->getJsonRawBody();
            $agama->setNamaAgama($post->nama_agama);
        

            if (!$agama->save()) {
                foreach ($agama->getMessages() as $message) {
                    $messages[] = $message;
                }
                $data = $messages;
            }
        }elseif($this->request->isPut()){
            $put = $this->request->getJsonRawBody();

            $agama = Agama::findFirstByid_agama($put->id);
            if (!$agama) {
                $messages[] = "agama does not exist " . $put->id;
            }

            $agama->setNamaAgama($put->nama_agama);

            if (!$agama->save()) {

                foreach ($agama->getMessages() as $message) {
                    $messages[] = $message;
                }
            }
            $data = $messages;
        }elseif($this->request->isDelete()){

            $delete = $this->request->getJsonRawBody();
            $agama = Agama::findFirstByid_agama($id_agama);
            if (!$agama) {
                $data = "data agama tidak ditemukan";
            }

            if (!$agama->delete()) {
                foreach ($agama->getMessages() as $message) {
                    $messages[] = $message;
                }
                $data = $messages;
            }
        }else{
            $agama = Agama::find();
            foreach ($agama as $item) {
                $data[] = array(
                    'id' => $item->getIdAgama(),
                    'nama_agama' => $item->getNamaAgama()
                    );
            }
        }
        $this->response->setContentType('application/json', 'UTF-8');
        $this->response->setContent(json_encode($data));
        $this->response->send();
    }

}
