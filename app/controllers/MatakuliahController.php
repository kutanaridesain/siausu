<?php
/**
 * @author Fery Putra Tarigan <kutanaridesain@gmail.com>
 * @link http://kutanari.com, http://kutanaridesain.com
 * @since 2015
 * @license MIT License
 */

 
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;

class MatakuliahController extends ControllerBase
{

    /**
     * Index action
     */
    public function indexAction()
    {
        $this->persistent->parameters = null;

        $numberPage = 1;
        // if ($this->request->isPost()) {
        //     $query = Criteria::fromInput($this->di, "Matakuliah", $_POST);
        //     $this->persistent->parameters = $query->getParams();
        // } else {
        //     $numberPage = $this->request->getQuery("page", "int");
        // }

        $parameters = $this->persistent->parameters;
        if (!is_array($parameters)) {
            $parameters = array();
        }
        $parameters["order"] = "id_matakuliah";

        $matakuliah = Matakuliah::find($parameters);
        if (count($matakuliah) == 0) {
            $this->flash->notice("Data mata kuliah masih kosong");
        }

        $paginator = new Paginator(array(
            "data" => $matakuliah,
            "limit"=> 10,
            "page" => $numberPage
        ));
            $this->view->page = $paginator->getPaginate();
        if($this->session->get('auth')['role']){
            $this->view->pick('matakuliah/plist');
        }
        $this->assets->addCss('public/css/ng-table.css');
        $this->assets->addJs('public/js/angular.js');
        $this->assets->addJs('public/js/ng-table.js');
        $this->assets->addJs('public/js/siausu.js');
    }

    /**
     * Searches for matakuliah
     */
    public function searchAction()
    {

        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, "Matakuliah", $_POST);
            $this->persistent->parameters = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = $this->persistent->parameters;
        if (!is_array($parameters)) {
            $parameters = array();
        }
        $parameters["order"] = "id_matakuliah";

        $matakuliah = Matakuliah::find($parameters);
        if (count($matakuliah) == 0) {
            $this->flash->notice("The search did not find any matakuliah");

            return $this->dispatcher->forward(array(
                "controller" => "matakuliah",
                "action" => "index"
            ));
        }

        $paginator = new Paginator(array(
            "data" => $matakuliah,
            "limit"=> 10,
            "page" => $numberPage
        ));

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Displayes the creation form
     */
    public function newAction()
    {

    }

    /**
     * Edits a matakuliah
     *
     * @param string $id_matakuliah
     */
    public function editAction($id_matakuliah)
    {

        if (!$this->request->isPost()) {

            $matakuliah = Matakuliah::findFirstByid_matakuliah($id_matakuliah);
            if (!$matakuliah) {
                $this->flash->error("matakuliah was not found");

                return $this->dispatcher->forward(array(
                    "controller" => "matakuliah",
                    "action" => "index"
                ));
            }

            $this->view->id_matakuliah = $matakuliah->id_matakuliah;

            $this->tag->setDefault("id_matakuliah", $matakuliah->getIdMatakuliah());
            $this->tag->setDefault("kode_matakuliah", $matakuliah->getKodeMatakuliah());
            $this->tag->setDefault("nama_matakuliah", $matakuliah->getNamaMatakuliah());
            $this->tag->setDefault("sks", $matakuliah->getSks());
            $this->tag->setDefault("semester", $matakuliah->getSemester());
            
        }
    }

    /**
     * Creates a new matakuliah
     */
    public function createAction()
    {

        if (!$this->request->isPost()) {
            return $this->dispatcher->forward(array(
                "controller" => "matakuliah",
                "action" => "index"
            ));
        }

        $matakuliah = new Matakuliah();

        $matakuliah->setKodeMatakuliah($this->request->getPost("kode_matakuliah"));
        $matakuliah->setNamaMatakuliah($this->request->getPost("nama_matakuliah"));
        $matakuliah->setSks($this->request->getPost("sks"));
        $matakuliah->setSemester($this->request->getPost("semester"));
        

        if (!$matakuliah->save()) {
            foreach ($matakuliah->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "matakuliah",
                "action" => "new"
            ));
        }

        $this->flash->success("matakuliah was created successfully");

        return $this->dispatcher->forward(array(
            "controller" => "matakuliah",
            "action" => "index"
        ));

    }

    /**
     * Saves a matakuliah edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            return $this->dispatcher->forward(array(
                "controller" => "matakuliah",
                "action" => "index"
            ));
        }

        $id_matakuliah = $this->request->getPost("id_matakuliah");

        $matakuliah = Matakuliah::findFirstByid_matakuliah($id_matakuliah);
        if (!$matakuliah) {
            $this->flash->error("matakuliah does not exist " . $id_matakuliah);

            return $this->dispatcher->forward(array(
                "controller" => "matakuliah",
                "action" => "index"
            ));
        }

        $matakuliah->setKodeMatakuliah($this->request->getPost("kode_matakuliah"));
        $matakuliah->setNamaMatakuliah($this->request->getPost("nama_matakuliah"));
        $matakuliah->setSks($this->request->getPost("sks"));
        $matakuliah->setSemester($this->request->getPost("semester"));
        

        if (!$matakuliah->save()) {

            foreach ($matakuliah->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "matakuliah",
                "action" => "edit",
                "params" => array($matakuliah->id_matakuliah)
            ));
        }

        $this->flash->success("matakuliah was updated successfully");

        return $this->dispatcher->forward(array(
            "controller" => "matakuliah",
            "action" => "index"
        ));

    }

    /**
     * Deletes a matakuliah
     *
     * @param string $id_matakuliah
     */
    public function deleteAction($id_matakuliah)
    {

        $matakuliah = Matakuliah::findFirstByid_matakuliah($id_matakuliah);
        if (!$matakuliah) {
            $this->flash->error("matakuliah was not found");

            return $this->dispatcher->forward(array(
                "controller" => "matakuliah",
                "action" => "index"
            ));
        }

        if (!$matakuliah->delete()) {

            foreach ($matakuliah->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "matakuliah",
                "action" => "search"
            ));
        }

        $this->flash->success("matakuliah was deleted successfully");

        return $this->dispatcher->forward(array(
            "controller" => "matakuliah",
            "action" => "index"
        ));
    }

    /**
     * Handle all AJAX request
     *
     * @param string $id_matakuliah
     */
    public function ajaxAction($id_matakuliah = null)
    {
        $this->view->disable();
        $messages = array();
        $data = array();
        $data['status'] = true;
    
        if($this->request->isPost()){
            //proses penambahan data
            $post = $this->request->getJsonRawBody();
            
            $matakuliah = new Matakuliah();
            $matakuliah->setNamaMatakuliah($post->nama_matakuliah);
    
    
            if (!$matakuliah->save()) {
                foreach ($matakuliah->getMessages() as $message) {
                    $messages[] = $message;
                }
                $data['message'] = $messages;
                $data['status'] = false;
            }
        }elseif($this->request->isPut()){
            //proses pengubahan data
            $put = $this->request->getJsonRawBody();
            $matakuliah = Matakuliah::findFirstByid_matakuliah($put->id);
            if (!$matakuliah) {
                $messages[] = "matakuliah tidak ditemukan " . $put->id;
                $data['status'] = false;
            }
    
            $matakuliah->setNamaMatakuliah($put->nama_matakuliah);
    
            if (!$matakuliah->save()) {
                $data['status'] = false;
                foreach ($matakuliah->getMessages() as $message) {
                    $messages[] = $message;
                }
            }
            $data['message'] = $messages;
        }elseif($this->request->isDelete()){
            //proses hapus data
            $delete = $this->request->getJsonRawBody();
            $matakuliah = Matakuliah::findFirstByid_matakuliah($id_matakuliah);
            if (!$matakuliah) {
                $messages[] = "data matakuliah tidak ditemukan";
                $data['status'] = false;
            }
            try {
                $matakuliah->delete();
            } catch (Exception $e) {
                $messages[] = $e->getMessage();
                $data['status'] = false;
            }
            $data['message'] = $messages;
        }else{
            //mengambil data
            $matakuliah = Matakuliah::find();
            foreach ($matakuliah as $item) {
                $data['data'][] = array(
                    'id' => $item->getIdMatakuliah(),
                    'nama_matakuliah' => $item->getNamaMatakuliah(),
                    'kode_matakuliah' => $item->getKodeMatakuliah(),
                    'sks' => $item->getSks(),
                    'semester' => $item->getSemester(),
                );
            }
        }
    
        $this->response->setContentType('application/json', 'UTF-8');
        $this->response->setContent(json_encode($data));
        $this->response->send();
    }

}
