<?php
/**
 * @author Fery Putra Tarigan <kutanaridesain@gmail.com>
 * @link http://kutanari.com, http://kutanaridesain.com
 * @since 2015
 * @license MIT License
 */

 
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;

class JurusanController extends ControllerBase
{

    /**
     * Index action
     */
    public function indexAction()
    {
        $this->persistent->parameters = null;
        $numberPage = 1;
        $parameters["order"] = "id_jurusan";

        $jurusan = Jurusan::find($parameters);
        if (count($jurusan) == 0) {
            $this->flash->notice("The search did not find any jurusan");
        }

        $paginator = new Paginator(array(
            "data" => $jurusan,
            "limit"=> 10,
            "page" => $numberPage
        ));

        $this->view->page = $paginator->getPaginate();
        $this->assets->addJs('public/js/angular.js');
        $this->assets->addJs('public/js/siausu.js');
    }

    /**
     * Searches for jurusan
     */
    public function searchAction()
    {

        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, "Jurusan", $_POST);
            $this->persistent->parameters = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = $this->persistent->parameters;
        if (!is_array($parameters)) {
            $parameters = array();
        }
        $parameters["order"] = "id_jurusan";

        $jurusan = Jurusan::find($parameters);
        if (count($jurusan) == 0) {
            $this->flash->notice("The search did not find any jurusan");

            return $this->dispatcher->forward(array(
                "controller" => "jurusan",
                "action" => "index"
            ));
        }

        $paginator = new Paginator(array(
            "data" => $jurusan,
            "limit"=> 10,
            "page" => $numberPage
        ));

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Displayes the creation form
     */
    public function newAction()
    {

    }

    /**
     * Edits a jurusan
     *
     * @param string $id_jurusan
     */
    public function editAction($id_jurusan)
    {

        if (!$this->request->isPost()) {

            $jurusan = Jurusan::findFirstByid_jurusan($id_jurusan);
            if (!$jurusan) {
                $this->flash->error("jurusan was not found");

                return $this->dispatcher->forward(array(
                    "controller" => "jurusan",
                    "action" => "index"
                ));
            }

            $this->view->id_jurusan = $jurusan->id_jurusan;

            $this->tag->setDefault("id_jurusan", $jurusan->getIdJurusan());
            $this->tag->setDefault("nama_jurusan", $jurusan->getNamaJurusan());
            
        }
    }

    /**
     * Creates a new jurusan
     */
    public function createAction()
    {

        if (!$this->request->isPost()) {
            return $this->dispatcher->forward(array(
                "controller" => "jurusan",
                "action" => "index"
            ));
        }

        $jurusan = new Jurusan();

        $jurusan->setNamaJurusan($this->request->getPost("nama_jurusan"));
        

        if (!$jurusan->save()) {
            foreach ($jurusan->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "jurusan",
                "action" => "new"
            ));
        }

        $this->flash->success("data jurusan sukses ditambah!");

        return $this->dispatcher->forward(array(
            "controller" => "jurusan",
            "action" => "index"
        ));

    }

    /**
     * Saves a jurusan edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            return $this->dispatcher->forward(array(
                "controller" => "jurusan",
                "action" => "index"
            ));
        }

        $id_jurusan = $this->request->getPost("id_jurusan");

        $jurusan = Jurusan::findFirstByid_jurusan($id_jurusan);
        if (!$jurusan) {
            $this->flash->error("data jurusan tidak ditemukan " . $id_jurusan);

            return $this->dispatcher->forward(array(
                "controller" => "jurusan",
                "action" => "index"
            ));
        }

        $jurusan->setNamaJurusan($this->request->getPost("nama_jurusan"));
        

        if (!$jurusan->save()) {

            foreach ($jurusan->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "jurusan",
                "action" => "edit",
                "params" => array($jurusan->id_jurusan)
            ));
        }

        $this->flash->success("data jurusan sukses diubah!");

        return $this->dispatcher->forward(array(
            "controller" => "jurusan",
            "action" => "index"
        ));

    }

    /**
     * Deletes a jurusan
     *
     * @param string $id_jurusan
     */
    public function deleteAction($id_jurusan)
    {

        $jurusan = Jurusan::findFirstByid_jurusan($id_jurusan);
        if (!$jurusan) {
            $this->flash->error("jurusan was not found");

            return $this->dispatcher->forward(array(
                "controller" => "jurusan",
                "action" => "index"
            ));
        }

        if (!$jurusan->delete()) {

            foreach ($jurusan->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "jurusan",
                "action" => "search"
            ));
        }

        $this->flash->success("jurusan was deleted successfully");

        return $this->dispatcher->forward(array(
            "controller" => "jurusan",
            "action" => "index"
        ));
    }


    /**
    * handler untuk reques ajax dari angular js
    **/
    public function ajaxAction($id_jurusan)
    {
        $this->view->disable();
        $messages = array();

        if ($this->request->isPost()) {
            $Jurusan = new Jurusan();

            $post = $this->request->getJsonRawBody();
            $Jurusan->setNamaJurusan($post->nama_jurusan);
        

            if (!$Jurusan->save()) {
                foreach ($Jurusan->getMessages() as $message) {
                    $messages[] = $message;
                }
                $data = $messages;
            }
        }elseif($this->request->isPut()){
            $put = $this->request->getJsonRawBody();

            $Jurusan = Jurusan::findFirstByid_jurusan($put->id);
            if (!$Jurusan) {
                $messages[] = "Jurusan does not exist " . $put->id;
            }

            $Jurusan->setNamaJurusan($put->nama_jurusan);

            if (!$Jurusan->save()) {

                foreach ($Jurusan->getMessages() as $message) {
                    $messages[] = $message;
                }
            }
            $data = $messages;
        }elseif($this->request->isDelete()){

            $delete = $this->request->getJsonRawBody();
            $Jurusan = Jurusan::findFirstByid_jurusan($id_jurusan);
            if (!$Jurusan) {
                $data = "data Jurusan tidak ditemukan";
            }

            if (!$Jurusan->delete()) {
                foreach ($Jurusan->getMessages() as $message) {
                    $messages[] = $message;
                }
                $data = $messages;
            }
        }else{
            $Jurusan = Jurusan::find();
            $data = array();
            foreach ($Jurusan as $item) {
                $data[] = array(
                    'id' => $item->getIdJurusan(),
                    'nama_jurusan' => $item->getNamaJurusan()
                    );
            }
        }
        $this->response->setContentType('application/json', 'UTF-8');
        $this->response->setContent(json_encode($data));
        $this->response->send();
    }

}
