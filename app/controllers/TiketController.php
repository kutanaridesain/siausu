<?php
 
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;

class TiketController extends ControllerBase
{

    /**
     * Index action
     */
    public function indexAction()
    {
        $parameters = array();
        $parameters["order"] = "id_tiket";

        $tiket = Tiket::find($parameters);
        if (count($tiket) == 0) {
            $this->flash->notice("Data tiket masing kosong");
        }
        $numberPage = $this->request->getQuery("page", "int");
        $paginator = new Paginator(array(
            "data" => $tiket,
            "limit"=> 10,
            "page" => $numberPage
        ));

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Searches for tiket
     */
    public function searchAction()
    {

        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, "Tiket", $_POST);
            $this->persistent->parameters = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = $this->persistent->parameters;
        if (!is_array($parameters)) {
            $parameters = array();
        }
        $parameters["order"] = "id_tiket";

        $tiket = Tiket::find($parameters);
        if (count($tiket) == 0) {
            $this->flash->notice("The search did not find any tiket");

            return $this->dispatcher->forward(array(
                "controller" => "tiket",
                "action" => "index"
            ));
        }

        $paginator = new Paginator(array(
            "data" => $tiket,
            "limit"=> 10,
            "page" => $numberPage
        ));

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Displayes the creation form
     */
    public function newAction()
    {

    }

    /**
     * Edits a tiket
     *
     * @param string $id_tiket
     */
    public function editAction($id_tiket)
    {

        if (!$this->request->isPost()) {

            $tiket = Tiket::findFirstByid_tiket($id_tiket);
            if (!$tiket) {
                $this->flash->error("tiket was not found");

                return $this->dispatcher->forward(array(
                    "controller" => "tiket",
                    "action" => "index"
                ));
            }

            $this->view->id_tiket = $tiket->id_tiket;

            $this->tag->setDefault("id_tiket", $tiket->id_tiket);
            $this->tag->setDefault("judul_aduan", $tiket->judul_aduan);
            $this->tag->setDefault("isi_pesan", $tiket->isi_pesan);
            $this->tag->setDefault("status", $tiket->status);
            $this->tag->setDefault("mahasiswa_nim", $tiket->mahasiswa_nim);
            $this->tag->setDefault("tanggal_register", $tiket->tanggal_register);
            
        }
    }

    /**
     * Creates a new tiket
     */
    public function createAction()
    {

        if (!$this->request->isPost()) {
            return $this->dispatcher->forward(array(
                "controller" => "tiket",
                "action" => "index"
            ));
        }

        $tiket = new Tiket();

        $tiket->judul_aduan = $this->request->getPost("judul_aduan");
        $tiket->isi_pesan = $this->request->getPost("isi_pesan");
        $tiket->status = $this->request->getPost("status");
        $tiket->mahasiswa_nim = $this->request->getPost("mahasiswa_nim");
        $tiket->tanggal_register = $this->request->getPost("tanggal_register");
        

        if (!$tiket->save()) {
            foreach ($tiket->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "tiket",
                "action" => "new"
            ));
        }

        $this->flash->success("tiket was created successfully");

        return $this->dispatcher->forward(array(
            "controller" => "tiket",
            "action" => "index"
        ));

    }

    /**
     * Saves a tiket edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            return $this->dispatcher->forward(array(
                "controller" => "tiket",
                "action" => "index"
            ));
        }

        $id_tiket = $this->request->getPost("id_tiket");

        $tiket = Tiket::findFirstByid_tiket($id_tiket);
        if (!$tiket) {
            $this->flash->error("tiket does not exist " . $id_tiket);

            return $this->dispatcher->forward(array(
                "controller" => "tiket",
                "action" => "index"
            ));
        }

        $tiket->judul_aduan = $this->request->getPost("judul_aduan");
        $tiket->isi_pesan = $this->request->getPost("isi_pesan");
        $tiket->status = $this->request->getPost("status");
        $tiket->mahasiswa_nim = $this->request->getPost("mahasiswa_nim");
        $tiket->tanggal_register = $this->request->getPost("tanggal_register");
        

        if (!$tiket->save()) {

            foreach ($tiket->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "tiket",
                "action" => "edit",
                "params" => array($tiket->id_tiket)
            ));
        }

        $this->flash->success("tiket was updated successfully");

        return $this->dispatcher->forward(array(
            "controller" => "tiket",
            "action" => "index"
        ));

    }

    /**
     * Deletes a tiket
     *
     * @param string $id_tiket
     */
    public function deleteAction($id_tiket)
    {

        $tiket = Tiket::findFirstByid_tiket($id_tiket);
        if (!$tiket) {
            $this->flash->error("tiket was not found");

            return $this->dispatcher->forward(array(
                "controller" => "tiket",
                "action" => "index"
            ));
        }

        if (!$tiket->delete()) {

            foreach ($tiket->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "tiket",
                "action" => "search"
            ));
        }

        $this->flash->success("tiket was deleted successfully");

        return $this->dispatcher->forward(array(
            "controller" => "tiket",
            "action" => "index"
        ));
    }

}
