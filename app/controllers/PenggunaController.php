<?php
/**
 * @author Fery Putra Tarigan <kutanaridesain@gmail.com>
 * @link http://kutanari.com, http://kutanaridesain.com
 * @since 2015
 * @license MIT License
 */

 
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;

class PenggunaController extends ControllerBase
{

    /**
     * Index action
     */
    public function indexAction()
    {
        $this->persistent->parameters = null;
    }

    /**
     * Searches for pengguna
     */
    public function searchAction()
    {

        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, "Pengguna", $_POST);
            $this->persistent->parameters = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = $this->persistent->parameters;
        if (!is_array($parameters)) {
            $parameters = array();
        }
        $parameters["order"] = "username";

        $pengguna = Pengguna::find($parameters);
        if (count($pengguna) == 0) {
            $this->flash->notice("The search did not find any pengguna");

            return $this->dispatcher->forward(array(
                "controller" => "pengguna",
                "action" => "index"
            ));
        }

        $paginator = new Paginator(array(
            "data" => $pengguna,
            "limit"=> 10,
            "page" => $numberPage
        ));

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Displayes the creation form
     */
    public function newAction()
    {

    }

    /**
     * Edits a pengguna
     *
     * @param string $username
     */
    public function editAction($username)
    {

        if (!$this->request->isPost()) {

            $pengguna = Pengguna::findFirstByusername($username);
            if (!$pengguna) {
                $this->flash->error("pengguna was not found");

                return $this->dispatcher->forward(array(
                    "controller" => "pengguna",
                    "action" => "index"
                ));
            }

            $this->view->username = $pengguna->username;

            $this->tag->setDefault("username", $pengguna->getUsername());
            $this->tag->setDefault("email", $pengguna->getEmail());
            $this->tag->setDefault("password", $pengguna->getPassword());
            $this->tag->setDefault("create_time", $pengguna->getCreateTime());
            $this->tag->setDefault("id_pengguna", $pengguna->getIdPengguna());
            $this->tag->setDefault("refrensi", $pengguna->getRefrensi());
            
        }
    }

    /**
     * Creates a new pengguna
     */
    public function createAction()
    {

        if (!$this->request->isPost()) {
            return $this->dispatcher->forward(array(
                "controller" => "pengguna",
                "action" => "index"
            ));
        }

        $pengguna = new Pengguna();

        $pengguna->setUsername($this->request->getPost("username"));
        $pengguna->setEmail($this->request->getPost("email", "email"));
        $pengguna->setPassword($this->request->getPost("password"));
        $pengguna->setCreateTime($this->request->getPost("create_time"));
        $pengguna->setRefrensi($this->request->getPost("refrensi"));
        

        if (!$pengguna->save()) {
            foreach ($pengguna->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "pengguna",
                "action" => "new"
            ));
        }

        $this->flash->success("pengguna was created successfully");

        return $this->dispatcher->forward(array(
            "controller" => "pengguna",
            "action" => "index"
        ));

    }

    /**
     * Saves a pengguna edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            return $this->dispatcher->forward(array(
                "controller" => "pengguna",
                "action" => "index"
            ));
        }

        $username = $this->request->getPost("username");

        $pengguna = Pengguna::findFirstByusername($username);
        if (!$pengguna) {
            $this->flash->error("pengguna does not exist " . $username);

            return $this->dispatcher->forward(array(
                "controller" => "pengguna",
                "action" => "index"
            ));
        }

        $pengguna->setUsername($this->request->getPost("username"));
        $pengguna->setEmail($this->request->getPost("email", "email"));
        $pengguna->setPassword($this->request->getPost("password"));
        $pengguna->setCreateTime($this->request->getPost("create_time"));
        $pengguna->setRefrensi($this->request->getPost("refrensi"));
        

        if (!$pengguna->save()) {

            foreach ($pengguna->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "pengguna",
                "action" => "edit",
                "params" => array($pengguna->username)
            ));
        }

        $this->flash->success("pengguna was updated successfully");

        return $this->dispatcher->forward(array(
            "controller" => "pengguna",
            "action" => "index"
        ));

    }

    /**
     * Deletes a pengguna
     *
     * @param string $username
     */
    public function deleteAction($username)
    {

        $pengguna = Pengguna::findFirstByusername($username);
        if (!$pengguna) {
            $this->flash->error("pengguna was not found");

            return $this->dispatcher->forward(array(
                "controller" => "pengguna",
                "action" => "index"
            ));
        }

        if (!$pengguna->delete()) {

            foreach ($pengguna->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "pengguna",
                "action" => "search"
            ));
        }

        $this->flash->success("pengguna was deleted successfully");

        return $this->dispatcher->forward(array(
            "controller" => "pengguna",
            "action" => "index"
        ));
    }

}
