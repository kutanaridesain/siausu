<?php
/**
 * @author Fery Putra Tarigan <kutanaridesain@gmail.com>
 * @link http://kutanari.com, http://kutanaridesain.com
 * @since 2015
 * @license MIT License
 */

 
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;

class RuanganController extends ControllerBase
{

    /**
     * Index action
     */
    public function indexAction()
    {
        $this->persistent->parameters = null;
        $numberPage = 1;

        $parameters = $this->persistent->parameters;
        if (!is_array($parameters)) {
            $parameters = array();
        }
        $parameters["order"] = "id_ruangan";

        $ruangan = Ruangan::find($parameters);
        if (count($ruangan) == 0) {
            $this->flash->notice("Data ruangan masih kosong");
        }

        $paginator = new Paginator(array(
            "data" => $ruangan,
            "limit"=> 10,
            "page" => $numberPage
        ));

        $this->view->page = $paginator->getPaginate();
        $this->assets->addJs('public/js/angular.js');
        $this->assets->addJs('public/js/siausu.js');
    }

    /**
     * Searches for ruangan
     */
    public function searchAction()
    {

        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, "Ruangan", $_POST);
            $this->persistent->parameters = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = $this->persistent->parameters;
        if (!is_array($parameters)) {
            $parameters = array();
        }
        $parameters["order"] = "id_ruangan";

        $ruangan = Ruangan::find($parameters);
        if (count($ruangan) == 0) {
            $this->flash->notice("The search did not find any ruangan");

            return $this->dispatcher->forward(array(
                "controller" => "ruangan",
                "action" => "index"
            ));
        }

        $paginator = new Paginator(array(
            "data" => $ruangan,
            "limit"=> 10,
            "page" => $numberPage
        ));

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Displayes the creation form
     */
    public function newAction()
    {

    }

    /**
     * Edits a ruangan
     *
     * @param string $id_ruangan
     */
    public function editAction($id_ruangan)
    {

        if (!$this->request->isPost()) {

            $ruangan = Ruangan::findFirstByid_ruangan($id_ruangan);
            if (!$ruangan) {
                $this->flash->error("ruangan was not found");

                return $this->dispatcher->forward(array(
                    "controller" => "ruangan",
                    "action" => "index"
                ));
            }

            $this->view->id_ruangan = $ruangan->id_ruangan;

            $this->tag->setDefault("id_ruangan", $ruangan->getIdRuangan());
            $this->tag->setDefault("nama_ruangan", $ruangan->getNamaRuangan());
            $this->tag->setDefault("kapasitas_ruangan", $ruangan->getKapasitasRuangan());
            
        }
    }

    /**
     * Creates a new ruangan
     */
    public function createAction()
    {

        if (!$this->request->isPost()) {
            return $this->dispatcher->forward(array(
                "controller" => "ruangan",
                "action" => "index"
            ));
        }

        $ruangan = new Ruangan();

        $ruangan->setIdRuangan($this->request->getPost("id_ruangan"));
        $ruangan->setNamaRuangan($this->request->getPost("nama_ruangan"));
        $ruangan->setKapasitasRuangan($this->request->getPost("kapasitas_ruangan"));
        

        if (!$ruangan->save()) {
            foreach ($ruangan->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "ruangan",
                "action" => "new"
            ));
        }

        $this->flash->success("ruangan was created successfully");

        return $this->dispatcher->forward(array(
            "controller" => "ruangan",
            "action" => "index"
        ));

    }

    /**
     * Saves a ruangan edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            return $this->dispatcher->forward(array(
                "controller" => "ruangan",
                "action" => "index"
            ));
        }

        $id_ruangan = $this->request->getPost("id_ruangan");

        $ruangan = Ruangan::findFirstByid_ruangan($id_ruangan);
        if (!$ruangan) {
            $this->flash->error("ruangan does not exist " . $id_ruangan);

            return $this->dispatcher->forward(array(
                "controller" => "ruangan",
                "action" => "index"
            ));
        }

        $ruangan->setIdRuangan($this->request->getPost("id_ruangan"));
        $ruangan->setNamaRuangan($this->request->getPost("nama_ruangan"));
        $ruangan->setKapasitasRuangan($this->request->getPost("kapasitas_ruangan"));
        

        if (!$ruangan->save()) {

            foreach ($ruangan->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "ruangan",
                "action" => "edit",
                "params" => array($ruangan->id_ruangan)
            ));
        }

        $this->flash->success("ruangan was updated successfully");

        return $this->dispatcher->forward(array(
            "controller" => "ruangan",
            "action" => "index"
        ));

    }

    /**
     * Deletes a ruangan
     *
     * @param string $id_ruangan
     */
    public function deleteAction($id_ruangan)
    {

        $ruangan = Ruangan::findFirstByid_ruangan($id_ruangan);
        if (!$ruangan) {
            $this->flash->error("ruangan was not found");

            return $this->dispatcher->forward(array(
                "controller" => "ruangan",
                "action" => "index"
            ));
        }

        if (!$ruangan->delete()) {

            foreach ($ruangan->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "ruangan",
                "action" => "search"
            ));
        }

        $this->flash->success("ruangan was deleted successfully");

        return $this->dispatcher->forward(array(
            "controller" => "ruangan",
            "action" => "index"
        ));
    }

    /**
     * Handle all AJAX request
     *
     * @param string $id_ruangan
     */
    public function ajaxAction($id_ruangan = null)
    {
        $this->view->disable();
        $messages = array();
        $data = array();
        $data['status'] = true;
    
        if($this->request->isPost()){
            //proses penambahan data
            $post = $this->request->getJsonRawBody();

            $ruangan = new Ruangan();
            $ruangan->setNamaRuangan($post->nama_ruangan);
            $ruangan->setKapasitasRuangan($post->kapasitas_ruangan);
    
            if (!$ruangan->save()) {
                foreach ($ruangan->getMessages() as $message) {
                    $messages[] = $message;
                }
                $data['message'] = $messages;
                $data['status'] = false;
            }
        }elseif($this->request->isPut()){
            //proses pengubahan data
            $put = $this->request->getJsonRawBody();
            $ruangan = Ruangan::findFirstByid_ruangan($put->id);
            if (!$ruangan) {
                $messages[] = "ruangan tidak ditemukan " . $put->id;
                $data['status'] = false;
            }
    
            $ruangan->setNamaRuangan($put->nama_ruangan);
            $ruangan->setKapasitasRuangan($put->kapasitas_ruangan);
    
            if (!$ruangan->save()) {
                $data['status'] = false;
                foreach ($ruangan->getMessages() as $message) {
                    $messages[] = $message;
                }
            }
            $data['message'] = $messages;
        }elseif($this->request->isDelete()){
            //proses hapus data
            $delete = $this->request->getJsonRawBody();
            $ruangan = Ruangan::findFirstByid_ruangan($id_ruangan);
            if (!$ruangan) {
                $messages[] = "data ruangan tidak ditemukan";
                $data['status'] = false;
            }
            try {
                $ruangan->delete();
            } catch (Exception $e) {
                $messages[] = $e->getMessage();
                $data['status'] = false;
            }
            $data['message'] = $messages;
        }else{
            //mengambil data
            $ruangan = Ruangan::find();
            foreach ($ruangan as $item) {
                $data['data'][] = array(
                    'id' => $item->getIdRuangan(),
                    'nama_ruangan' => $item->getNamaRuangan(),
                    'kapasitas_ruangan' => (int)$item->getKapasitasRuangan()
                    );
            }
        }
    
        $this->response->setContentType('application/json', 'UTF-8');
        $this->response->setContent(json_encode($data));
        $this->response->send();
    }

}
