<?php
/**
 * @author Fery Putra Tarigan <kutanaridesain@gmail.com>
 * @link http://kutanari.com, http://kutanaridesain.com
 * @since 2015
 * @license MIT License
 */

 
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;

class DosenController extends ControllerBase
{
    public function initialize()
    {
        parent::initialize();
        $this->tag->appendTitle('| Agama');
    }

    /**
     * Index action
     */
    public function indexAction()
    {
        $parameters = null;
        $numberPage = 1;
        if (!is_array($parameters)) {
            $parameters = array();
        }
        $parameters["order"] = "nip";

        $dosen = Dosen::find($parameters);
        if (count($dosen) == 0) {
            $this->flash->notice("Data dosen masih kosong");
        }

        $paginator = new Paginator(array(
            "data" => $dosen,
            "limit"=> 10,
            "page" => $numberPage
        ));

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Searches for dosen
     */
    public function searchAction()
    {

        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, "Dosen", $_POST);
            $this->persistent->parameters = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = $this->persistent->parameters;
        if (!is_array($parameters)) {
            $parameters = array();
        }
        $parameters["order"] = "nip";

        $dosen = Dosen::find($parameters);
        if (count($dosen) == 0) {
            $this->flash->notice("The search did not find any dosen");

            return $this->dispatcher->forward(array(
                "controller" => "dosen",
                "action" => "index"
            ));
        }

        $paginator = new Paginator(array(
            "data" => $dosen,
            "limit"=> 10,
            "page" => $numberPage
        ));

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Displayes the creation form
     */
    public function newAction()
    {

    }

    /**
     * Edits a dosen
     *
     * @param string $nip
     */
    public function editAction($nip)
    {

        if (!$this->request->isPost()) {

            $dosen = Dosen::findFirstBynip($nip);
            if (!$dosen) {
                $this->flash->error("dosen was not found");

                return $this->dispatcher->forward(array(
                    "controller" => "dosen",
                    "action" => "index"
                ));
            }

            $this->view->nip = $dosen->getNip();
            $this->view->nama = $dosen->getNamaDosen();

            $this->tag->setDefault("nip", $dosen->getNip());
            $this->tag->setDefault("nama_dosen", $dosen->getNamaDosen());
            $this->tag->setDefault("jenis_kelamin", $dosen->getJenisKelamin());
            $this->tag->setDefault("alamat", $dosen->getAlamat());
            $this->tag->setDefault("email", $dosen->getEmail());
            $this->tag->setDefault("telp", $dosen->getTelp());
            $this->tag->setDefault("status", $dosen->getStatus());
            
        }
    }

    /**
     * Creates a new dosen
     */
    public function createAction()
    {

        if (!$this->request->isPost()) {
            return $this->dispatcher->forward(array(
                "controller" => "dosen",
                "action" => "index"
            ));
        }

        $dosen = new Dosen();

        $dosen->setNip($this->request->getPost("nip"));
        $dosen->setNamaDosen($this->request->getPost("nama_dosen"));
        $dosen->setJenisKelamin($this->request->getPost("jenis_kelamin"));
        $dosen->setAlamat($this->request->getPost("alamat"));
        $dosen->setEmail($this->request->getPost("email", "email"));
        $dosen->setTelp($this->request->getPost("telp"));
        $dosen->setStatus($this->request->getPost("status"));
        

        if (!$dosen->save()) {
            foreach ($dosen->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "dosen",
                "action" => "new"
            ));
        }

        $this->flash->success("dosen was created successfully");

        return $this->dispatcher->forward(array(
            "controller" => "dosen",
            "action" => "index"
        ));

    }

    /**
     * Saves a dosen edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            return $this->dispatcher->forward(array(
                "controller" => "dosen",
                "action" => "index"
            ));
        }

        $nip = $this->request->getPost("nip");

        $dosen = Dosen::findFirstBynip($nip);
        if (!$dosen) {
            $this->flash->error("dosen does not exist " . $nip);

            return $this->dispatcher->forward(array(
                "controller" => "dosen",
                "action" => "index"
            ));
        }

        $dosen->setNip($this->request->getPost("nip"));
        $dosen->setNamaDosen($this->request->getPost("nama_dosen"));
        $dosen->setJenisKelamin($this->request->getPost("jenis_kelamin"));
        $dosen->setAlamat($this->request->getPost("alamat"));
        $dosen->setEmail($this->request->getPost("email", "email"));
        $dosen->setTelp($this->request->getPost("telp"));
        $dosen->setStatus($this->request->getPost("status"));
        

        if (!$dosen->save()) {

            foreach ($dosen->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "dosen",
                "action" => "edit",
                "params" => array($dosen->nip)
            ));
        }

        $this->flash->success("dosen was updated successfully");

        return $this->dispatcher->forward(array(
            "controller" => "dosen",
            "action" => "index"
        ));

    }

    /**
     * Deletes a dosen
     *
     * @param string $nip
     */
    public function deleteAction($nip)
    {

        $dosen = Dosen::findFirstBynip($nip);
        if (!$dosen) {
            $this->flash->error("dosen was not found");

            return $this->dispatcher->forward(array(
                "controller" => "dosen",
                "action" => "index"
            ));
        }

        if (!$dosen->delete()) {

            foreach ($dosen->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "dosen",
                "action" => "search"
            ));
        }

        $this->flash->success("dosen was deleted successfully");

        return $this->dispatcher->forward(array(
            "controller" => "dosen",
            "action" => "index"
        ));
    }

    public function detailAction($nip)
    {
        $dosen = Dosen::findFirstBynip($nip);
        $this->view->dosen = $dosen;
    }

}
