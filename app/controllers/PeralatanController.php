<?php
/**
 * @author Fery Putra Tarigan <kutanaridesain@gmail.com>
 * @link http://kutanari.com, http://kutanaridesain.com
 * @since 2015
 * @license MIT License
 */


class PeralatanController extends ControllerBase
{
    public function initialize()
    {
        // $this->view->setTemplateAfter('main');
        Phalcon\Tag::setTitle('Unggah DNA');
        parent::initialize();
    }

    public function indexAction()
    {
    }

    public function importdnaAction()
    {
        if ($this->request->isPost() && $this->request->hasFiles()) {
            $file = $this->request->getUploadedFiles();
            $dnaCsvFile = $file[0]->getTempName();

            $row = 1;
            if (($handle = fopen($dnaCsvFile, "r")) !== FALSE) {
                // $arrObjDna = array();
                while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                    if($row == 1){
                      $row++;
                      continue; 
                    }

                    // $num = count($data);
                    // $objDna = new stdClass();
                    // $objDna->nim = $data[0];
                    // $objDna->nama = $data[1];
                    // $objDna->tugas = $data[2];
                    // $objDna->kuis = $data[3];
                    // $objDna->uts = $data[4];
                    // $objDna->uas = $data[5];
                    // $objDna->angka = $data[6];
                    // $objDna->huruf = $data[7];
                    // $arrObjDna[] = $objDna;

                    $modelDna = new Dna();
                    $modelDna->setNim($data[0]);
                    $modelDna->setNama($data[1]);
                    $modelDna->setTugas($data[2]);
                    $modelDna->setKuis($data[3]);
                    $modelDna->setUts($data[4]);
                    $modelDna->setUas($data[5]);
                    $modelDna->setAngka($data[6]);
                    $modelDna->setHuruf($data[7]);
                    $modelDna->setIdPengampu($this->request->getPost('pengampu_id_pengampu'));
                    $param = sprintf("nim='%s' AND pengampu_id_pengampu=%s", $data[0], $this->request->getPost('pengampu_id_pengampu'));
                    $count = $modelDna->count($param);
                    if($count == 0){
                        try{
                            if($modelDna->save()){
                                printf("<p> nilai untuk nim %s berhasil diimport <br /></p>\n", $data[0]);
                                $row++;
                            }
                        }catch(Exception $e){
                            printf("<p> nilai untuk nim %s gagal diimport (sudah ada)<br /></p>\n", $data[0]);
                            $row++;
                        }
                    }else{
                        printf("<p> nilai untuk nim %s gagal diimport (sudah ada)<br /></p>\n", $data[0]);
                            $row++;
                    }
                }
                fclose($handle);
            }
        }
        $this->view->jadwalkuliah = Pengampu::find();
        $this->assets->addJs('public/js/plugins/select/bootstrap-select.min.js');
        $this->assets->addCss('public/js/plugins/select/bootstrap-select.min.css');
    }

}

