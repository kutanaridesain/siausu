<?php
/**
 * @author Fery Putra Tarigan <kutanaridesain@gmail.com>
 * @link http://kutanari.com, http://kutanaridesain.com
 * @since 2015
 * @license MIT License
 */

 
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;

class PengampuController extends ControllerBase
{

    /**
     * Index action
     */
    public function indexAction()
    {
        $this->persistent->parameters = null;
        $numberPage = 1;

        $parameters = $this->persistent->parameters;
        if (!is_array($parameters)) {
            $parameters = array();
        }
        $parameters["order"] = "id_pengampu";

        $pengampu = Pengampu::find($parameters);
        if (count($pengampu) == 0) {
            $this->flash->notice("The search did not find any pengampu");
        }

        $paginator = new Paginator(array(
            "data" => $pengampu,
            "limit"=> 10,
            "page" => $numberPage
        ));

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Searches for pengampu
     */
    public function searchAction()
    {

        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, "Pengampu", $_POST);
            $this->persistent->parameters = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = $this->persistent->parameters;
        if (!is_array($parameters)) {
            $parameters = array();
        }
        $parameters["order"] = "id_pengampu";

        $pengampu = Pengampu::find($parameters);
        if (count($pengampu) == 0) {
            $this->flash->notice("The search did not find any pengampu");

            return $this->dispatcher->forward(array(
                "controller" => "pengampu",
                "action" => "index"
            ));
        }

        $paginator = new Paginator(array(
            "data" => $pengampu,
            "limit"=> 10,
            "page" => $numberPage
        ));

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Displayes the creation form
     */
    public function newAction()
    {
        $modelDosen = Dosen::find();
        $dosen = array();
        foreach ($modelDosen as $itemDosen) {
            $dosen[$itemDosen->getNip()] = sprintf('%s - %s', $itemDosen->getNamaDosen(), $itemDosen->getNip());
        }

        $modelMatakuliah = Matakuliah::find();
        $matakuliah = array();
        foreach ($modelMatakuliah as $itemMatakuliah) {
            $matakuliah[$itemMatakuliah->getIdMatakuliah()] = sprintf('%s - %s', $itemMatakuliah->getNamaMatakuliah(), $itemMatakuliah->getKodeMatakuliah());
        }


        $this->view->dosen = $dosen;
        $this->view->matakuliah = $matakuliah;
    }

    /**
     * Edits a pengampu
     *
     * @param string $id_pengampu
     */
    public function editAction($id_pengampu)
    {

        if (!$this->request->isPost()) {

            $pengampu = Pengampu::findFirstByid_pengampu($id_pengampu);
            if (!$pengampu) {
                $this->flash->error("pengampu was not found");

                return $this->dispatcher->forward(array(
                    "controller" => "pengampu",
                    "action" => "index"
                ));
            }

            $this->view->id_pengampu = $id_pengampu;
            $this->view->dosen = Dosen::find();
            $this->view->matakuliah = Matakuliah::find();

            $this->tag->setDefault("id_pengampu", $pengampu->getIdPengampu());
            $this->tag->setDefault("dosen_nip", $pengampu->getDosenNip());
            $this->tag->setDefault("matakuliah_id_matakuliah", $pengampu->getMatakuliahIdMatakuliah());
            
        }
    }

    /**
     * Creates a new pengampu
     */
    public function createAction()
    {

        if (!$this->request->isPost()) {
            return $this->dispatcher->forward(array(
                "controller" => "pengampu",
                "action" => "index"
            ));
        }

        $pengampu = new Pengampu();

        $pengampu->setDosenNip($this->request->getPost("dosen_nip"));
        $pengampu->setMatakuliahIdMatakuliah($this->request->getPost("matakuliah_id_matakuliah"));
        

        if (!$pengampu->save()) {
            foreach ($pengampu->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "pengampu",
                "action" => "new"
            ));
        }

        $this->flash->success("pengampu was created successfully");

        return $this->dispatcher->forward(array(
            "controller" => "pengampu",
            "action" => "index"
        ));

    }

    /**
     * Saves a pengampu edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            return $this->dispatcher->forward(array(
                "controller" => "pengampu",
                "action" => "index"
            ));
        }

        $id_pengampu = $this->request->getPost("id_pengampu");

        $pengampu = Pengampu::findFirstByid_pengampu($id_pengampu);
        if (!$pengampu) {
            $this->flash->error("pengampu does not exist " . $id_pengampu);

            return $this->dispatcher->forward(array(
                "controller" => "pengampu",
                "action" => "index"
            ));
        }

        $pengampu->setDosenNip($this->request->getPost("dosen_nip"));
        $pengampu->setMatakuliahIdMatakuliah($this->request->getPost("matakuliah_id_matakuliah"));
        

        if (!$pengampu->save()) {

            foreach ($pengampu->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "pengampu",
                "action" => "edit",
                "params" => array($pengampu->id_pengampu)
            ));
        }

        $this->flash->success("pengampu was updated successfully");

        return $this->dispatcher->forward(array(
            "controller" => "pengampu",
            "action" => "index"
        ));

    }

    /**
     * Deletes a pengampu
     *
     * @param string $id_pengampu
     */
    public function deleteAction($id_pengampu)
    {

        $pengampu = Pengampu::findFirstByid_pengampu($id_pengampu);
        if (!$pengampu) {
            $this->flash->error("pengampu was not found");

            return $this->dispatcher->forward(array(
                "controller" => "pengampu",
                "action" => "index"
            ));
        }

        if (!$pengampu->delete()) {

            foreach ($pengampu->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "pengampu",
                "action" => "search"
            ));
        }

        $this->flash->success("pengampu was deleted successfully");

        return $this->dispatcher->forward(array(
            "controller" => "pengampu",
            "action" => "index"
        ));
    }

}
