<?php
 
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;

class KhsController extends ControllerBase
{

    /**
     * Index action
     */
    public function indexAction()
    {
        $this->persistent->parameters = null;
    }

    /**
     * Searches for khs
     */
    public function searchAction()
    {

        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, "Khs", $_POST);
            $this->persistent->parameters = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = $this->persistent->parameters;
        if (!is_array($parameters)) {
            $parameters = array();
        }
        $parameters["order"] = "id_khs";

        $khs = Khs::find($parameters);
        if (count($khs) == 0) {
            $this->flash->notice("The search did not find any khs");

            return $this->dispatcher->forward(array(
                "controller" => "khs",
                "action" => "index"
            ));
        }

        $paginator = new Paginator(array(
            "data" => $khs,
            "limit"=> 10,
            "page" => $numberPage
        ));

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Displayes the creation form
     */
    public function newAction()
    {

    }

    /**
     * Edits a kh
     *
     * @param string $id_khs
     */
    public function editAction($id_khs)
    {

        if (!$this->request->isPost()) {

            $kh = Khs::findFirstByid_khs($id_khs);
            if (!$kh) {
                $this->flash->error("kh was not found");

                return $this->dispatcher->forward(array(
                    "controller" => "khs",
                    "action" => "index"
                ));
            }

            $this->view->id_khs = $kh->id_khs;

            $this->tag->setDefault("id_khs", $kh->getIdKhs());
            $this->tag->setDefault("ta", $kh->getTa());
            $this->tag->setDefault("semester", $kh->getSemester());
            $this->tag->setDefault("mahasiswa_nim", $kh->getMahasiswaNim());
            $this->tag->setDefault("jadwal_id_jadwal", $kh->getJadwalIdJadwal());
            
        }
    }

    /**
     * Creates a new kh
     */
    public function createAction()
    {

        if (!$this->request->isPost()) {
            return $this->dispatcher->forward(array(
                "controller" => "khs",
                "action" => "index"
            ));
        }

        $kh = new Khs();

        $kh->setTa($this->request->getPost("ta"));
        $kh->setSemester($this->request->getPost("semester"));
        $kh->setMahasiswaNim($this->request->getPost("mahasiswa_nim"));
        $kh->setJadwalIdJadwal($this->request->getPost("jadwal_id_jadwal"));
        

        if (!$kh->save()) {
            foreach ($kh->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "khs",
                "action" => "new"
            ));
        }

        $this->flash->success("kh was created successfully");

        return $this->dispatcher->forward(array(
            "controller" => "khs",
            "action" => "index"
        ));

    }

    /**
     * Saves a kh edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            return $this->dispatcher->forward(array(
                "controller" => "khs",
                "action" => "index"
            ));
        }

        $id_khs = $this->request->getPost("id_khs");

        $kh = Khs::findFirstByid_khs($id_khs);
        if (!$kh) {
            $this->flash->error("kh does not exist " . $id_khs);

            return $this->dispatcher->forward(array(
                "controller" => "khs",
                "action" => "index"
            ));
        }

        $kh->setTa($this->request->getPost("ta"));
        $kh->setSemester($this->request->getPost("semester"));
        $kh->setMahasiswaNim($this->request->getPost("mahasiswa_nim"));
        $kh->setJadwalIdJadwal($this->request->getPost("jadwal_id_jadwal"));
        

        if (!$kh->save()) {

            foreach ($kh->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "khs",
                "action" => "edit",
                "params" => array($kh->id_khs)
            ));
        }

        $this->flash->success("kh was updated successfully");

        return $this->dispatcher->forward(array(
            "controller" => "khs",
            "action" => "index"
        ));

    }

    /**
     * Deletes a kh
     *
     * @param string $id_khs
     */
    public function deleteAction($id_khs)
    {

        $kh = Khs::findFirstByid_khs($id_khs);
        if (!$kh) {
            $this->flash->error("kh was not found");

            return $this->dispatcher->forward(array(
                "controller" => "khs",
                "action" => "index"
            ));
        }

        if (!$kh->delete()) {

            foreach ($kh->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "khs",
                "action" => "search"
            ));
        }

        $this->flash->success("kh was deleted successfully");

        return $this->dispatcher->forward(array(
            "controller" => "khs",
            "action" => "index"
        ));
    }


    public function infoAction()
    {
        
    }

}
