<?php
/**
 * @author Fery Putra Tarigan <kutanaridesain@gmail.com>
 * @link http://kutanari.com, http://kutanaridesain.com
 * @since 2015
 * @license MIT License
 */

use Phalcon\Mvc\Controller;

/**
 * 
 */
class ControllerBase extends Controller
{
    /**
     * [initialize description]
     * @return [type]
     */
    public function initialize()
    {
        $this->tag->prependTitle('SIAUSUDTE');
        if(!$this->session->has('auth')){
            return $this->response->redirect('login');
        }
        $this->view->auth = $this->session->get('auth');
        $this->view->namaControl = $this->dispatcher->getControllerName();
        $this->view->namaAction = "";
        $this->view->title = "SIA DTE USU";
        $this->view->customScript = "";
        $this->view->counter = 0;
        $this->view->backendRole = array('admin', 'pegawai');
    }
}
