<?php
/**
 * @author Fery Putra Tarigan <kutanaridesain@gmail.com>
 * @link http://kutanari.com, http://kutanaridesain.com
 * @since 2015
 * @license MIT License
 */

use Phalcon\Mvc\View;
use Phalcon\Mvc\Controller;

/**
 * login controller handling login and logout process
 */
class LoginController extends Controller
{
    /**
     * private function to register ression after success login
     * @param  object $user object of user to register to session
     * @return void   
     */
    private function _registerSession($user)
    {
        $this->session->set('auth', array(
            'id' => $user->id,
            'nama' => $user->nama,
            'role' => $user->role
        ));
    }

    /**
     * index action as login page and login process
     * @return void
     */
    public function indexAction()
    {
        $this->view->setRenderLevel(View::LEVEL_LAYOUT);
        $post = $this->request->getJsonRawBody();
        if($this->request->isPost()){
            $username = $this->request->getPost('username');
            $password = $this->request->getPost('password');
            //TODO:password harus di hash sha1() atau md5()
            $params = array("username='{$username}' AND password='{$password}'");
            $otentikasi = Otentikasi::findFirst($params);
            if($otentikasi !== FALSE){
                $role = $otentikasi->getRole();
                $user = new stdClass();
                switch ($role) {
                    case 'd':
                        $dosen = Dosen::findFirstBynip($username);
                        $user->id = $username;
                        $user->nama = $dosen->getNamaDosen();
                        $user->role = 'dosen';
                        break;
                    case 'p':
                        $pegawai = Pegawai::findFirstBynip($username);
                        $user->id = $username;
                        $user->nama = $pegawai->getNamaPegawai();
                        $user->role = 'pegawai';
                        break;
                    case 'm':
                        $mahasiswa = Mahasiswa::findFirstBynim($username);
                        $user->id = $username;
                        $user->nama = $mahasiswa->getNama();
                        $user->role = 'mahasiswa';
                        break;
                    default:
                        $user->id = $username;
                        $user->nama = $username;
                        $user->role = 'admin';
                        break;
                }
                $this->_registerSession($user);
                $this->flash->outputMessage('benar', 'Selamat datang ' . $user->nama . ' anda adalah seorang '.$user->role);
                return $this->response->redirect("");
            }else{
                $this->flash->outputMessage('salah', 'Login salah, periksa kembali akun anda!');
            }
        }
    }

    /**
     * logout the system
     * @return void
     */
    public function logoutAction()
    {
        $this->session->remove('auth');
        $this->response->redirect("login");
    }
}