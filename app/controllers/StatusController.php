<?php
/**
 * @author Fery Putra Tarigan <kutanaridesain@gmail.com>
 * @link http://kutanari.com, http://kutanaridesain.com
 * @since 2015
 * @license MIT License
 */

 
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;

class StatusController extends ControllerBase
{
    /**
     * Index action
     */
    public function indexAction()
    {
        $this->persistent->parameters = null;

        $status = Status::find();
        if(count($status) == 0){
            $this->flash->notice("Data status masih kosong.");
        }

        $paginator = new Paginator(array(
            "data"=>$status,
            "limit"=> 10,
            "page"=> $numberPage
        ));

        $this->view->page = $paginator->getPaginate();
        $this->assets->addJs('public/js/angular.js');
        $this->assets->addJs('public/js/siausu.js');

    }

    /**
     * Searches for status
     */
    public function searchAction()
    {

        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, "Status", $_POST);
            $this->persistent->parameters = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = $this->persistent->parameters;
        if (!is_array($parameters)) {
            $parameters = array();
        }
        $parameters["order"] = "id_status";

        $status = Status::find($parameters);
        if (count($status) == 0) {
            $this->flash->notice("The search did not find any status");

            return $this->dispatcher->forward(array(
                "controller" => "status",
                "action" => "index"
            ));
        }

        $paginator = new Paginator(array(
            "data" => $status,
            "limit"=> 10,
            "page" => $numberPage
        ));

        $this->view->page = $paginator->getPaginate();
        $this->view->namaAction = 'Pencarian data status';
    }

    /**
     * Displayes the creation form
     */
    public function newAction()
    {
        $this->view->namaAction = 'Tambah status';
    }

    /**
     * Edits a statu
     *
     * @param string $id_status
     */
    public function editAction($id_status)
    {

        if (!$this->request->isPost()) {

            $statu = Status::findFirstByid_status($id_status);
            if (!$statu) {
                $this->flash->error("statu was not found");

                return $this->dispatcher->forward(array(
                    "controller" => "status",
                    "action" => "index"
                ));
            }

            $this->view->id_status = $statu->id_status;

            $this->tag->setDefault("id_status", $statu->getIdStatus());
            $this->tag->setDefault("nama_status", $statu->getNamaStatus());
            
            $this->view->namaAction = 'Ubah status';
        }
    }

    /**
     * Creates a new statu
     */
    public function createAction()
    {

        if (!$this->request->isPost()) {
            return $this->dispatcher->forward(array(
                "controller" => "status",
                "action" => "index"
            ));
        }

        $statu = new Status();

        $statu->setNamaStatus($this->request->getPost("nama_status"));
        

        if (!$statu->save()) {
            foreach ($statu->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "status",
                "action" => "new"
            ));
        }

        $this->flash->success("statu was created successfully");

        return $this->dispatcher->forward(array(
            "controller" => "status",
            "action" => "index"
        ));

    }

    /**
     * Saves a statu edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            return $this->dispatcher->forward(array(
                "controller" => "status",
                "action" => "index"
            ));
        }

        $id_status = $this->request->getPost("id_status");

        $statu = Status::findFirstByid_status($id_status);
        if (!$statu) {
            $this->flash->error("statu does not exist " . $id_status);

            return $this->dispatcher->forward(array(
                "controller" => "status",
                "action" => "index"
            ));
        }

        $statu->setNamaStatus($this->request->getPost("nama_status"));
        

        if (!$statu->save()) {

            foreach ($statu->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "status",
                "action" => "edit",
                "params" => array($statu->id_status)
            ));
        }

        $this->flash->success("statu was updated successfully");

        return $this->dispatcher->forward(array(
            "controller" => "status",
            "action" => "index"
        ));

    }

    /**
     * Deletes a statu
     *
     * @param string $id_status
     */
    public function deleteAction($id_status)
    {

        $statu = Status::findFirstByid_status($id_status);
        if (!$statu) {
            $this->flash->error("statu was not found");

            return $this->dispatcher->forward(array(
                "controller" => "status",
                "action" => "index"
            ));
        }

        if (!$statu->delete()) {

            foreach ($statu->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "status",
                "action" => "search"
            ));
        }

        $this->flash->success("statu was deleted successfully");

        return $this->dispatcher->forward(array(
            "controller" => "status",
            "action" => "index"
        ));
    }

    /**
    * handler untuk reques ajax dari angular js
    **/
    public function ajaxAction($id_status)
    {
        $this->view->disable();
        $messages = array();
        $data = array();
        $data['status'] = true;

        if ($this->request->isPost()) {
            $Status = new Status();

            $post = $this->request->getJsonRawBody();
            $Status->setNamaStatus($post->nama_status);
        

            if (!$Status->save()) {
                foreach ($Status->getMessages() as $message) {
                    $messages[] = $message;
                }
                $data['message'] = $messages;
                $data['status'] = false;
            }
        }elseif($this->request->isPut()){
            $put = $this->request->getJsonRawBody();

            $Status = Status::findFirstByid_status($put->id);
            if (!$Status) {
                $messages[] = "Status does not exist " . $put->id;
                $data['status'] = false;
            }

            $Status->setNamaStatus($put->nama_status);

            if (!$Status->save()) {
                $data['status'] = false;
                foreach ($Status->getMessages() as $message) {
                    $messages[] = $message;
                }
            }
            $data['message'] = $messages;
        }elseif($this->request->isDelete()){
            $delete = $this->request->getJsonRawBody();
            $Status = Status::findFirstByid_status($id_status);
            if (!$Status) {
                $messages[] = "data Status tidak ditemukan";
                $data['status'] = false;
            }
            try {
                $Status->delete();
            } catch (Exception $e) {
                $messages[] = $e->getMessage();
                $data['status'] = false;
            }
            $data['message'] = $messages;
        }else{
            $Status = Status::find();
            foreach ($Status as $item) {
                $data['data'][] = array(
                    'id' => $item->getIdStatus(),
                    'nama_status' => $item->getNamaStatus()
                    );
            }
        }
        $this->response->setContentType('application/json', 'UTF-8');
        $this->response->setContent(json_encode($data));
        $this->response->send();
    }

}
