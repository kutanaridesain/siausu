<?php
/**
 * @author Fery Putra Tarigan <kutanaridesain@gmail.com>
 * @link http://kutanari.com, http://kutanaridesain.com
 * @since 2015
 * @license MIT License
 */

 
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;

class MahasiswaController extends ControllerBase
{
    /**
     * Index action
     */
    public function indexAction()
    {
        $modelMahasiswa = Mahasiswa::find();
        if (count($modelMahasiswa) == 0) {
            $this->flash->notice("Data mahasiswa masih kosong!");
        }
        $this->view->modelMahasiswa = $modelMahasiswa;

        $this->view->customScript = '
            jQuery("#modalDetail").on("hide.bs.modal", function(){
                $(this).data("modal", null);
            });
            jQuery("#modalDetail").on("show.bs.modal", function(){
                $("#modalDetail .modal-body").html("<div class=\"progress progress-striped active\"><div class=\"progress-bar\"  role=\"progressbar\" aria-valuenow=\"45\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 45%\"><span class=\"sr-only\">45% Complete</span></div></div>");
            });
        ';
    }

    /**
     * Searches for mahasiswa
     */
    public function searchAction()
    {

        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, "Mahasiswa", $_POST);
            $this->persistent->parameters = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = $this->persistent->parameters;
        if (!is_array($parameters)) {
            $parameters = array();
        }
        $parameters["order"] = "nim";

        $mahasiswa = Mahasiswa::find($parameters);
        if (count($mahasiswa) == 0) {
            $this->flash->notice("Data mahasiswa masih kosong");

            return $this->dispatcher->forward(array(
                "controller" => "mahasiswa",
                "action" => "index"
            ));
        }

        $paginator = new Paginator(array(
            "data" => $mahasiswa,
            "limit"=> 10,
            "page" => $numberPage
        ));

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Displayes the creation form
     */
    public function newAction()
    {
        $this->loadTahun();
        $this->view->tahun = $this->loadTahun();
        $this->view->agama = Agama::find();
        $this->view->konsentrasi = Konsentrasi::find();
        $this->view->status = Status::find();
    }

    /**
     * Edits a mahasiswa
     *
     * @param string $nim
     */
    public function editAction($nim)
    {

        if (!$this->request->isPost()) {

            $mahasiswa = Mahasiswa::findFirstBynim($nim);
            if (!$mahasiswa) {
                $this->flash->error("Data mahasiswa tidak ditemukan");

                return $this->dispatcher->forward(array(
                    "controller" => "mahasiswa",
                    "action" => "index"
                ));
            }

            $this->view->nim = $mahasiswa->getNim();

            $this->tag->setDefault("nim", $mahasiswa->getNim());
            $this->tag->setDefault("nama", $mahasiswa->getNama());
            $this->tag->setDefault("angkatan", $mahasiswa->getAngkatan());
            $this->tag->setDefault("foto", $mahasiswa->getFoto());
            $this->tag->setDefault("alamat", $mahasiswa->getAlamat());
            $this->tag->setDefault("kota", $mahasiswa->getKota());
            $this->tag->setDefault("kodepos", $mahasiswa->getKodepos());
            $this->tag->setDefault("tempat_lahir", $mahasiswa->getTempatLahir());
            $this->tag->setDefault("tanggal_lahir", $mahasiswa->getTanggalLahir());
            $this->tag->setDefault("jenis_kelamin", $mahasiswa->getJenisKelamin());
            $this->tag->setDefault("no_telp", $mahasiswa->getNoTelp());
            $this->tag->setDefault("asal_sekolah", $mahasiswa->getAsalSekolah());
            $this->tag->setDefault("tahun_lulus", $mahasiswa->getTahunLulus());
            $this->tag->setDefault("jenis_kuliah", $mahasiswa->getJenisKuliah());
            $this->tag->setDefault("wali", $mahasiswa->getWali());
            $this->tag->setDefault("email", $mahasiswa->getEmail());
            $this->tag->setDefault("agama_id_agama", $mahasiswa->getAgamaIdAgama());
            $this->tag->setDefault("status_id_status", $mahasiswa->getStatusIdStatus());
            $this->tag->setDefault("konsentrasi_id_konsentrasi", $mahasiswa->getKonsentrasiIdKonsentrasi());
            $this->tag->setDefault("nama_orangtua", $mahasiswa->getNamaOrangtua());
            $this->tag->setDefault("pekerjaan_orangtua", $mahasiswa->getPekerjaanOrangtua());
            $this->tag->setDefault("alamat_orangtua", $mahasiswa->getAlamatOrangtua());
            $this->tag->setDefault("kota_orangtua", $mahasiswa->getKotaOrangtua());
            $this->tag->setDefault("kodepos_orangtua", $mahasiswa->getKodeposOrangtua());
            $this->tag->setDefault("no_telp_orangtua", $mahasiswa->getNoTelpOrangtua());
            $this->tag->setDefault("password", $mahasiswa->getPassword());
            
        }
        
        $this->view->tahun = $this->loadTahun();
        $this->view->agama = Agama::find();
        $this->view->konsentrasi = Konsentrasi::find();
        $this->view->status = Status::find();
    }

    /**
     * Creates a new mahasiswa
     */
    public function createAction()
    {

        if (!$this->request->isPost()) {
            return $this->dispatcher->forward(array(
                "controller" => "mahasiswa",
                "action" => "index"
            ));
        }

        $mahasiswa = new Mahasiswa();

        $mahasiswa->setNim($this->request->getPost("nim"));
        $mahasiswa->setNama($this->request->getPost("nama"));
        $mahasiswa->setAngkatan($this->request->getPost("angkatan"));
        $mahasiswa->setFoto($this->request->getPost("foto"));
        $mahasiswa->setAlamat($this->request->getPost("alamat"));
        $mahasiswa->setKota($this->request->getPost("kota"));
        $mahasiswa->setKodepos($this->request->getPost("kodepos"));
        $mahasiswa->setTempatLahir($this->request->getPost("tempat_lahir"));
        $mahasiswa->setTanggalLahir($this->request->getPost("tanggal_lahir"));
        $mahasiswa->setJenisKelamin($this->request->getPost("jenis_kelamin"));
        $mahasiswa->setNoTelp($this->request->getPost("no_telp"));
        $mahasiswa->setAsalSekolah($this->request->getPost("asal_sekolah"));
        $mahasiswa->setTahunLulus($this->request->getPost("tahun_lulus"));
        $mahasiswa->setJenisKuliah($this->request->getPost("jenis_kuliah"));
        $mahasiswa->setWali($this->request->getPost("wali"));
        $mahasiswa->setEmail($this->request->getPost("email", "email"));
        $mahasiswa->setAgamaIdAgama($this->request->getPost("agama_id_agama"));
        $mahasiswa->setStatusIdStatus($this->request->getPost("status_id_status"));
        $mahasiswa->setKonsentrasiIdKonsentrasi($this->request->getPost("konsentrasi_id_konsentrasi"));
        $mahasiswa->setNamaOrangtua($this->request->getPost("nama_orangtua"));
        $mahasiswa->setPekerjaanOrangtua($this->request->getPost("pekerjaan_orangtua"));
        $mahasiswa->setAlamatOrangtua($this->request->getPost("alamat_orangtua"));
        $mahasiswa->setKotaOrangtua($this->request->getPost("kota_orangtua"));
        $mahasiswa->setKodeposOrangtua($this->request->getPost("kodepos_orangtua"));
        $mahasiswa->setNoTelpOrangtua($this->request->getPost("no_telp_orangtua"));
        $mahasiswa->setPassword($this->request->getPost("password"));
        

        if (!$mahasiswa->save()) {
            foreach ($mahasiswa->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "mahasiswa",
                "action" => "new"
            ));
        }

        $this->flash->success("Data mahasiswa sukses ditambahkan");

        return $this->dispatcher->forward(array(
            "controller" => "mahasiswa",
            "action" => "index"
        ));

    }

    /**
     * Saves a mahasiswa edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            return $this->dispatcher->forward(array(
                "controller" => "mahasiswa",
                "action" => "index"
            ));
        }

        $nim = $this->request->getPost("nim");

        $mahasiswa = Mahasiswa::findFirstBynim($nim);
        if (!$mahasiswa) {
            $this->flash->error("Data mahasiswa dengan NIM " . $nim . " tidak ditemukan");

            return $this->dispatcher->forward(array(
                "controller" => "mahasiswa",
                "action" => "index"
            ));
        }

        $mahasiswa->setNim($this->request->getPost("nim"));
        $mahasiswa->setNama($this->request->getPost("nama"));
        $mahasiswa->setAngkatan($this->request->getPost("angkatan"));
        $mahasiswa->setFoto($this->request->getPost("foto"));
        $mahasiswa->setAlamat($this->request->getPost("alamat"));
        $mahasiswa->setKota($this->request->getPost("kota"));
        $mahasiswa->setKodepos($this->request->getPost("kodepos"));
        $mahasiswa->setTempatLahir($this->request->getPost("tempat_lahir"));
        $mahasiswa->setTanggalLahir($this->request->getPost("tanggal_lahir"));
        $mahasiswa->setJenisKelamin($this->request->getPost("jenis_kelamin"));
        $mahasiswa->setNoTelp($this->request->getPost("no_telp"));
        $mahasiswa->setAsalSekolah($this->request->getPost("asal_sekolah"));
        $mahasiswa->setTahunLulus($this->request->getPost("tahun_lulus"));
        $mahasiswa->setJenisKuliah($this->request->getPost("jenis_kuliah"));
        $mahasiswa->setWali($this->request->getPost("wali"));
        $mahasiswa->setEmail($this->request->getPost("email", "email"));
        $mahasiswa->setAgamaIdAgama($this->request->getPost("agama_id_agama"));
        $mahasiswa->setStatusIdStatus($this->request->getPost("status_id_status"));
        $mahasiswa->setKonsentrasiIdKonsentrasi($this->request->getPost("konsentrasi_id_konsentrasi"));
        $mahasiswa->setNamaOrangtua($this->request->getPost("nama_orangtua"));
        $mahasiswa->setPekerjaanOrangtua($this->request->getPost("pekerjaan_orangtua"));
        $mahasiswa->setAlamatOrangtua($this->request->getPost("alamat_orangtua"));
        $mahasiswa->setKotaOrangtua($this->request->getPost("kota_orangtua"));
        $mahasiswa->setKodeposOrangtua($this->request->getPost("kodepos_orangtua"));
        $mahasiswa->setNoTelpOrangtua($this->request->getPost("no_telp_orangtua"));
        

        if (!$mahasiswa->save()) {

            foreach ($mahasiswa->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "mahasiswa",
                "action" => "edit",
                "params" => array($mahasiswa->nim)
            ));
        }

        $this->flash->success("Sukses mengubah data mahasiswa");

        return $this->dispatcher->forward(array(
            "controller" => "mahasiswa",
            "action" => "index"
        ));

    }

    /**
     * Deletes a mahasiswa
     *
     * @param string $nim
     */
    public function deleteAction($nim)
    {

        $mahasiswa = Mahasiswa::findFirstBynim($nim);
        if (!$mahasiswa) {
            $this->flash->error("Data mahasiswa tidak ditemukan");

            return $this->dispatcher->forward(array(
                "controller" => "mahasiswa",
                "action" => "index"
            ));
        }

        if (!$mahasiswa->delete()) {

            foreach ($mahasiswa->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "mahasiswa",
                "action" => "search"
            ));
        }

        $this->flash->success("Sukses menghapus data mahasiswa");

        return $this->dispatcher->forward(array(
            "controller" => "mahasiswa",
            "action" => "index"
        ));
    }

    /**
     * menampilkan detail informasi mahasiwa
     * @param  string $nim nim mahasiswa
     * @return void      view
     */
    public function detailAction($nim)
    {
        // usleep(1000000);
        $this->view->ajax = false;
        if($this->request->isAjax()){
            $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_ACTION_VIEW);
            $this->view->ajax = true;
        }
        $mahasiswa = Mahasiswa::findFirstBynim($nim);
        $this->view->mahasiswa = $mahasiswa;
    }

    private function loadTahun()
    {
        $tahun = array();
        $tahunAwal = '2000';
        $tahunSaatIni = date('Y');
        for ($i = $tahunAwal; $i  <= $tahunSaatIni; $i ++) { 
            $tahun[$i] = $i;
        }

        return $tahun;
    }
}
