{#
/**
 * @author Fery Putra Tarigan <kutanaridesain@gmail.com>
 * @link http://kutanari.com, http://kutanaridesain.com
 * @since 2015
 * @license MIT License
 */
#}
<div class="row">
    <div class="col-xs-12">
        <div class="box box-success">
            <div class="box-header">
                <h3 class="box-title">Daftar Ruangan</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
            {{ content() }}
            <div class="box-body clearfix">
                {{link_to("ruangan/new", "Tambah", "class":"btn btn-success btn-xs pull-right")}}
            </div>
            
            {{page.items.ruangan}}

                <div class="box-body table-responsive">
                <table class="table table-bordered table-hover">
                    <colgroup>
                        <col width="2%"></col>
                        <col width="60%"></col>
                        <col width="20%"></col>
                        <col width="10%"></col>
                    </colgroup>
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Nama Ruangan</th>
                            <th>Kapasitas <em>(orang)</em></th>
                            <th>Proses</th>
                        </tr>
                    </thead>
                    <tbody>
                        {% if page.items is defined %}
                        {% for ruangan in page.items %}
                            {% set counter += 1 %}
                            <tr>
                                <td>{{ counter }}</td>
                                <td>{{ ruangan.getNamaRuangan() }}</td>
                                <td>{{ ruangan.getKapasitasRuangan() }}</td>
                                <td align="center">

                                    {{ link_to("ruangan/edit/"~ruangan.getIdRuangan(), '<i class="fa fa-fw fa-edit"></i>', 'data-toggle':"tooltip", 'title':'Edit', 'class':'btn btn-xs btn-primary') }} 

                                    {{ link_to("ruangan/delete/"~ruangan.getIdRuangan(), '<i class="fa fa-fw fa-minus-circle"></i>', 'data-toggle':"tooltip", 'title':'Delete', 'class':'btn btn-xs btn-danger', "onclick":"javascript:if(confirm('Anda yakin menghapus item ini?')==false)return false;") }}
                                </td>
                            </tr>
                        {% endfor %}
                        {% endif %}
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="4" align="center">
                            {{ link_to("jurusan/", "First") }} | 
                            {{ link_to("jurusan/?page="~page.before, "Previous") }} | 
                            {{ link_to("jurusan/?page="~page.next, "Next") }} | 
                            {{ link_to("jurusan/?page="~page.last, "Last") }} | 
                            {{ page.current~"/"~page.total_pages }}
                            </td>
                        </tr>
                    </tfoot>
                </table>
                </div>
                
            </div>
        </div>
    </div>
</div>