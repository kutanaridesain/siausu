{#
/**
 * @author Fery Putra Tarigan <kutanaridesain@gmail.com>
 * @link http://kutanari.com, http://kutanaridesain.com
 * @since 2015
 * @license MIT License
 */
#}

{{ content() }}

<table width="100%">
    <tr>
        <td align="left">
            {{ link_to("ruangan/index", "Go Back") }}
        </td>
        <td align="right">
            {{ link_to("ruangan/new", "Create ") }}
        </td>
    </tr>
</table>

<table class="browse" align="center">
    <thead>
        <tr>
            <th>Id Of Ruangan</th>
            <th>Nama Of Ruangan</th>
            <th>Kapasitas Of Ruangan</th>
         </tr>
    </thead>
    <tbody>
    {% if page.items is defined %}
    {% for ruangan in page.items %}
        <tr>
            <td>{{ ruangan.getIdRuangan() }}</td>
            <td>{{ ruangan.getNamaRuangan() }}</td>
            <td>{{ ruangan.getKapasitasRuangan() }}</td>
            <td>{{ link_to("ruangan/edit/"~ruangan.getIdRuangan(), "Edit") }}</td>
            <td>{{ link_to("ruangan/delete/"~ruangan.getIdRuangan(), "Delete") }}</td>
        </tr>
    {% endfor %}
    {% endif %}
    </tbody>
    <tbody>
        <tr>
            <td colspan="2" align="right">
                <table align="center">
                    <tr>
                        <td>{{ link_to("ruangan/search", "First") }}</td>
                        <td>{{ link_to("ruangan/search?page="~page.before, "Previous") }}</td>
                        <td>{{ link_to("ruangan/search?page="~page.next, "Next") }}</td>
                        <td>{{ link_to("ruangan/search?page="~page.last, "Last") }}</td>
                        <td>{{ page.current~"/"~page.total_pages }}</td>
                    </tr>
                </table>
            </td>
        </tr>
    </tbody>
</table>
