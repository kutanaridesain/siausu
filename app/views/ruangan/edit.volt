{#
/**
 * @author Fery Putra Tarigan <kutanaridesain@gmail.com>
 * @link http://kutanari.com, http://kutanaridesain.com
 * @since 2015
 * @license MIT License
 */
#}
{{ form("ruangan/save", "method":"post", "autocomplete" : "off", "role":"form") }}
{% if content() != "" %}
<div class="alert alert-danger alert-dismissable">
    <i class="fa fa-ban"></i>
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
{{ content() }}
</div>
{% endif %}
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Ubah Data Ruangan</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group">
                        <label for="nama_ruangan">Nama Ruangan</label>
                        {{ text_field("nama_ruangan", "size" : 30, "class":"form-control", "placeholder":"masukkan nama ruangan") }}
                    </div>
                    <div class="form-group">
                        <label for="kapasitas_ruangan">Kapasitas</label>
                        {{ text_field("kapasitas_ruangan", "size" : 30, 'type':'numeric', "class":"form-control", "placeholder":"masukkan kapasitas ruangan") }}
                    </div>
                </div>
                <div class="box-footer clearfix">
                    {{ hidden_field("id_ruangan") }}
                    {{ link_to("ruangan", "Batal", "class":"btn btn-default pull-left") }}
                    {{ submit_button("Simpan", "class":"btn btn-primary pull-right") }}
                </div>
            </div>
        </div>
    </div>
</form>