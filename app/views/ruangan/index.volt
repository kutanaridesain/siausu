{#
/**
 * @author Fery Putra Tarigan <kutanaridesain@gmail.com>
 * @link http://kutanari.com, http://kutanaridesain.com
 * @since 2015
 * @license MIT License
 */
#}
<div ng-controller="RuanganController as ruangan">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-success">
                <div class="box-header">
                    <h3 class="box-title">Daftar Ruangan</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                {{ content() }}
                <div class="box-body clearfix">
                    {{link_to("#", "Tambah", "class":"btn btn-success btn-xs pull-right", "data-toggle":"modal", "data-target":"#modalRuangan", "ng-click":"tambah()")}}
                </div>
                
                    <div class="box-body table-responsive">
                    <table class="table table-bordered table-hover">
                        <colgroup>
                            <col width="2%"></col>
                            <col width="60%"></col>
                            <col width="20%"></col>
                            <col width="10%"></col>
                        </colgroup>
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Nama Ruangan</th>
                                <th>Kapasitas <em>(orang)</em></th>
                                <th>Proses</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="itemRuangan in listRuangan">
                                <td>[[$index+1]]</td>
                                <td>[[itemRuangan.nama_ruangan]]</td>
                                <td>[[itemRuangan.kapasitas_ruangan]]</td>
                                <td align="center">
                                    <a href ng-click="ubah($index)" data-toggle="modal" data-target="#modalRuangan" title='Edit' class='btn btn-xs btn-primary' tooltip><i class="fa fa-fw fa-edit"></i></a>
                                    <a href data-toggle="tooltip" title='Delete' class='btn btn-xs btn-danger' ng-click="hapus($index)" tooltip><i class="fa fa-fw fa-minus-circle"></i></a>
                                </td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="4" align="center"></td>
                            </tr>
                        </tfoot>
                    </table>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modalRuangan" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
        <form autocomplete="off" role="form" ng-submit="simpanRuangan()">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">[[action]] Ruangan</h4>
          </div>
          <div class="modal-body">
            <div class="box-body">
                <div class="form-group">
                    <label for="nama_ruangan">Nama Ruangan</label>
                    <input type="text" name="nama_ruangan" ng-model="nama_ruangan" class="form-control" placeholder="Masukkan nama ruangan" required>
                </div>
                <div class="form-group">
                    <label for="kapasitas_ruangan">Kapasitas</label>
                    <input type="number" name="kapasitas_ruangan" ng-model="kapasitas_ruangan" class="form-control" placeholder="Masukkan kapasitas ruangan" required>
                </div>
                <input type="hidden" name="id" ng-model="id" >
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <input type="submit" class="btn btn-primary" value="Simpan" />
          </div>
        </form>

        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</div>
