{#
/**
 * @author Fery Putra Tarigan <kutanaridesain@gmail.com>
 * @link http://kutanari.com, http://kutanaridesain.com
 * @since 2015
 * @license MIT License
 */
#}
<!-- sidebar menu: : style can be found in sidebar.less -->
<ul class="sidebar-menu">
    
<!-- 
    <li class="active">
        <a href="index.html">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
        </a>
    </li>
    <li>
        <a href="pages/widgets.html">
            <i class="fa fa-th"></i> <span>Widgets</span> <small class="badge pull-right bg-green">new</small>
        </a>
    </li>
    <li class="treeview">
        <a href="#">
            <i class="fa fa-bar-chart-o"></i>
            <span>Charts</span>
            <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
            <li><a href="pages/charts/morris.html"><i class="fa fa-angle-double-right"></i> Morris</a></li>
            <li><a href="pages/charts/flot.html"><i class="fa fa-angle-double-right"></i> Flot</a></li>
            <li><a href="pages/charts/inline.html"><i class="fa fa-angle-double-right"></i> Inline charts</a></li>
        </ul>
    </li>
    <li class="treeview">
        <a href="#">
            <i class="fa fa-laptop"></i>
            <span>UI Elements</span>
            <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
            <li><a href="pages/UI/general.html"><i class="fa fa-angle-double-right"></i> General</a></li>
            <li><a href="pages/UI/icons.html"><i class="fa fa-angle-double-right"></i> Icons</a></li>
            <li><a href="pages/UI/buttons.html"><i class="fa fa-angle-double-right"></i> Buttons</a></li>
            <li><a href="pages/UI/sliders.html"><i class="fa fa-angle-double-right"></i> Sliders</a></li>
            <li><a href="pages/UI/timeline.html"><i class="fa fa-angle-double-right"></i> Timeline</a></li>
        </ul>
    </li>
    <li class="treeview">
        <a href="#">
            <i class="fa fa-edit"></i> <span>Forms</span>
            <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
            <li><a href="pages/forms/general.html"><i class="fa fa-angle-double-right"></i> General Elements</a></li>
            <li><a href="pages/forms/advanced.html"><i class="fa fa-angle-double-right"></i> Advanced Elements</a></li>
            <li><a href="pages/forms/editors.html"><i class="fa fa-angle-double-right"></i> Editors</a></li>                                
        </ul>
    </li>
    <li class="treeview">
        <a href="#">
            <i class="fa fa-table"></i> <span>Tables</span>
            <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
            <li><a href="pages/tables/simple.html"><i class="fa fa-angle-double-right"></i> Simple tables</a></li>
            <li><a href="pages/tables/data.html"><i class="fa fa-angle-double-right"></i> Data tables</a></li>
        </ul>
    </li>
    <li>
        <a href="pages/calendar.html">
            <i class="fa fa-calendar"></i> <span>Calendar</span>
            <small class="badge pull-right bg-red">3</small>
        </a>
    </li>
    <li>
        <a href="pages/mailbox.html">
            <i class="fa fa-envelope"></i> <span>Mailbox</span>
            <small class="badge pull-right bg-yellow">12</small>
        </a>
    </li>
    -->
    <li class="active">
        <a href="#">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
        </a>
    </li>
    <li class="treeview">
        <a href="#">
            <i class="fa fa-inbox"></i> <span>Master data</span>
            <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
            <li>{{ link_to("mahasiswa", '<i class="fa fa-angle-double-right"></i> Mahasiswa') }}</li>
            <li>{{ link_to("dosen", '<i class="fa fa-angle-double-right"></i> Dosen') }}</li>
            <li>{{ link_to("agama", '<i class="fa fa-angle-double-right"></i> Agama') }}</li>
            <li>{{ link_to("konsentrasi", '<i class="fa fa-angle-double-right"></i> Konsentrasi') }}</li>
            <li>{{ link_to("matakuliah", '<i class="fa fa-angle-double-right"></i> Mata Kuliah') }}</li>
            <li>{{ link_to("status", '<i class="fa fa-angle-double-right"></i> Status') }}</li>
        </ul>
    </li>
    <li class="treeview">
        <a href="#">
            <i class="fa fa-gears"></i> <span>Pengaturan</span>
            <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
            <li>{{ link_to("ruangan", '<i class="fa fa-angle-double-right"></i> Ruangan') }}</li>
            <li>{{ link_to("pengampu", '<i class="fa fa-angle-double-right"></i> Dosen Pengampu') }}</li>
            <li>{{ link_to("jadwal", '<i class="fa fa-calendar"></i> Jadwal') }}</li>
            <li>{{ link_to("krs", '<i class="fa fa-angle-double-right"></i> KRS') }}</li>
            <li>{{ link_to("#", '<i class="fa fa-angle-double-right"></i> KHS') }}</li>
            <li>{{ link_to("#", '<i class="fa fa-angle-double-right"></i> Input Nilai') }}</li>
            <li>{{ link_to("tiket", '<i class="fa fa-angle-double-right"></i> Manajemen Tiket') }}</li>
            <li>{{ link_to("#", '<i class="fa fa-angle-double-right"></i> Manajemen User') }}</li>
            <li>{{ link_to("#", '<i class="fa fa-angle-double-right"></i> Manajemen Notifikasi') }}</li>
        </ul>
    </li>
    <li class="treeview">
        <a href="#">
            <i class="fa fa-gears"></i> <span>Peralatan</span>
            <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
            <li>{{ link_to("peralatan/importdna", '<i class="fa fa-angle-double-right"></i> Import DNA') }}</li>
            <li>{{ link_to("#", '<i class="fa fa-angle-double-right"></i> Import Data Mahasiswa') }}</li>
            <li>{{ link_to("#", '<i class="fa fa-angle-double-right"></i> Import Data Dosen') }}</li>
            <li>{{ link_to("krs", '<i class="fa fa-angle-double-right"></i> Import Data Nilai') }}</li>
            <li>{{ link_to("#", '<i class="fa fa-angle-double-right"></i> Export Data Mahasiswa') }}</li>
            <li>{{ link_to("#", '<i class="fa fa-angle-double-right"></i> Export Data Dosen') }}</li>
            <li>{{ link_to("krs", '<i class="fa fa-angle-double-right"></i> Export Data Nilai') }}</li>
        </ul>
    </li>
 </ul>
<!-- /.sidebar -->