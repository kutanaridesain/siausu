<ul class="sidebar-menu">
    <li>{{ link_to("matakuliah", '<i class="fa fa-angle-double-right"></i> Informasi Mata Kuliah') }}</li>
    <li>{{ link_to("krs", '<i class="fa fa-angle-double-right"></i> KRS') }}</li>
    <li>{{ link_to("khs/info", '<i class="fa fa-angle-double-right"></i> KHS') }}</li>
    <li>{{ link_to("jadwal/kuliah", '<i class="fa fa-angle-double-right"></i> Jadwal Kuliah') }}</li>
    <li>{{ link_to("tiket", '<i class="fa fa-angle-double-right"></i> Pengaduan') }}</li>
    <li>{{ link_to("user/password", '<i class="fa fa-angle-double-right"></i> Ganti Password') }}</li>
</ul>