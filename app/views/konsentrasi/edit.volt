{#
/**
 * @author Fery Putra Tarigan <kutanaridesain@gmail.com>
 * @link http://kutanari.com, http://kutanaridesain.com
 * @since 2015
 * @license MIT License
 */
#}
{{ content() }}
{{ form("konsentrasi/save", "method":"post") }}

<table width="100%">
    <tr>
        <td align="left">{{ link_to("konsentrasi", "Go Back") }}</td>
        <td align="right">{{ submit_button("Save") }}</td>
    </tr>
</table>

<div align="center">
    <h1>Edit konsentrasi</h1>
</div>

<table>
    <tr>
        <td align="right">
            <label for="nama_jurusan">Nama Of Jurusan</label>
        </td>
        <td align="left">
            {{ text_field("nama_jurusan", "size" : 30) }}
        </td>
    </tr>

    <tr>
        <td>{{ hidden_field("id") }}</td>
        <td>{{ submit_button("Save") }}</td>
    </tr>
</table>

</form>
