{#
/**
 * @author Fery Putra Tarigan <kutanaridesain@gmail.com>
 * @link http://kutanari.com, http://kutanaridesain.com
 * @since 2015
 * @license MIT License
 */
#}

{{ content() }}

<table width="100%">
    <tr>
        <td align="left">
            {{ link_to("konsentrasi/index", "Go Back") }}
        </td>
        <td align="right">
            {{ link_to("konsentrasi/new", "Create ") }}
        </td>
    </tr>
</table>

<table class="browse" align="center">
    <thead>
        <tr>
            <th>Id Of Konsentrasi</th>
            <th>Nama Of Jurusan</th>
         </tr>
    </thead>
    <tbody>
    {% if page.items is defined %}
    {% for konsentrasi in page.items %}
        <tr>
            <td>{{ konsentrasi.getIdKonsentrasi() }}</td>
            <td>{{ konsentrasi.getNamaJurusan() }}</td>
            <td>{{ link_to("konsentrasi/edit/"~konsentrasi.getIdKonsentrasi(), "Edit") }}</td>
            <td>{{ link_to("konsentrasi/delete/"~konsentrasi.getIdKonsentrasi(), "Delete") }}</td>
        </tr>
    {% endfor %}
    {% endif %}
    </tbody>
    <tbody>
        <tr>
            <td colspan="2" align="right">
                <table align="center">
                    <tr>
                        <td>{{ link_to("konsentrasi/search", "First") }}</td>
                        <td>{{ link_to("konsentrasi/search?page="~page.before, "Previous") }}</td>
                        <td>{{ link_to("konsentrasi/search?page="~page.next, "Next") }}</td>
                        <td>{{ link_to("konsentrasi/search?page="~page.last, "Last") }}</td>
                        <td>{{ page.current~"/"~page.total_pages }}</td>
                    </tr>
                </table>
            </td>
        </tr>
    </tbody>
</table>
