{#
/**
 * @author Fery Putra Tarigan <kutanaridesain@gmail.com>
 * @link http://kutanari.com, http://kutanaridesain.com
 * @since 2015
 * @license MIT License
 */
#}
<div ng-controller="KonsentrasiController as konsentrasi">
    
<div class="row">
    <div class="col-xs-12">
        <div class="box box-success">
            <div class="box-header">
                <h3 class="box-title">Daftar Konsentrasi</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
            {{ content() }}
            <div class="box-body clearfix">
                {{link_to("#", "Tambah", "class":"btn btn-success btn-xs pull-right", "data-toggle":"modal", "data-target":"#modalKonsentrasi", "ng-click":"tambah()")}}
            </div>

                <div class="box-body table-responsive">
                <table class="table table-bordered table-hover">
                    <colgroup>
                        <col width="2%"></col>
                        <col width="80%"></col>
                        <col width="10%"></col>
                    </colgroup>
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Nama Konsentrasi</th>
                            <th>Proses</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr ng-repeat="itemKonsentrasi in listKonsentrasi" ng-cloak>
                            <td>[[$index+1]]</td>
                            <td>[[itemKonsentrasi.nama_konsentrasi]]</td>
                            <td align="center">
                            <a href ng-click="ubah($index)" data-toggle="modal" data-target="#modalKonsentrasi"title='Edit' class='btn btn-xs btn-primary' tooltip><i class="fa fa-fw fa-edit"></i></a>
                            <a href data-toggle="tooltip" title='Delete' class='btn btn-xs btn-danger' ng-click="hapus($index)" tooltip><i class="fa fa-fw fa-minus-circle"></i></a>
                            </td>
                        </tr>
                    </tbody>
                    
                </table>
                </div>
                
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modalKonsentrasi" tabindex="-1" role="dialog" aria-labelledby="modalKonsentrasiLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
    <form autocomplete="off" role="form" ng-submit="simpanKonsentrasi()">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="modalKonsentrasiLabel">Modal title</h4>
          </div>
          <div class="modal-body">
            <div class="box-body">
                <div class="form-group">
                    <label for="nama_konsentrasi">Nama Konsentrasi</label>
                    <input type="text" name="nama_konsentrasi" ng-model="nama_konsentrasi" class="form-control" placeholder="Masukkan nama konsentrasi" required >
                    <input type="hidden" name="id" ng-model="id" >
                </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <input type="submit" ng-show="nama_konsentrasi.length" class="btn btn-primary" value="Simpan" />
          </div>
    </form>

    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
</div>
