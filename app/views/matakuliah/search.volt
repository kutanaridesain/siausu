{#
/**
 * @author Fery Putra Tarigan <kutanaridesain@gmail.com>
 * @link http://kutanari.com, http://kutanaridesain.com
 * @since 2015
 * @license MIT License
 */
#}

{{ content() }}

<table width="100%">
    <tr>
        <td align="left">
            {{ link_to("matakuliah/index", "Go Back") }}
        </td>
        <td align="right">
            {{ link_to("matakuliah/new", "Create ") }}
        </td>
    </tr>
</table>

<table class="browse" align="center">
    <thead>
        <tr>
            <th>Id Of Matakuliah</th>
            <th>Kode Of Matakuliah</th>
            <th>Nama Of Matakuliah</th>
            <th>Sks</th>
            <th>Semester</th>
         </tr>
    </thead>
    <tbody>
    {% if page.items is defined %}
    {% for matakuliah in page.items %}
        <tr>
            <td>{{ matakuliah.getIdMatakuliah() }}</td>
            <td>{{ matakuliah.getKodeMatakuliah() }}</td>
            <td>{{ matakuliah.getNamaMatakuliah() }}</td>
            <td>{{ matakuliah.getSks() }}</td>
            <td>{{ matakuliah.getSemester() }}</td>
            <td>{{ link_to("matakuliah/edit/"~matakuliah.getIdMatakuliah(), "Edit") }}</td>
            <td>{{ link_to("matakuliah/delete/"~matakuliah.getIdMatakuliah(), "Delete") }}</td>
        </tr>
    {% endfor %}
    {% endif %}
    </tbody>
    <tbody>
        <tr>
            <td colspan="2" align="right">
                <table align="center">
                    <tr>
                        <td>{{ link_to("matakuliah/search", "First") }}</td>
                        <td>{{ link_to("matakuliah/search?page="~page.before, "Previous") }}</td>
                        <td>{{ link_to("matakuliah/search?page="~page.next, "Next") }}</td>
                        <td>{{ link_to("matakuliah/search?page="~page.last, "Last") }}</td>
                        <td>{{ page.current~"/"~page.total_pages }}</td>
                    </tr>
                </table>
            </td>
        </tr>
    </tbody>
</table>
