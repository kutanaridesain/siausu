{#
/**
 * @author Fery Putra Tarigan <kutanaridesain@gmail.com>
 * @link http://kutanari.com, http://kutanaridesain.com
 * @since 2015
 * @license MIT License
 */
#}
{{ form("matakuliah/create", "method":"post", "autocomplete" : "off", "role":"form") }}
{% if content() != "" %}
<div class="alert alert-danger alert-dismissable">
    <i class="fa fa-ban"></i>
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
{{ content() }}
</div>
{% endif %}
    <div class="row">
        <div class="col-md-6">
            <div class="box box-success">
                <div class="box-header">
                    <h3 class="box-title">Tambah Mata Kuliah</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group">
                        <label for="kode_matakuliah">Kode Mata Kuliah</label>
                        {{ text_field("kode_matakuliah", "size" : 30, "class":"form-control", "placeholder":"masukkan kode mata kuliah") }}
                    </div>
                    <div class="form-group">
                        <label for="nama_matakuliah">Nama Mata Kuliah</label>
                        {{ text_field("nama_matakuliah", "size" : 30, "class":"form-control", "placeholder":"masukkan nama mata kuliah") }}
                    </div>
                    <div class="form-group">
                        <label for="sks">SKS</label>
                        {{ select_static('sks', ['1':'1', '2':'2', '3':'3', '4':'4'], "class":"form-control") }}
                    </div>
                    <div class="form-group">
                        <label for="semester">Semester</label>
                        {{ text_field("semester", "size" : 30, "type":"numeric", "class":"form-control", "placeholder":"masukkan angka semester") }}
                    </div>
                </div>
                <div class="box-footer clearfix">
                    {{link_to("matakuliah", "Batal", "class":"btn btn-default pull-left")}}
                    {{ submit_button("Simpan", "class":"btn btn-success pull-right") }}
                </div>
            </div>
        </div>
    </div>
</form>