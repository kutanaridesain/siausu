{#
/**
 * @author Fery Putra Tarigan <kutanaridesain@gmail.com>
 * @link http://kutanari.com, http://kutanaridesain.com
 * @since 2015
 * @license MIT License
 */
#}

{{ content() }}

<table width="100%">
    <tr>
        <td align="left">
            {{ link_to("status/index", "Go Back") }}
        </td>
        <td align="right">
            {{ link_to("status/new", "Create ") }}
        </td>
    </tr>
</table>

<table class="browse" align="center">
    <thead>
        <tr>
            <th>Id Of Status</th>
            <th>Nama Of Status</th>
         </tr>
    </thead>
    <tbody>
    {% if page.items is defined %}
    {% for statu in page.items %}
        <tr>
            <td>{{ statu.getIdStatus() }}</td>
            <td>{{ statu.getNamaStatus() }}</td>
            <td>{{ link_to("status/edit/"~statu.getIdStatus(), "Edit") }}</td>
            <td>{{ link_to("status/delete/"~statu.getIdStatus(), "Delete") }}</td>
        </tr>
    {% endfor %}
    {% endif %}
    </tbody>
    <tbody>
        <tr>
            <td colspan="2" align="right">
                <table align="center">
                    <tr>
                        <td>{{ link_to("status/search", "First") }}</td>
                        <td>{{ link_to("status/search?page="~page.before, "Previous") }}</td>
                        <td>{{ link_to("status/search?page="~page.next, "Next") }}</td>
                        <td>{{ link_to("status/search?page="~page.last, "Last") }}</td>
                        <td>{{ page.current~"/"~page.total_pages }}</td>
                    </tr>
                </table>
            </td>
        </tr>
    </tbody>
</table>
