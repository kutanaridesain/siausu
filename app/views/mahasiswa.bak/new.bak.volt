{#
/**
 * @author Fery Putra Tarigan <kutanaridesain@gmail.com>
 * @link http://kutanari.com, http://kutanaridesain.com
 * @since 2015
 * @license MIT License
 */
#}

{{ form("mahasiswa/create", "method":"post", "autocomplete" : "off", "role":"form") }}
<div class="alert alert-danger alert-dismissable">
    <i class="fa fa-ban"></i>
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
{{ content() }}
</div>

    <div class="row">
        <div class="col-md-6">
            <div class="box box-success">
                <div class="box-header">
                    <h3 class="box-title">Tambah Mahasiswa</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group">
                        <label for="nim">NIM</label>
                        {{ text_field("nim", "size" : 30, "class":"form-control", "placeholder":"Masukkan NIM") }}
                    </div>
                    <div class="form-group">
                        <label for="nama">Nama</label>
                        {{ text_field("nama", "size" : 30, "class":"form-control", "placeholder":"Masukkan Nama") }}
                    </div>
                    <div class="form-group">
                        <label for="alamat">Alamat</label>
                        {{ text_field("alamat", "size" : 30, "class":"form-control", "placeholder":"Masukkan Alamat") }}
                    </div>
                    <div class="form-group">
                        <label for="angkatan">Angkatan</label>
                        {{ text_field("angkatan", "size" : 30, "class":"form-control", "placeholder":"Masukkan Angkatan") }}
                    </div>
                    <div class="form-group">
                        <label for="tempat_lahir">Tempat Lahir</label>
                        {{ text_field("tempat_lahir", "size" : 30, "class":"form-control", "placeholder":"Masukkan Tempat Lahir") }}
                    </div>
                    <div class="form-group">
                        <label for="tanggal_lahir">Tanggal Lahir</label>
                        {{ text_field("tanggal_lahir", "size" : 30, "class":"form-control", "placeholder":"Masukkan Tanggal Lahir") }}
                    </div>
                    <div class="form-group">
                        <label for="jenis_kelamin">Jenis Kelamin</label>
                        {{ text_field("jenis_kelamin", "size" : 30, "class":"form-control", "placeholder":"Masukkan Jenis Kelamin") }}
                    </div>
                    <div class="form-group">
                        <label for="no_telp">No. Telp</label>
                        {{ text_field("no_telp", "size" : 30, "class":"form-control", "placeholder":"Masukkan No. Telp") }}
                    </div>
                    <div class="form-group">
                        <label for="asal_sekolah">Asal Sekolah</label>
                        {{ text_field("asal_sekolah", "size" : 30, "class":"form-control", "placeholder":"Masukkan Asal Sekolah") }}
                    </div>
                    <div class="form-group">
                        <label for="tahun_lulus">Tahun Lulus</label>
                        {{ text_field("tahun_lulus", "size" : 30, "class":"form-control", "placeholder":"Masukkan Tahun Lulus") }}
                    </div>
                    <div class="form-group">
                        <label for="agama_id_agama">Agama</label>
                        {{ select("agama_id_agama", Agama, "using":["id_agama", "nama_agama"]) }}
                        {{ text_field("agama_id_agama", "size" : 30, "class":"form-control", "placeholder":"Masukkan Agama") }}
                    </div>
                    <div class="form-group">
                        <label for="jurusan_id_jurusan">Jurusan</label>
                        {{ text_field("jurusan_id_jurusan", "size" : 30, "class":"form-control", "placeholder":"Masukkan Jurusan") }}
                    </div>
                    <div class="form-group">
                        <label for="status_id_status">Status</label>
                        {{ text_field("status_id_status", "size" : 30, "class":"form-control", "placeholder":"Masukkan Status") }}
                    </div>
                    <div class="form-group">
                        <label for="jenis_kuliah">Jenis Kuliah</label>
                        {{ text_field("jenis_kuliah", "size" : 30, "class":"form-control", "placeholder":"Masukkan Jenis Kuliah") }}
                    </div>
                    <div class="form-group">
                        <label for="jenis_kuliah">Jenis Kuliah</label>
                        {{ text_field("jenis_kuliah", "size" : 30, "class":"form-control", "placeholder":"Masukkan Jenis Kuliah") }}
                    </div>
                </div>
                <div class="box-footer clearfix">
                    {{ submit_button("Simpan", "class":"btn btn-success pull-right") }}
                </div>
            </div>
        </div>
    </div>
</form>