{#
/**
 * @author Fery Putra Tarigan <kutanaridesain@gmail.com>
 * @link http://kutanari.com, http://kutanaridesain.com
 * @since 2015
 * @license MIT License
 */
#}

{{ content() }}

<table width="100%">
    <tr>
        <td align="left">
            {{ link_to("mahasiswa/index", "Go Back") }}
        </td>
        <td align="right">
            {{ link_to("mahasiswa/new", "Create ") }}
        </td>
    </tr>
</table>

<table class="browse" align="center">
    <thead>
        <tr>
            <th>Nim</th>
            <th>Nama</th>
            <th>Angkatan</th>
            <th>Foto</th>
            <th>Alamat</th>
            <th>Tempat Of Lahir</th>
            <th>Tanggal Of Lahir</th>
            <th>Jenis Of Kelamin</th>
            <th>No Of Telp</th>
            <th>Asal Of Sekolah</th>
            <th>Tahun Of Lulus</th>
            <th>Agama Of Id Of Agama</th>
            <th>Jurusan Of Id Of Jurusan</th>
            <th>Status Of Id Of Status</th>
            <th>Jenis Of Kuliah</th>
         </tr>
    </thead>
    <tbody>
    {% if page.items is defined %}
    {% for mahasiswa in page.items %}
        <tr>
            <td>{{ mahasiswa.getNim() }}</td>
            <td>{{ mahasiswa.getNama() }}</td>
            <td>{{ mahasiswa.getAngkatan() }}</td>
            <td>{{ mahasiswa.getFoto() }}</td>
            <td>{{ mahasiswa.getAlamat() }}</td>
            <td>{{ mahasiswa.getTempatLahir() }}</td>
            <td>{{ mahasiswa.getTanggalLahir() }}</td>
            <td>{{ mahasiswa.getJenisKelamin() }}</td>
            <td>{{ mahasiswa.getNoTelp() }}</td>
            <td>{{ mahasiswa.getAsalSekolah() }}</td>
            <td>{{ mahasiswa.getTahunLulus() }}</td>
            <td>{{ mahasiswa.getAgamaIdAgama() }}</td>
            <td>{{ mahasiswa.getJurusanIdJurusan() }}</td>
            <td>{{ mahasiswa.getStatusIdStatus() }}</td>
            <td>{{ mahasiswa.getJenisKuliah() }}</td>
            <td>{{ link_to("mahasiswa/edit/"~mahasiswa.getNim(), "Edit") }}</td>
            <td>{{ link_to("mahasiswa/delete/"~mahasiswa.getNim(), "Delete") }}</td>
        </tr>
    {% endfor %}
    {% endif %}
    </tbody>
    <tbody>
        <tr>
            <td colspan="2" align="right">
                <table align="center">
                    <tr>
                        <td>{{ link_to("mahasiswa/search", "First") }}</td>
                        <td>{{ link_to("mahasiswa/search?page="~page.before, "Previous") }}</td>
                        <td>{{ link_to("mahasiswa/search?page="~page.next, "Next") }}</td>
                        <td>{{ link_to("mahasiswa/search?page="~page.last, "Last") }}</td>
                        <td>{{ page.current~"/"~page.total_pages }}</td>
                    </tr>
                </table>
            </td>
        </tr>
    </tbody>
</table>
