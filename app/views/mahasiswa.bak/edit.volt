{#
/**
 * @author Fery Putra Tarigan <kutanaridesain@gmail.com>
 * @link http://kutanari.com, http://kutanaridesain.com
 * @since 2015
 * @license MIT License
 */
#}
{{ form("mahasiswa/save", "method":"post", "autocomplete" : "off", "role":"form") }}
{% if content() != "" %}
<div class="alert alert-danger alert-dismissable">
    <i class="fa fa-ban"></i>
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
{{ content() }}
</div>
{% endif %}
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Ubah Mahasiswa</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group">
                        <label for="nim">NIM</label>
                        {{ text_field("nim", "size" : 30, "class":"form-control", "placeholder":"Masukkan NIM") }}
                    </div>
                    <div class="form-group">
                        <label for="nama">Nama Mahasiswa</label>
                        {{ text_field("nama", "size" : 30, "class":"form-control", "placeholder":"Masukkan Nama Mahasiswa") }}
                    </div>
                    <div class="form-group">
                        <label for="alamat">Alamat</label>
                        {{ text_field("alamat", "size" : 30, "class":"form-control", "placeholder":"Masukkan Alamat") }}
                    </div>
                    <div class="form-group">
                        <label for="angkatan">Angkatan</label>
                        {{ text_field("angkatan", "size" : 30, "class":"form-control", "placeholder":"Masukkan Angkatan") }}
                    </div>
                    <div class="form-group">
                        <label for="tempat_lahir">Tempat Lahir</label>
                        {{ text_field("tempat_lahir", "size" : 30, "class":"form-control", "placeholder":"Masukkan Tempat Lahir") }}
                    </div>
                    <div class="form-group">
                        <label for="tanggal_lahir">Tanggal Lahir</label>
                        {{ text_field("tanggal_lahir", "size" : 30, "class":"form-control", "placeholder":"Masukkan Tanggal Lahir") }}
                    </div>
                    <div class="form-group">
                        <label for="jenis_kelamin">Jenis Kelamin</label>
                        {{ text_field("jenis_kelamin", "size" : 30, "class":"form-control", "placeholder":"Masukkan Jenis Kelamin") }}
                    </div>
                    <div class="form-group">
                        <label for="no_telp">No. Telp</label>
                        {{ text_field("no_telp", "size" : 30, "class":"form-control", "placeholder":"Masukkan No. Telp") }}
                    </div>
                    <div class="form-group">
                        <label for="asal_sekolah">Asal Sekolah</label>
                        {{ text_field("asal_sekolah", "size" : 30, "class":"form-control", "placeholder":"Masukkan Asal Sekolah") }}
                    </div>
                    <div class="form-group">
                        <label for="tahun_lulus">Tahun Lulus</label>
                        {{ text_field("tahun_lulus", "size" : 30, "class":"form-control", "placeholder":"Masukkan Tahun Lulus") }}
                    </div>
                    <div class="form-group">
                        <label for="agama_id_agama">Agama</label>
                        {{ select("agama_id_agama", agama, "using":["id_agama", "nama_agama"], "useEmpty":true, "emptyText":"Pilih agama", "emptyValue":"", "class":"form-control") }}
                    </div>
                    <div class="form-group">
                        <label for="jurusan_id_jurusan">Jurusan</label>

                        {{ select("jurusan_id_jurusan", jurusan, "using":["id_jurusan", "nama_jurusan"], "useEmpty":true, "emptyText":"Pilih jurusan", "emptyValue":"", "class":"form-control") }}
                    </div>
                    <div class="form-group">
                        <label for="status_id_status">Status</label>
                        {{ select("status_id_status", status, "using":["id_status", "nama_status"], "useEmpty":true, "emptyText":"Pilih status", "emptyValue":"", "class":"form-control") }}
                    </div>
                    <div class="form-group">
                        <label for="jenis_kuliah">Jenis Kuliah</label>
                        {{ text_field("jenis_kuliah", "size" : 30, "class":"form-control", "placeholder":"Masukkan Jenis Kuliah") }}
                    </div>
                    <div class="form-group">
                        <label for="jenis_kuliah">Jenis Kuliah</label>
                        {{ text_field("jenis_kuliah", "size" : 30, "class":"form-control", "placeholder":"Masukkan Jenis Kuliah") }}
                    </div>
                </div>
                <div class="box-footer clearfix">
                    {{ hidden_field("id_mahasiswa") }}
                    {{ link_to("mahasiswa", "Batal", "class":"btn btn-default pull-left") }}
                    {{ submit_button("Simpan", "class":"btn btn-primary pull-right") }}
                </div>
            </div>
        </div>
    </div>
</form>