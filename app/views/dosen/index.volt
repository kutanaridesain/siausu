{#
/**
 * @author Fery Putra Tarigan <kutanaridesain@gmail.com>
 * @link http://kutanari.com, http://kutanaridesain.com
 * @since 2015
 * @license MIT License
 */
#}
<div class="row">
    <div class="col-xs-12">
        <div class="box box-success">
            <div class="box-header">
                <h3 class="box-title">Daftar Dosen</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
            {{ content() }}
            <div class="box-body clearfix">
                {{link_to("dosen/new", "Tambah", "class":"btn btn-success btn-xs pull-right")}}
            </div>
                <div class="box-body table-responsive">
                <table class="table table-bordered table-hover">
                    <colgroup>
                        <col width="2%"></col>
                        <col width="40%"></col>
                        <col width="40%"></col>
                        <col width="10%"></col>
                    </colgroup>
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Nama Dosen</th>
                            <th>NIP</th>
                            <th>Proses</th>
                        </tr>
                    </thead>
                    <tbody>
                        {% if page.items is defined %}
                        {% for dosen in page.items %}
                            {% set counter += 1 %}
                            <tr>
                                <td>{{ counter }}</td>
                                <td>{{ dosen.getNamaDosen() }}</td>
                                <td>{{ dosen.getNip() }}</td>
                                <td>
                                {{ link_to("dosen/detail/"~dosen.getNip(), '<i class="fa fa-fw fa-list-alt"></i>', 'data-toggle':"tooltip", 'title':'Detail', 'class':'btn btn-xs btn-warning') }} 

                                {{ link_to("dosen/edit/"~dosen.getNip(), '<i class="fa fa-fw fa-edit"></i>', 'data-toggle':"tooltip", 'title':'Edit', 'class':'btn btn-xs btn-primary') }} 

                                {{ link_to("dosen/delete/"~dosen.getNip(), '<i class="fa fa-fw fa-minus-circle"></i>', 'data-toggle':"tooltip", 'title':'Delete', 'class':'btn btn-xs btn-danger', "onclick":"javascript:if(confirm('Anda yakin menghapus item ini?')==false)return false;") }}</td>
                            </tr>
                        {% endfor %}
                        {% endif %}
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="3" align="center">
                            {{ link_to("jurusan/", "First") }} | 
                            {{ link_to("jurusan/?page="~page.before, "Previous") }} | 
                            {{ link_to("jurusan/?page="~page.next, "Next") }} | 
                            {{ link_to("jurusan/?page="~page.last, "Last") }} | 
                            {{ page.current~"/"~page.total_pages }}
                            </td>
                        </tr>
                    </tfoot>
                </table>
                </div>
                
            </div>
        </div>
    </div>
</div>

<!-- Button trigger modal -->
<a data-toggle="modal" href="remote.html" data-target="#myModal">Click me</a>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->