{#
/**
 * @author Fery Putra Tarigan <kutanaridesain@gmail.com>
 * @link http://kutanari.com, http://kutanaridesain.com
 * @since 2015
 * @license MIT License
 */
#}

{{ content() }}

<table width="100%">
    <tr>
        <td align="left">
            {{ link_to("dosen/index", "Go Back") }}
        </td>
        <td align="right">
            {{ link_to("dosen/new", "Create ") }}
        </td>
    </tr>
</table>

<table class="browse" align="center">
    <thead>
        <tr>
            <th>Nip</th>
            <th>Nama Of Dosen</th>
            <th>Jenis Of Kelamin</th>
            <th>Alamat</th>
            <th>Email</th>
            <th>Telp</th>
            <th>Status</th>
         </tr>
    </thead>
    <tbody>
    {% if page.items is defined %}
    {% for dosen in page.items %}
        <tr>
            <td>{{ dosen.getNip() }}</td>
            <td>{{ dosen.getNamaDosen() }}</td>
            <td>{{ dosen.getJenisKelamin() }}</td>
            <td>{{ dosen.getAlamat() }}</td>
            <td>{{ dosen.getEmail() }}</td>
            <td>{{ dosen.getTelp() }}</td>
            <td>{{ dosen.getStatus() }}</td>
            <td>{{ link_to("dosen/edit/"~dosen.getNip(), "Edit") }}</td>
            <td>{{ link_to("dosen/delete/"~dosen.getNip(), "Delete") }}</td>
        </tr>
    {% endfor %}
    {% endif %}
    </tbody>
    <tbody>
        <tr>
            <td colspan="2" align="right">
                <table align="center">
                    <tr>
                        <td>{{ link_to("dosen/search", "First") }}</td>
                        <td>{{ link_to("dosen/search?page="~page.before, "Previous") }}</td>
                        <td>{{ link_to("dosen/search?page="~page.next, "Next") }}</td>
                        <td>{{ link_to("dosen/search?page="~page.last, "Last") }}</td>
                        <td>{{ page.current~"/"~page.total_pages }}</td>
                    </tr>
                </table>
            </td>
        </tr>
    </tbody>
</table>
