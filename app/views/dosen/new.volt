{#
/**
 * @author Fery Putra Tarigan <kutanaridesain@gmail.com>
 * @link http://kutanari.com, http://kutanaridesain.com
 * @since 2015
 * @license MIT License
 */
#}
{{ form("dosen/create", "method":"post", "autocomplete" : "off", "role":"form") }}
{% if content() != "" %}
<div class="alert alert-danger alert-dismissable">
    <i class="fa fa-ban"></i>
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
{{ content() }}
</div>
{% endif %}
    <div class="row">
        <div class="col-md-6">
            <div class="box box-success">
                <div class="box-header">
                    <h3 class="box-title">Tambah Dosen</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group">
                        <label for="nip">NIP</label>
                        {{ text_field("nip", "size" : 30, "class":"form-control", "placeholder":"Masukkan NIP") }}
                    </div>
                    <div class="form-group">
                        <label for="nama_dosen">Nama</label>
                        {{ text_field("nama_dosen", "size" : 30, "class":"form-control", "placeholder":"Masukkan Nama Dosen") }}
                    </div>
                    <div class="form-group">
                        <label for="jenis_kelamin">Jenis Kelamin</label>
                        {{ select_static("jenis_kelamin", ["Laki-laki":"Laki-laki", "Perempuan":"Perempuan"], "class":"form-control", "placeholder":"") }}
                    </div>
                    <div class="form-group">
                        <label for="alamat">Alamat</label>
                        {{ text_area("alamat", "size" : 30, "class":"form-control", "placeholder":"Masukkan Alamat") }}
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        {{ text_field("email", "size" : 30, "class":"form-control", "placeholder":"email@domain.com") }}
                    </div>
                    <div class="form-group">
                        <label for="telp">Telepon</label>
                        {{ text_field("telp", "size" : 30, "class":"form-control", "placeholder":"Masukkan Telepon") }}
                    </div>
                    <div class="form-group">
                        <label for="status">Status</label>
                        {{ text_field("status", "size" : 30, "class":"form-control", "placeholder":"Masukkan Status") }}
                    </div>
                </div>
                <div class="box-footer clearfix">
                    {{link_to("dosen", "Batal", "class":"btn btn-default pull-left")}}
                    {{ submit_button("Simpan", "class":"btn btn-success pull-right") }}
                </div>
            </div>
        </div>
    </div>
</form>