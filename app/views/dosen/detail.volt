{#
/**
 * @author Fery Putra Tarigan <kutanaridesain@gmail.com>
 * @link http://kutanari.com, http://kutanaridesain.com
 * @since 2015
 * @license MIT License
 */
#}
{{ content() }}
<div class="row">
    <div class="col-md-6">
        <div class="box box-warning">
            <div class="box-header">
                <h3 class="box-title">Detail data <em>{{ dosen.getNamaDosen() }}</em></h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <div class="clearfix">
                    <table class="detail-info pull-left">
                        <colgroup>
                            <col width="50%"></col>
                            <col width="50%"></col>
                        </colgroup>
                        <tr>
                            <th>Nama </th>
                            <td><strong>:</strong> {{dosen.getNamaDosen()}}</td>
                        </tr>
                        <tr>
                            <th>NIP </th>
                            <td><strong>:</strong> {{dosen.getNip()}}</td>
                        </tr>
                        <tr>
                            <th>Jenis Kelamin </th>
                            <td><strong>:</strong> {{dosen.getJenisKelamin()}}</td>
                        </tr>
                        <tr>
                            <th>Alamat </th>
                            <td><strong>:</strong> {{dosen.getAlamat()}}</td>
                        </tr>
                        <tr>
                            <th>Email </th>
                            <td><strong>:</strong> {{dosen.getEmail()}}</td>
                        </tr>
                        <tr>
                            <th>Telepon </th>
                            <td><strong>:</strong> {{dosen.getTelp()}}</td>
                        </tr>
                        <tr>
                            <th>Status </th>
                            <td><strong>:</strong> {{dosen.getStatus()}}</td>
                        </tr>
                    </table>    
                </div>
                <div class="box-footer clearfix">
                    {{ link_to("dosen", "Kembali", "class":"btn btn-default pull-right") }}
                </div>
            </div>
        </div>
    </div>
</div>
