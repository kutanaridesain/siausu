{#
/**
 * @author Fery Putra Tarigan <kutanaridesain@gmail.com>
 * @link http://kutanari.com, http://kutanaridesain.com
 * @since 2015
 * @license MIT License
 */
#}
<!DOCTYPE html>
<html class="bg-black">
    <head>
        <meta charset="UTF-8">
        <title>Sistem Informasi Akademik DTE USU</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <link href="/public/css/backend.css" rel="stylesheet" type="text/css" />
        <link href="/public/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="/public/css/ionicons.min.css" rel="stylesheet" type="text/css" />
        {{ assets.outputCss() }}
        <!-- Theme style -->
        <link href="/public/css/adminDTE.css" rel="stylesheet" type="text/css" />


        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="bg-black">
        <div class="form-box" id="login-box">
        {{content()}}
            <div class="header">Masuk SIA DTE USU</div>
            <form class="box-shadow" action="/login" method="post">
                <div class="body bg-gray">
                    <div class="form-group">
                        <input type="text" name="username" class="form-control" placeholder="NIP atau NIM"/>
                    </div>
                    <div class="form-group">
                        <input type="password" name="password" class="form-control" placeholder="Kata sandi"/>
                    </div>          
                    <div class="form-group">
                        <!-- <input type="checkbox" name="remember_me"/> Remember me -->
                    </div>
                </div>
                <div class="footer">                                                               
                    <button type="submit" class="btn bg-olive btn-block">Masuk</button>  
                    
                    <!-- <p><a href="#">I forgot my password</a></p> -->
                    
                    <!-- <a href="register.html" class="text-center">Register a new membership</a> -->
                </div>
            </form>
            <p align="center">Copyleft <span class="copyleft">&copy;</span> DTE USU 2015</p>
        </div>

        <!-- jQuery 2.0.2 -->
        <script src="/public/js/jquery/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="/public/js/bootstrap/bootstrap.min.js" type="text/javascript"></script>

    </body>
</html>