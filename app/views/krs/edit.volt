{#
/**
 * @author Fery Putra Tarigan <kutanaridesain@gmail.com>
 * @link http://kutanari.com, http://kutanaridesain.com
 * @since 2015
 * @license MIT License
 */
#}
{{ form("krs/save", "method":"post", "autocomplete" : "off", "role":"form") }}
{% if content() != "" %}
<div class="alert alert-danger alert-dismissable">
    <i class="fa fa-ban"></i>
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
{{ content() }}
</div>
{% endif %}
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Ubah Krs</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="row form-group">
                        <div class="col-xs-6">
                            <label for="mahasiswa_nim">Mahasiswa</label>
                            {{ select('mahasiswa_nim', mahasiswa, 'using':['nim', 'nama'], 'class':'form-control') }}
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-xs-4">
                            <label for="ta">TA</label>
                            {{ select('ta', ta, 'class':'form-control') }}
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-xs-3">
                            <label for="ta">Semester</label>
                            {{ select_static('semester', ['Ganjil':'Ganjil', 'Genap':'Genap'], 'class':'form-control') }}
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-xs-8">
                            <label for="jadwal_id_jadwal">Jadwal</label>
                            {#{select('jadwal_id_jadwal', jadwal, 'using':['id_jadwal', 'nama_matakuliah'], 'class':'form-control')}#}
                            <select class="selectpicker" name="jadwal_id_jadwal" data-width="100%">
                                {% for jadwalItem in jadwal %}
                                    <option data-subtext="{{jadwalItem.getNamaRuangan()}}|{{jadwalItem.getMulai()}} s/d {{jadwalItem.getSelesai()}}" value="{{jadwalItem.getIdJadwal()}}">{{jadwalItem.getNamaMatakuliah()}} - {{jadwalItem.getKodeMatakuliah()}}</option>
                                {% endfor %}
                            </select>
                        </div>
                    </div>
                </div>
                <div class="box-footer clearfix">
                    {{ hidden_field("id_krs") }}
                    {{ link_to("krs", "Batal", "class":"btn btn-default pull-left") }}
                    {{ submit_button("Simpan", "class":"btn btn-primary pull-right") }}
                </div>
            </div>
        </div>
    </div>
</form>