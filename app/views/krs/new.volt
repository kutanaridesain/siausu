{#
/**
 * @author Fery Putra Tarigan <kutanaridesain@gmail.com>
 * @link http://kutanari.com, http://kutanaridesain.com
 * @since 2015
 * @license MIT License
 */
#}
{%- macro option_list(jkul, hari, namaHari) %}
<optgroup label="{{namaHari}}">
    {% set counter = 0 %}
    {% for jadwal in jkul %}
        {% if jadwal.hari is hari %}
            {% set counter += 1 %}
            <option data-subtext="{{jadwal.nama_ruangan}}|{{jadwal.mulai}} s/d {{jadwal.selesai}}" value="{{jadwal.id_jadwal}}">{{jadwal.nama_matakuliah}} - {{jadwal.kode_matakuliah}}</option>
        {% endif %}
    {% endfor %}
</optgroup>
{%- endmacro %}

{{ form("krs/create", "method":"post", "autocomplete" : "off", "role":"form") }}
{% if content() != "" %}
<div class="alert alert-danger alert-dismissable">
    <i class="fa fa-ban"></i>
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
{{ content() }}
</div>
{% endif %}
    <div class="row">
        <div class="col-md-6">
            <div class="box box-success">
                <div class="box-header">
                    <h3 class="box-title">Tambah KRS</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="row form-group">
                        <div class="col-xs-6">
                            <label for="mahasiswa_nim">Mahasiswa</label>
                            {{ select('mahasiswa_nim', mahasiswa, 'using':['nim', 'nama'], 'class':'form-control') }}
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-xs-4">
                            <label for="ta">TA</label>
                            {{ select('ta', ta, 'class':'form-control') }}
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-xs-3">
                            <label for="ta">Semester</label>
                            {{ select_static('semester', ['Ganjil':'Ganjil', 'Genap':'Genap'], 'class':'form-control') }}
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-xs-8">
                            <label for="jadwal_id_jadwal">Jadwal</label>
                            <select class="selectpicker" name="jadwal_id_jadwal" data-width="100%">
                                {% set curntHari = null %}
                                {% for jd in jk %}

                                    {% if jd.hari is curntHari %}
                                        {% continue %}
                                    {% else %}
                                        {% set curntHari = jd.hari %}
                                        {{ option_list("jkul":jk, "hari":curntHari, "namaHari":arrHari[jd.hari]) }}
                                    {% endif %}
                                {% endfor %}
                            </select>
                        </div>
                    </div>
                </div>
                <div class="box-footer clearfix">
                    {{link_to("krs", "Batal", "class":"btn btn-default pull-left")}}
                    {{ submit_button("Simpan", "class":"btn btn-success pull-right") }}
                </div>
            </div>
        </div>
    </div>
</form>