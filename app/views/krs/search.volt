{#
/**
 * @author Fery Putra Tarigan <kutanaridesain@gmail.com>
 * @link http://kutanari.com, http://kutanaridesain.com
 * @since 2015
 * @license MIT License
 */
#}

{{ content() }}

<table width="100%">
    <tr>
        <td align="left">
            {{ link_to("krs/index", "Go Back") }}
        </td>
        <td align="right">
            {{ link_to("krs/new", "Create ") }}
        </td>
    </tr>
</table>

<table class="browse" align="center">
    <thead>
        <tr>
            <th>Id Of Krs</th>
            <th>Ta</th>
            <th>Semester</th>
            <th>Mahasiswa Of Nim</th>
            <th>Jadwal Of Id Of Jadwal</th>
         </tr>
    </thead>
    <tbody>
    {% if page.items is defined %}
    {% for kr in page.items %}
        <tr>
            <td>{{ kr.getIdKrs() }}</td>
            <td>{{ kr.getTa() }}</td>
            <td>{{ kr.getSemester() }}</td>
            <td>{{ kr.getMahasiswaNim() }}</td>
            <td>{{ kr.getJadwalIdJadwal() }}</td>
            <td>{{ link_to("krs/edit/"~kr.getIdKrs(), "Edit") }}</td>
            <td>{{ link_to("krs/delete/"~kr.getIdKrs(), "Delete") }}</td>
        </tr>
    {% endfor %}
    {% endif %}
    </tbody>
    <tbody>
        <tr>
            <td colspan="2" align="right">
                <table align="center">
                    <tr>
                        <td>{{ link_to("krs/search", "First") }}</td>
                        <td>{{ link_to("krs/search?page="~page.before, "Previous") }}</td>
                        <td>{{ link_to("krs/search?page="~page.next, "Next") }}</td>
                        <td>{{ link_to("krs/search?page="~page.last, "Last") }}</td>
                        <td>{{ page.current~"/"~page.total_pages }}</td>
                    </tr>
                </table>
            </td>
        </tr>
    </tbody>
</table>
