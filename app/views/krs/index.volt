{#
/**
 * @author Fery Putra Tarigan <kutanaridesain@gmail.com>
 * @link http://kutanari.com, http://kutanaridesain.com
 * @since 2015
 * @license MIT License
 */
#}
<div class="row">
    <div class="col-xs-12">
                {{ content() }}
        <div class="box box-success">
            
            <div class="box-header">
                <h3 class="box-title">Daftar KRS</h3>
            </div><!-- /.box-header -->
            <div class="box-body clearfix">
                {{link_to("krs/new", "Tambah", "class":"btn btn-success btn-xs pull-right")}}
            </div>
            <div class="box-body">
            
            <div class="row">
                <div class="col-md-4">
                    {{ form("krs/index", "method":"post", "autocomplete" : "off", "role":"form") }}
                        <div class="box-body">
                             <div class="input-group">
                              {{ text_field("mahasiswa_nim", "size" : 30, "class":"form-control", "placeholder":"NIM Mahasiswa") }}
                              <span class="input-group-btn">
                                {{ submit_button("Tampilkan KRS", "class":"btn btn-success") }}
                              </span>
                            </div><!-- /input-group -->
                        </div>
                    </form>
                </div>
            </div>    
            {% if page.items is defined %}
                <div class="box-body table-responsive">
                <table class="table table-bordered table-hover">
                    <colgroup>
                        <col width="2%"></col>
                        <col width="48%"></col>
                        <col width="30%"></col>
                        <col width="20%"></col>
                    </colgroup>
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Tahun Ajaran</th>
                            <th>NIM</th>
                            <th>Proses</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                        {% for krs in page.items %}
                            {% set counter += 1 %}
                            <tr>
                                <td>{{ counter }}</td>
                                <td>{{ krs.getTa() }}</td>
                                <td>{{ krs.getMahasiswaNim() }}</td>
                                <td align="center">
                                    {{ link_to("krs/detail/"~krs.getIdKRS(), '<i class="fa fa-fw fa-list-alt"></i>', 'data-toggle':"tooltip", 'title':'Detail', 'class':'btn btn-xs btn-warning') }} 

                                    {{ link_to("krs/edit/"~krs.getIdKRS(), '<i class="fa fa-fw fa-edit"></i>', 'data-toggle':"tooltip", 'title':'Edit', 'class':'btn btn-xs btn-primary') }} 

                                    {{ link_to("krs/delete/"~krs.getIdKRS(), '<i class="fa fa-fw fa-minus-circle"></i>', 'data-toggle':"tooltip", 'title':'Delete', 'class':'btn btn-xs btn-danger', "onclick":"javascript:if(confirm('Anda yakin menghapus item ini?')==false)return false;") }}
                                </td>
                            </tr>
                        {% endfor %}
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="3" align="center">
                                {{ link_to("krs/", "First") }} | 
                                {{ link_to("krs/?page="~page.before, "Previous") }} | 
                                {{ link_to("krs/?page="~page.next, "Next") }} | 
                                {{ link_to("krs/?page="~page.last, "Last") }} | 
                                {{ page.current~"/"~page.total_pages }}
                            </td>
                        </tr>
                    </tfoot>
                </table>
                </div>
            {% endif %}
            </div>
        </div>
    </div>
</div>

