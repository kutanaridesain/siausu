{#
/**
 * @author Fery Putra Tarigan <kutanaridesain@gmail.com>
 * @link http://kutanari.com, http://kutanaridesain.com
 * @since 2015
 * @license MIT License
 */
#}

<div class="row">
    <div class="col-xs-12">
        <div class="box box-success">
            <div class="box-header">
                <h3 class="box-title">Import DNA </h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="box-body clearfix">
                    {{ form("peralatan/importdna", "method":"post", "autocomplete" : "off", "role":"form", "enctype":"multipart/form-data") }}
                    <div class="form-group">
                        <div class="row">
                            <div class="col-xs-5">
                                <label for="pengampu_id_pengampu">Pilih Matakuliah</label>
                                <select class="selectpicker" name="pengampu_id_pengampu" data-width="100%">
                                    {% for jk in jadwalkuliah %}
                                        <option data-subtext="" value="{{jk.getIdPengampu()}}">{{jk.matakuliah.getNamaMatakuliah()}} / {{jk.matakuliah.getKodeMatakuliah()}} - {{jk.dosen.getNamaDosen()}}</option>
                                    {% endfor %}
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="kode_matakuliah">Pilih file csv yang akan di import</label>
                        {{ file_field('dna_csv', 'class': 'input-xxlarge', 'style' : 'font-size:15px; height:40px; margin-top: 3px;') }}
                        {{ submit_button('Unggah', 'class': 'btn btn-primary btn-large btn-success') }}
                      </div>
                    </form>
                {{ content() }}
                </div>
            </div>
        </div>
    </div>
</div>