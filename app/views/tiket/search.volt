
{{ content() }}

<table width="100%">
    <tr>
        <td align="left">
            {{ link_to("tiket/index", "Go Back") }}
        </td>
        <td align="right">
            {{ link_to("tiket/new", "Create ") }}
        </td>
    </tr>
</table>

<table class="browse" align="center">
    <thead>
        <tr>
            <th>Id Of Tiket</th>
            <th>Judul Of Aduan</th>
            <th>Isi Of Pesan</th>
            <th>Status</th>
            <th>Mahasiswa Of Nim</th>
            <th>Tanggal Of Register</th>
         </tr>
    </thead>
    <tbody>
    {% if page.items is defined %}
    {% for tiket in page.items %}
        <tr>
            <td>{{ tiket.id_tiket }}</td>
            <td>{{ tiket.judul_aduan }}</td>
            <td>{{ tiket.isi_pesan }}</td>
            <td>{{ tiket.status }}</td>
            <td>{{ tiket.mahasiswa_nim }}</td>
            <td>{{ tiket.tanggal_register }}</td>
            <td>{{ link_to("tiket/edit/"~tiket.id_tiket, "Edit") }}</td>
            <td>{{ link_to("tiket/delete/"~tiket.id_tiket, "Delete") }}</td>
        </tr>
    {% endfor %}
    {% endif %}
    </tbody>
    <tbody>
        <tr>
            <td colspan="2" align="right">
                <table align="center">
                    <tr>
                        <td>{{ link_to("tiket/search", "First") }}</td>
                        <td>{{ link_to("tiket/search?page="~page.before, "Previous") }}</td>
                        <td>{{ link_to("tiket/search?page="~page.next, "Next") }}</td>
                        <td>{{ link_to("tiket/search?page="~page.last, "Last") }}</td>
                        <td>{{ page.current~"/"~page.total_pages }}</td>
                    </tr>
                </table>
            </td>
        </tr>
    </tbody>
</table>
