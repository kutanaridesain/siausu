{{ content() }}
{{ form("tiket/save", "method":"post") }}

<table width="100%">
    <tr>
        <td align="left">{{ link_to("tiket", "Go Back") }}</td>
        <td align="right">{{ submit_button("Save") }}</td>
    </tr>
</table>

<div align="center">
    <h1>Edit tiket</h1>
</div>

<table>
    <tr>
        <td align="right">
            <label for="judul_aduan">Judul Of Aduan</label>
        </td>
        <td align="left">
            {{ text_field("judul_aduan", "size" : 30) }}
        </td>
    </tr>
    <tr>
        <td align="right">
            <label for="isi_pesan">Isi Of Pesan</label>
        </td>
        <td align="left">
                {{ text_field("isi_pesan", "type" : "date") }}
        </td>
    </tr>
    <tr>
        <td align="right">
            <label for="status">Status</label>
        </td>
        <td align="left">
                {{ text_field("status") }}
        </td>
    </tr>
    <tr>
        <td align="right">
            <label for="mahasiswa_nim">Mahasiswa Of Nim</label>
        </td>
        <td align="left">
            {{ text_field("mahasiswa_nim", "size" : 30) }}
        </td>
    </tr>
    <tr>
        <td align="right">
            <label for="tanggal_register">Tanggal Of Register</label>
        </td>
        <td align="left">
            {{ text_field("tanggal_register", "size" : 30) }}
        </td>
    </tr>

    <tr>
        <td>{{ hidden_field("id") }}</td>
        <td>{{ submit_button("Save") }}</td>
    </tr>
</table>

</form>
