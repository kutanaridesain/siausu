{{ form("tiket/create", "method":"post", "autocomplete" : "off", "role":"form") }}
{% if content() != "" %}
<div class="alert alert-danger alert-dismissable">
    <i class="fa fa-ban"></i>
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
{{ content() }}
</div>
{% endif %}
    <div class="row">
        <div class="col-md-6">
            <div class="box box-success">
                <div class="box-header">
                    <h3 class="box-title">Tambah Tiket</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group">
                        <label for="tiket">Tiket</label>
                        {{ text_field("tiket", "size" : 30, "class":"form-control", "placeholder":"Masukkan Tiket") }}
                    </div>
                </div>
                <div class="box-footer clearfix">
                    {{link_to("tiket", "Batal", "class":"btn btn-default pull-left")}}
                    {{ submit_button("Simpan", "class":"btn btn-success pull-right") }}
                </div>
            </div>
        </div>
    </div>
</form>


{{ form("tiket/create", "method":"post") }}

<table width="100%">
    <tr>
        <td align="left">{{ link_to("tiket", "Go Back") }}</td>
        <td align="right">{{ submit_button("Save") }}</td>
    </tr>
</table>

{{ content() }}

<div align="center">
    <h1>Create tiket</h1>
</div>

<table>
    <tr>
        <td align="right">
            <label for="judul_aduan">Judul Of Aduan</label>
        </td>
        <td align="left">
            {{ text_field("judul_aduan", "size" : 30) }}
        </td>
    </tr>
    <tr>
        <td align="right">
            <label for="isi_pesan">Isi Of Pesan</label>
        </td>
        <td align="left">
                {{ text_field("isi_pesan", "type" : "date") }}
        </td>
    </tr>
    <tr>
        <td align="right">
            <label for="status">Status</label>
        </td>
        <td align="left">
                {{ text_field("status") }}
        </td>
    </tr>
    <tr>
        <td align="right">
            <label for="mahasiswa_nim">Mahasiswa Of Nim</label>
        </td>
        <td align="left">
            {{ text_field("mahasiswa_nim", "size" : 30) }}
        </td>
    </tr>
    <tr>
        <td align="right">
            <label for="tanggal_register">Tanggal Of Register</label>
        </td>
        <td align="left">
            {{ text_field("tanggal_register", "size" : 30) }}
        </td>
    </tr>

    <tr>
        <td></td>
        <td>{{ submit_button("Save") }}</td>
    </tr>
</table>

</form>
