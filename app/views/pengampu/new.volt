{#
/**
 * @author Fery Putra Tarigan <kutanaridesain@gmail.com>
 * @link http://kutanari.com, http://kutanaridesain.com
 * @since 2015
 * @license MIT License
 */
#}
{{ form("pengampu/create", "method":"post", "autocomplete" : "off", "role":"form") }}
{% if content() != "" %}
<div class="alert alert-danger alert-dismissable">
    <i class="fa fa-ban"></i>
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
{{ content() }}
</div>
{% endif %}
    <div class="row">
        <div class="col-md-6">
            <div class="box box-success">
                <div class="box-header">
                    <h3 class="box-title">Tambah Pengampu</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <label for="matakuliah_id_matakuliah">Mata Kuliah</label>
                    <div class="form-group input-group">
                        {{ select('matakuliah_id_matakuliah', matakuliah, 'using':['id_matakuliah', 'nama_matakuliah'], "class":"form-control") }}
                        <div class="input-group-btn">
                            {{link_to('#', '<i class="fa fa-fw fa-plus-square-o"></i>Tambah', 'data-toggle':'tooltip', 'title':'Tambah mata kuliah', 'class':'btn btn-success', 'data-toggle':'modal', 'data-target':'#compose-modal')}}
                        </div>
                    </div>
                    <label for="dosen_nip">Dosen Pengampu</label>
                    <div class="form-group input-group">
                        {{ select('dosen_nip', dosen, 'using':['nip', 'nama_dosen'], "class":"form-control") }}
                        <div class="input-group-btn">
                            {{link_to("dosen/new", '<i class="fa fa-fw fa-plus"></i>', 'data-toggle':"tooltip", 'title':'Tambah data dosen', "class":"btn btn-success")}}
                        </div>
                    </div>
                </div>
                <div class="box-footer clearfix">
                    {{link_to("pengampu", "Batal", "class":"btn btn-default pull-left")}}
                    {{ submit_button("Simpan", "class":"btn btn-success pull-right") }}
                </div>
            </div>
        </div>
    </div>
</form>

<!-- modal matakuliah -->
        <div class="modal fade" id="compose-modal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title"><i class="fa fa-envelope-o"></i> Tambah Mata Kuliah</h4>
                    </div>
                    {{ form("matakuliah/create", "method":"post", "autocomplete" : "off", "role":"form") }}
                    <form action="#" method="post">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="kode_matakuliah">Kode Mata Kuliah</label>
                                        {{ text_field("kode_matakuliah", "size" : 30, "class":"form-control", "placeholder":"masukkan kode mata kuliah") }}
                                    </div>
                                    <div class="form-group">
                                        <label for="nama_matakuliah">Nama Mata Kuliah</label>
                                        {{ text_field("nama_matakuliah", "size" : 30, "class":"form-control", "placeholder":"masukkan nama mata kuliah") }}
                                    </div>
                                    <div class="form-group">
                                        <label for="sks">SKS</label>
                                        {{ select_static('sks', ['1':'1', '2':'2', '3':'3', '4':'4'], "class":"form-control") }}
                                    </div>
                                    <div class="form-group">
                                        <label for="semester">Semester</label>
                                        {{ text_field("semester", "size" : 30, "type":"numeric", "class":"form-control", "placeholder":"masukkan angka semester") }}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer clearfix">

                            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Batal</button>

                            <button type="submit" class="btn btn-primary pull-left"><i class="fa fa-plus"></i> Tambah</button>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->