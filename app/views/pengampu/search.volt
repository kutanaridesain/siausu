{#
/**
 * @author Fery Putra Tarigan <kutanaridesain@gmail.com>
 * @link http://kutanari.com, http://kutanaridesain.com
 * @since 2015
 * @license MIT License
 */
#}

{{ content() }}

<table width="100%">
    <tr>
        <td align="left">
            {{ link_to("pengampu/index", "Go Back") }}
        </td>
        <td align="right">
            {{ link_to("pengampu/new", "Create ") }}
        </td>
    </tr>
</table>

<table class="browse" align="center">
    <thead>
        <tr>
            <th>Id Of Pengampu</th>
            <th>Dosen Of Nip</th>
            <th>Matakuliah Of Id Of Matakuliah</th>
         </tr>
    </thead>
    <tbody>
    {% if page.items is defined %}
    {% for pengampu in page.items %}
        <tr>
            <td>{{ pengampu.getIdPengampu() }}</td>
            <td>{{ pengampu.getDosenNip() }}</td>
            <td>{{ pengampu.getMatakuliahIdMatakuliah() }}</td>
            <td>{{ link_to("pengampu/edit/"~pengampu.getIdPengampu(), "Edit") }}</td>
            <td>{{ link_to("pengampu/delete/"~pengampu.getIdPengampu(), "Delete") }}</td>
        </tr>
    {% endfor %}
    {% endif %}
    </tbody>
    <tbody>
        <tr>
            <td colspan="2" align="right">
                <table align="center">
                    <tr>
                        <td>{{ link_to("pengampu/search", "First") }}</td>
                        <td>{{ link_to("pengampu/search?page="~page.before, "Previous") }}</td>
                        <td>{{ link_to("pengampu/search?page="~page.next, "Next") }}</td>
                        <td>{{ link_to("pengampu/search?page="~page.last, "Last") }}</td>
                        <td>{{ page.current~"/"~page.total_pages }}</td>
                    </tr>
                </table>
            </td>
        </tr>
    </tbody>
</table>
