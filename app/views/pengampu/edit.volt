{#
/**
 * @author Fery Putra Tarigan <kutanaridesain@gmail.com>
 * @link http://kutanari.com, http://kutanaridesain.com
 * @since 2015
 * @license MIT License
 */
#}
{{ form("pengampu/save", "method":"post", "autocomplete" : "off", "role":"form") }}
{% if content() != "" %}
<div class="alert alert-danger alert-dismissable">
    <i class="fa fa-ban"></i>
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
{{ content() }}
</div>
{% endif %}
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Ubah Nama Pengampu</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group">
                        <label for="dosen_nip">Mata Kuliah</label>
                        {{ select('matakuliah_id_matakuliah', matakuliah, 'using':['id_matakuliah', 'nama_matakuliah'], "class":"form-control") }}
                    </div>
                    <div class="form-group">
                        <label for="dosen_nip">Dosen Pengampu</label>
                        {{ select('dosen_nip', dosen, 'using':['nip', 'nama_dosen'], "class":"form-control") }}
                    </div>
                </div>
                <div class="box-footer clearfix">
                    {{ hidden_field("id_pengampu") }}
                    {{ link_to("pengampu", "Batal", "class":"btn btn-default pull-left") }}
                    {{ submit_button("Simpan", "class":"btn btn-primary pull-right") }}
                </div>
            </div>
        </div>
    </div>
</form>