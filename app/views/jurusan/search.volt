{#
/**
 * @author Fery Putra Tarigan <kutanaridesain@gmail.com>
 * @link http://kutanari.com, http://kutanaridesain.com
 * @since 2015
 * @license MIT License
 */
#}

{{ content() }}

<table width="100%">
    <tr>
        <td align="left">
            {{ link_to("jurusan/index", "Go Back") }}
        </td>
        <td align="right">
            {{ link_to("jurusan/new", "Create ") }}
        </td>
    </tr>
</table>

<table class="browse" align="center">
    <thead>
        <tr>
            <th>Id Of Jurusan</th>
            <th>Nama Of Jurusan</th>
         </tr>
    </thead>
    <tbody>
    {% if page.items is defined %}
    {% for jurusan in page.items %}
        <tr>
            <td>{{ jurusan.getIdJurusan() }}</td>
            <td>{{ jurusan.getNamaJurusan() }}</td>
            <td>{{ link_to("jurusan/edit/"~jurusan.getIdJurusan(), "Edit") }}</td>
            <td>{{ link_to("jurusan/delete/"~jurusan.getIdJurusan(), "Delete") }}</td>
        </tr>
    {% endfor %}
    {% endif %}
    </tbody>
    <tbody>
        <tr>
            <td colspan="2" align="right">
                <table align="center">
                    <tr>
                        <td>{{ link_to("jurusan/search", "First") }}</td>
                        <td>{{ link_to("jurusan/search?page="~page.before, "Previous") }}</td>
                        <td>{{ link_to("jurusan/search?page="~page.next, "Next") }}</td>
                        <td>{{ link_to("jurusan/search?page="~page.last, "Last") }}</td>
                        <td>{{ page.current~"/"~page.total_pages }}</td>
                    </tr>
                </table>
            </td>
        </tr>
    </tbody>
</table>
