
{{ content() }}

<table width="100%">
    <tr>
        <td align="left">
            {{ link_to("khs/index", "Go Back") }}
        </td>
        <td align="right">
            {{ link_to("khs/new", "Create ") }}
        </td>
    </tr>
</table>

<table class="browse" align="center">
    <thead>
        <tr>
            <th>Id Of Khs</th>
            <th>Ta</th>
            <th>Semester</th>
            <th>Mahasiswa Of Nim</th>
            <th>Jadwal Of Id Of Jadwal</th>
         </tr>
    </thead>
    <tbody>
    {% if page.items is defined %}
    {% for kh in page.items %}
        <tr>
            <td>{{ kh.getIdKhs() }}</td>
            <td>{{ kh.getTa() }}</td>
            <td>{{ kh.getSemester() }}</td>
            <td>{{ kh.getMahasiswaNim() }}</td>
            <td>{{ kh.getJadwalIdJadwal() }}</td>
            <td>{{ link_to("khs/edit/"~kh.getIdKhs(), "Edit") }}</td>
            <td>{{ link_to("khs/delete/"~kh.getIdKhs(), "Delete") }}</td>
        </tr>
    {% endfor %}
    {% endif %}
    </tbody>
    <tbody>
        <tr>
            <td colspan="2" align="right">
                <table align="center">
                    <tr>
                        <td>{{ link_to("khs/search", "First") }}</td>
                        <td>{{ link_to("khs/search?page="~page.before, "Previous") }}</td>
                        <td>{{ link_to("khs/search?page="~page.next, "Next") }}</td>
                        <td>{{ link_to("khs/search?page="~page.last, "Last") }}</td>
                        <td>{{ page.current~"/"~page.total_pages }}</td>
                    </tr>
                </table>
            </td>
        </tr>
    </tbody>
</table>
