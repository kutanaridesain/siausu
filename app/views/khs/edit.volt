{{ content() }}
{{ form("khs/save", "method":"post") }}

<table width="100%">
    <tr>
        <td align="left">{{ link_to("khs", "Go Back") }}</td>
        <td align="right">{{ submit_button("Save") }}</td>
    </tr>
</table>

<div align="center">
    <h1>Edit khs</h1>
</div>

<table>
    <tr>
        <td align="right">
            <label for="ta">Ta</label>
        </td>
        <td align="left">
            {{ text_field("ta", "size" : 30) }}
        </td>
    </tr>
    <tr>
        <td align="right">
            <label for="semester">Semester</label>
        </td>
        <td align="left">
                {{ text_field("semester") }}
        </td>
    </tr>
    <tr>
        <td align="right">
            <label for="mahasiswa_nim">Mahasiswa Of Nim</label>
        </td>
        <td align="left">
            {{ text_field("mahasiswa_nim", "size" : 30) }}
        </td>
    </tr>
    <tr>
        <td align="right">
            <label for="jadwal_id_jadwal">Jadwal Of Id Of Jadwal</label>
        </td>
        <td align="left">
            {{ text_field("jadwal_id_jadwal", "type" : "numeric") }}
        </td>
    </tr>

    <tr>
        <td>{{ hidden_field("id") }}</td>
        <td>{{ submit_button("Save") }}</td>
    </tr>
</table>

</form>
