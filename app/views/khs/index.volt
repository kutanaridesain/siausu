
{{ content() }}

<div align="right">
    {{ link_to("khs/new", "Create khs") }}
</div>

{{ form("khs/search", "method":"post", "autocomplete" : "off") }}

<div align="center">
    <h1>Search khs</h1>
</div>

<table>
    <tr>
        <td align="right">
            <label for="id_khs">Id Of Khs</label>
        </td>
        <td align="left">
            {{ text_field("id_khs", "type" : "numeric") }}
        </td>
    </tr>
    <tr>
        <td align="right">
            <label for="ta">Ta</label>
        </td>
        <td align="left">
            {{ text_field("ta", "size" : 30) }}
        </td>
    </tr>
    <tr>
        <td align="right">
            <label for="semester">Semester</label>
        </td>
        <td align="left">
                {{ text_field("semester") }}
        </td>
    </tr>
    <tr>
        <td align="right">
            <label for="mahasiswa_nim">Mahasiswa Of Nim</label>
        </td>
        <td align="left">
            {{ text_field("mahasiswa_nim", "size" : 30) }}
        </td>
    </tr>
    <tr>
        <td align="right">
            <label for="jadwal_id_jadwal">Jadwal Of Id Of Jadwal</label>
        </td>
        <td align="left">
            {{ text_field("jadwal_id_jadwal", "type" : "numeric") }}
        </td>
    </tr>

    <tr>
        <td></td>
        <td>{{ submit_button("Search") }}</td>
    </tr>
</table>

</form>
