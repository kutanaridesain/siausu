{#
/**
 * @author Fery Putra Tarigan <kutanaridesain@gmail.com>
 * @link http://kutanari.com, http://kutanaridesain.com
 * @since 2015
 * @license MIT License
 */
#}
{{ form("jadwal/save", "method":"post", "autocomplete" : "off", "role":"form") }}
{% if content() != "" %}
<div class="alert alert-danger alert-dismissable">
    <i class="fa fa-ban"></i>
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
{{ content() }}
</div>
{% endif %}
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Ubah Jadwal</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group">
                        <label for="pengampu_id_pengampu">Mata Kuliah / Pengampu</label>
                        {{ select('pengampu_id_pengampu', arrPengampu, 'using':['id', 'nama_pengampu'], "class":"form-control") }}
                    </div>
                    <div class="row form-group">
                        <div class="col-xs-3">
                            <label for="hari">Hari</label>
                            {{ select_static('hari', ["1":"Senin", "2":"Selasa", "3":"Rabu", "4":"Kamis", "5":"Jumat", "6":"Sabtu", "7":"Minggu"], "class":"form-control") }}
                        </div>
                    </div>
                    <label for="jam">Jam</label>
                    <div class="row form-group">
                      <div class="col-xs-2 bootstrap-timepicker">
                        {{ text_field("mulai", "class":"form-control input-sm timepicker", "placeholder":"12:00") }}
                      </div>
                      <div class="col-xs-1">
                          <label for="">s/d</label>
                      </div>
                      <div class="col-xs-2 bootstrap-timepicker">
                        {{ text_field("selesai", "class":"form-control input-sm timepicker", "placeholder":"12:00") }}
                      </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-xs-3">
                            <label for="ruangan_id_ruangan">Ruangan</label>
                            {{ select('ruangan_id_ruangan', ruangan, "using":["id_ruangan", "nama_ruangan"], "class":"form-control") }}
                        </div>
                    </div>
                </div>
                <div class="box-footer clearfix">
                    {{ hidden_field("id_jadwal") }}
                    {{ link_to("jadwal", "Batal", "class":"btn btn-default pull-left") }}
                    {{ submit_button("Simpan", "class":"btn btn-primary pull-right") }}
                </div>
            </div>
        </div>
    </div>
</form>