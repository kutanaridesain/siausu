{#
/**
 * @author Fery Putra Tarigan <kutanaridesain@gmail.com>
 * @link http://kutanari.com, http://kutanaridesain.com
 * @since 2015
 * @license MIT License
 */
#}

{{ content() }}

<table width="100%">
    <tr>
        <td align="left">
            {{ link_to("jadwal/index", "Go Back") }}
        </td>
        <td align="right">
            {{ link_to("jadwal/new", "Create ") }}
        </td>
    </tr>
</table>

<table class="browse" align="center">
    <thead>
        <tr>
            <th>Id Of Jadwal</th>
            <th>Hari</th>
            <th>Mulai</th>
            <th>Selesai</th>
            <th>Pengampu Of Id Of Pengampu</th>
            <th>Ruangan Of Id Of Ruangan</th>
         </tr>
    </thead>
    <tbody>
    {% if page.items is defined %}
    {% for jadwal in page.items %}
        <tr>
            <td>{{ jadwal.getIdJadwal() }}</td>
            <td>{{ jadwal.getHari() }}</td>
            <td>{{ jadwal.getMulai() }}</td>
            <td>{{ jadwal.getSelesai() }}</td>
            <td>{{ jadwal.getPengampuIdPengampu() }}</td>
            <td>{{ jadwal.getRuanganIdRuangan() }}</td>
            <td>{{ link_to("jadwal/edit/"~jadwal.getIdJadwal(), "Edit") }}</td>
            <td>{{ link_to("jadwal/delete/"~jadwal.getIdJadwal(), "Delete") }}</td>
        </tr>
    {% endfor %}
    {% endif %}
    </tbody>
    <tbody>
        <tr>
            <td colspan="2" align="right">
                <table align="center">
                    <tr>
                        <td>{{ link_to("jadwal/search", "First") }}</td>
                        <td>{{ link_to("jadwal/search?page="~page.before, "Previous") }}</td>
                        <td>{{ link_to("jadwal/search?page="~page.next, "Next") }}</td>
                        <td>{{ link_to("jadwal/search?page="~page.last, "Last") }}</td>
                        <td>{{ page.current~"/"~page.total_pages }}</td>
                    </tr>
                </table>
            </td>
        </tr>
    </tbody>
</table>
