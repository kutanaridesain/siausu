{#
/**
 * @author Fery Putra Tarigan <kutanaridesain@gmail.com>
 * @link http://kutanari.com, http://kutanaridesain.com
 * @since 2015
 * @license MIT License
 */
#}
{%- macro table_list(jkul, hari, namaHari) %}
<div class="box-body table-responsive">
    <label for="" class="pull-left">{{namaHari}}</label>
    <table class="table table-bordered table-hover">
        <colgroup>
            <col width="2%"></col>
            <col width="20%"></col>
            <col width="20%"></col>
            <col width="20%"></col>
            <col width="20%"></col>
            <col width="10%"></col>
        </colgroup>
        <thead>
            <tr>
                <th>No.</th>
                <th>Mata Kuliah</th>
                <th>Nama Dosen Pengampu</th>
                <th>Waktu</th>
                <th>Ruangan</th>
                <th>Proses</th>
            </tr>
        </thead>
        <tbody>
            {% set counter = 0 %}
            {% for jadwal in jkul %}
                {% if jadwal.hari is hari %}
                    {% set counter += 1 %}
                    <tr>
                        <td>{{ counter }}</td>
                        <td>{{ jadwal.kode_matakuliah }} - {{ jadwal.nama_matakuliah }}</td>
                        <td>{{ jadwal.nama_dosen }}</td>
                        <td>{{ jadwal.mulai }} <span class="small">s/d</span> {{ jadwal.selesai }}</td>
                        <td>{{ jadwal.nama_ruangan }}</td>
                        <td align="center">
                            {{ link_to("jadwal/edit/"~jadwal.id_jadwal, '<i class="fa fa-fw fa-edit"></i>', 'data-toggle':"tooltip", 'title':'Edit', 'class':'btn btn-xs btn-primary') }} 

                            {{ link_to("jadwal/delete/"~jadwal.id_jadwal, '<i class="fa fa-fw fa-minus-circle"></i>', 'data-toggle':"tooltip", 'title':'Delete', 'class':'btn btn-xs btn-danger', "onclick":"javascript:if(confirm('Anda yakin menghapus item ini?')==false)return false;") }}
                        </td>
                    </tr>
                {% endif %}
            {% endfor %}
        </tbody>
        <tfoot>
            <tr>
                <td colspan="3" align="center">
                </td>
            </tr>
        </tfoot>
    </table>
</div>
{%- endmacro %}

<div class="row">
    <div class="col-xs-12">
        <div class="box box-success">
            <div class="box-header">
                <h3 class="box-title">Daftar Jadwal</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
            {{ content() }}
            <div class="box-body clearfix">
                {{link_to("jadwal/new", "Tambah", "class":"btn btn-success btn-xs pull-right")}}
            </div>
            {% set curntHari = null %}
            {% for jd in jk %}

                {% if jd.hari is curntHari %}
                    {% continue %}
                {% else %}
                    {% set curntHari = jd.hari %}
                    {{ table_list("jkul":jk, "hari":curntHari, "namaHari":arrHari[jd.hari]) }}
                {% endif %}
            {% endfor %}
            </div>
        </div>
    </div>
</div>

