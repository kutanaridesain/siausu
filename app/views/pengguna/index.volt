{#
/**
 * @author Fery Putra Tarigan <kutanaridesain@gmail.com>
 * @link http://kutanari.com, http://kutanaridesain.com
 * @since 2015
 * @license MIT License
 */
#}

{{ content() }}

<div align="right">
    {{ link_to("pengguna/new", "Create pengguna") }}
</div>

{{ form("pengguna/search", "method":"post", "autocomplete" : "off") }}

<div align="center">
    <h1>Search pengguna</h1>
</div>

<table>
    <tr>
        <td align="right">
            <label for="username">Username</label>
        </td>
        <td align="left">
            {{ text_field("username", "size" : 30) }}
        </td>
    </tr>
    <tr>
        <td align="right">
            <label for="email">Email</label>
        </td>
        <td align="left">
            {{ text_field("email", "size" : 30) }}
        </td>
    </tr>
    <tr>
        <td align="right">
            <label for="password">Password</label>
        </td>
        <td align="left">
            {{ text_field("password", "size" : 30) }}
        </td>
    </tr>
    <tr>
        <td align="right">
            <label for="create_time">Create Of Time</label>
        </td>
        <td align="left">
            {{ text_field("create_time", "size" : 30) }}
        </td>
    </tr>
    <tr>
        <td align="right">
            <label for="id_pengguna">Id Of Pengguna</label>
        </td>
        <td align="left">
            {{ text_field("id_pengguna", "type" : "numeric") }}
        </td>
    </tr>
    <tr>
        <td align="right">
            <label for="refrensi">Refrensi</label>
        </td>
        <td align="left">
            {{ text_field("refrensi", "size" : 30) }}
        </td>
    </tr>

    <tr>
        <td></td>
        <td>{{ submit_button("Search") }}</td>
    </tr>
</table>

</form>
