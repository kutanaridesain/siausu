{#
/**
 * @author Fery Putra Tarigan <kutanaridesain@gmail.com>
 * @link http://kutanari.com, http://kutanaridesain.com
 * @since 2015
 * @license MIT License
 */
#}

{{ content() }}

<table width="100%">
    <tr>
        <td align="left">
            {{ link_to("pengguna/index", "Go Back") }}
        </td>
        <td align="right">
            {{ link_to("pengguna/new", "Create ") }}
        </td>
    </tr>
</table>

<table class="browse" align="center">
    <thead>
        <tr>
            <th>Username</th>
            <th>Email</th>
            <th>Password</th>
            <th>Create Of Time</th>
            <th>Id Of Pengguna</th>
            <th>Refrensi</th>
         </tr>
    </thead>
    <tbody>
    {% if page.items is defined %}
    {% for pengguna in page.items %}
        <tr>
            <td>{{ pengguna.getUsername() }}</td>
            <td>{{ pengguna.getEmail() }}</td>
            <td>{{ pengguna.getPassword() }}</td>
            <td>{{ pengguna.getCreateTime() }}</td>
            <td>{{ pengguna.getIdPengguna() }}</td>
            <td>{{ pengguna.getRefrensi() }}</td>
            <td>{{ link_to("pengguna/edit/"~pengguna.getUsername(), "Edit") }}</td>
            <td>{{ link_to("pengguna/delete/"~pengguna.getUsername(), "Delete") }}</td>
        </tr>
    {% endfor %}
    {% endif %}
    </tbody>
    <tbody>
        <tr>
            <td colspan="2" align="right">
                <table align="center">
                    <tr>
                        <td>{{ link_to("pengguna/search", "First") }}</td>
                        <td>{{ link_to("pengguna/search?page="~page.before, "Previous") }}</td>
                        <td>{{ link_to("pengguna/search?page="~page.next, "Next") }}</td>
                        <td>{{ link_to("pengguna/search?page="~page.last, "Last") }}</td>
                        <td>{{ page.current~"/"~page.total_pages }}</td>
                    </tr>
                </table>
            </td>
        </tr>
    </tbody>
</table>
