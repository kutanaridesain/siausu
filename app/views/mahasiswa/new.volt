{#
/**
 * @author Fery Putra Tarigan <kutanaridesain@gmail.com>
 * @link http://kutanari.com, http://kutanaridesain.com
 * @since 2015
 * @license MIT License
 */
#}
{{ form("mahasiswa/create", "method":"post", "autocomplete" : "off", "role":"form") }}
{% if content() != "" %}
<div class="alert alert-danger alert-dismissable">
    <i class="fa fa-ban"></i>
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
{{ content() }}
</div>
{% endif %}
    <div class="row">
        <div class="col-md-6">
            <div class="box box-success">
                <div class="box-header">
                    <h3 class="box-title">Tambah Mahasiswa</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group">
                        <label for="nim">NIM</label>
                        {{ text_field("nim", "size" : 30, "class":"form-control", "placeholder":"Masukkan NIM") }}
                    </div>
                    <div class="form-group">
                        <label for="nama">Nama Mahasiswa</label>
                        {{ text_field("nama", "size" : 30, "class":"form-control", "placeholder":"Masukkan Nama Mahasiswa") }}
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        {{ text_field("password", "size" : 30, "class":"form-control", "placeholder":"Password") }}
                    </div>
                    <div class="form-group">
                        <label for="tempat_lahir">Tempat Lahir</label>
                        {{ text_field("tempat_lahir", "size" : 30, "class":"form-control", "placeholder":"Masukkan Tempat Lahir") }}
                    </div>
                    <div class="form-group">
                        <label for="tanggal_lahir">Tanggal Lahir</label>
                        {{ text_field("tanggal_lahir", "size" : 30, "class":"form-control", "placeholder":"Masukkan Tanggal Lahir") }}
                    </div>
                    <div class="row form-group">
                        <div class="col-xs-3">
                            <label for="jenis_kelamin">Jenis Kelamin</label>
                            {{ select_static("jenis_kelamin", ["Laki-laki":"Laki-laki", "Perempuan":"Perempuan"], "class":"form-control") }}
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-xs-4">
                            <label for="agama_id_agama">Agama</label>
                            {{ select("agama_id_agama", agama, "using":["id_agama", "nama_agama"], "useEmpty":true, "emptyText":"Pilih agama", "emptyValue":"", "class":"form-control") }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="alamat">Alamat</label>
                        {{ text_field("alamat", "size" : 30, "class":"form-control", "placeholder":"Masukkan Alamat") }}
                    </div>
                    <div class="form-group">
                        <label for="kota">Kota</label>
                        {{ text_field("kota", "size" : 30, "class":"form-control", "placeholder":"Masukkan kota") }}
                    </div>
                    <div class="form-group">
                        <label for="kodepos">Kodepos</label>
                        {{ text_field("kodepos", "size" : 30, "class":"form-control", "placeholder":"Masukkan kodepos") }}
                    </div>
                    <div class="form-group">
                        <label for="no_telp">No. Telp</label>
                        {{ text_field("no_telp", "size" : 30, "class":"form-control", "placeholder":"Masukkan No. Telp") }}
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        {{ text_field("email", "size" : 30, "class":"form-control", "placeholder":"Masukkan email") }}
                    </div>
                    <div class="row form-group">
                        <div class="col-xs-3">
                            <label for="angkatan">Angkatan</label>
                            {{ select_static("angkatan", tahun, "class":"form-control") }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="asal_sekolah">Asal Sekolah</label>
                        {{ text_field("asal_sekolah", "size" : 30, "class":"form-control", "placeholder":"Masukkan Asal Sekolah") }}
                    </div>
                    <div class="row form-group">
                        <div class="col-xs-3">
                            <label for="tahun_lulus">Tahun Lulus</label>
                            {{ select_static("tahun_lulus", tahun, "class":"form-control") }}
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-xs-4">
                            <label for="konsentrasi_id_konsentrasi">Konsentrasi</label>

                            {{ select("konsentrasi_id_konsentrasi", konsentrasi, "using":["id_konsentrasi", "nama_konsentrasi"], "useEmpty":true, "emptyText":"Pilih konsentrasi", "emptyValue":"", "class":"form-control") }}
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-xs-4">
                            <label for="status_id_status">Status</label>
                            {{ select("status_id_status", status, "using":["id_status", "nama_status"], "useEmpty":true, "emptyText":"Pilih status", "emptyValue":"", "class":"form-control") }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="jenis_kuliah">Jenis Kuliah</label>
                        <div class="radio">
                            <label>
                                {{ radio_field("jenis_kuliah", "value":"Reguler", "id":"reguler", "class":"form-control", "checked":"checked") }}
                                Reguler
                            </label>
                            <label>
                                {{ radio_field("jenis_kuliah", "value":"Ekstensi", "id":"ekstensi", "class":"form-control") }}
                                Ekstensi
                            </label>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="col-md-6">
            <div class="box box-success">
                <div class="box-header">
                    <h3 class="box-title">Info orangtua</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group">
                        <label for="nama_orangtua">Nama</label>
                        {{ text_field("nama_orangtua", "size" : 30, "class":"form-control", "placeholder":"Nama orangtua") }}
                    </div>
                    <div class="form-group">
                        <label for="pekerjaan_orangtua">Pekerjaan</label>
                        {{ text_field("pekerjaan_orangtua", "size" : 30, "class":"form-control", "placeholder":"Masukkan pekerjaan orangtua") }}
                    </div>
                    <div class="form-group">
                        <label for="alamat_orangtua">Alamat</label>
                        {{ text_field("alamat_orangtua", "size" : 30, "class":"form-control", "placeholder":"Masukkan Alamat") }}
                    </div>
                    <div class="form-group">
                        <label for="kota_orangtua">Kota</label>
                        {{ text_field("kota_orangtua", "size" : 30, "class":"form-control", "placeholder":"Masukkan Kota") }}
                    </div>
                    <div class="form-group">
                        <label for="kodepos_orangtua">Kodepos</label>
                        {{ text_field("kodepos_orangtua", "size" : 30, "class":"form-control", "placeholder":"Masukkan Kodepos") }}
                    </div>
                    <div class="form-group">
                        <label for="no_telp_orangtua">No. Telepon</label>
                        {{ text_field("no_telp_orangtua", "size" : 30, "class":"form-control", "placeholder":"Masukkan nomer telepon") }}
                    </div>

                </div>
            </div>
            <div class="box-footer clearfix">
                    {{link_to("mahasiswa", "Batal", "class":"btn btn-default pull-left")}}
                    {{ submit_button("Simpan", "class":"btn btn-success pull-right") }}
                </div>
        </div>
    </div>
</form>