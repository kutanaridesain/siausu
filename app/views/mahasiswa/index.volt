{#
/**
 * @author Fery Putra Tarigan <kutanaridesain@gmail.com>
 * @link http://kutanari.com, http://kutanaridesain.com
 * @since 2015
 * @license MIT License
 */
#}

<div ng-controller="MahasiswaController as mahasiswa">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-success">
                <div class="box-header">
                    <h3 class="box-title">Daftar Mahasiswa</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                {{ content() }}
                <div class="box-body clearfix">
                    {{link_to("mahasiswa/new", "Tambah", "class":"btn btn-success btn-xs pull-right")}}
                </div>
                    <div class="box-body table-responsive">
                        <table class="table table-bordered table-hover">
                            <colgroup>
                                <col width="2%"></col>
                                <col width="10%"></col>
                                <col width="38%"></col>
                                <col width="10%"></col>
                                <col width="25%"></col>
                                <col width="15%"></col>
                            </colgroup>
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>NIM</th>
                                    <th>Nama</th>
                                    <th>Angkatan</th>
                                    <th>Jurusan</th>
                                    <th>Proses</th>
                                </tr>
                            </thead>
                            <tbody>
                                {% if modelMahasiswa is defined %}
                                {% for mahasiswa in modelMahasiswa %}
                                    {% set counter += 1 %}
                                    <tr>
                                        <td>{{ counter }}</td>
                                        <td>{{ mahasiswa.getNim() }}</td>
                                        <td>{{ mahasiswa.getNama() }}</td>
                                        <td>{{ mahasiswa.getAngkatan() }}</td>
                                        <td>{{ mahasiswa.konsentrasi.getNamaKonsentrasi() }}</td>
                                        <td align="center">
                                        {{ link_to("mahasiswa/detail/"~mahasiswa.getNim(), '<i class="fa fa-fw fa-list-alt"></i>', 'data-toggle':"modal", 'data-target':'#modalDetail', 'title':'Detail', 'class':'btn btn-xs btn-warning', 'tooltip':"") }} 

                                        {{ link_to("mahasiswa/edit/"~mahasiswa.getNim(), '<i class="fa fa-fw fa-edit"></i>', 'data-toggle':"tooltip", 'title':'Edit', 'class':'btn btn-xs btn-primary') }} 

                                        {{ link_to("mahasiswa/delete/"~mahasiswa.getNim(), '<i class="fa fa-fw fa-minus-circle"></i>', 'data-toggle':"tooltip", 'title':'Delete', 'class':'btn btn-xs btn-danger', "onclick":"javascript:if(confirm('Anda yakin menghapus item ini?')==false)return false;") }}
                                        </td>
                                    </tr>
                                {% endfor %}
                                {% endif %}
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="6" align="center">
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>

    <!-- modal detail -->
    <div class="modal fade" id="modalDetail" tabindex="-1" role="dialog" aria-labelledby="modalDetail" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button> 
                <h4 class="modal-title">Detail Mahasiswa</h4>
              </div>
              <div class="modal-body">
                <div class="box-body">

                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
              </div> 
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</div>