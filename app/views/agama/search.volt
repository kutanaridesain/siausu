{#
/**
 * @author Fery Putra Tarigan <kutanaridesain@gmail.com>
 * @link http://kutanari.com, http://kutanaridesain.com
 * @since 2015
 * @license MIT License
 */
#}

{{ content() }}

<table width="100%">
    <tr>
        <td align="left">
            {{ link_to("agama/index", "Go Back") }}
        </td>
        <td align="right">
            {{ link_to("agama/new", "Create ") }}
        </td>
    </tr>
</table>

<table class="browse" align="center">
    <thead>
        <tr>
            <th>Id Of Agama</th>
            <th>Nama Of Agama</th>
         </tr>
    </thead>
    <tbody>
    {% if page.items is defined %}
    {% for agama in page.items %}
        <tr>
            <td>{{ agama.getIdAgama() }}</td>
            <td>{{ agama.getNamaAgama() }}</td>
            <td>{{ link_to("agama/edit/"~agama.getIdAgama(), "Edit") }}</td>
            <td>{{ link_to("agama/delete/"~agama.getIdAgama(), "Delete") }}</td>
        </tr>
    {% endfor %}
    {% endif %}
    </tbody>
    <tbody>
        <tr>
            <td colspan="2" align="right">
                <table align="center">
                    <tr>
                        <td>{{ link_to("agama/search", "First") }}</td>
                        <td>{{ link_to("agama/search?page="~page.before, "Previous") }}</td>
                        <td>{{ link_to("agama/search?page="~page.next, "Next") }}</td>
                        <td>{{ link_to("agama/search?page="~page.last, "Last") }}</td>
                        <td>{{ page.current~"/"~page.total_pages }}</td>
                    </tr>
                </table>
            </td>
        </tr>
    </tbody>
</table>
