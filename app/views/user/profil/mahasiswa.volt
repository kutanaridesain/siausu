{#
/**
 * @author Fery Putra Tarigan <kutanaridesain@gmail.com>
 * @link http://kutanari.com, http://kutanaridesain.com
 * @since 2015
 * @license MIT License
 */
#}
{{ content() }}
<div class="row">
    <div class="col-md-12">
        <div class="box box-warning ">
            <div class="box-header">
                <h3 class="box-title">Info mahasiswa</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <div class="clearfix">
                    <table class="detail-info pull-left">
                        <colgroup>
                            <col width="50%"></col>
                            <col width="50%"></col>
                        </colgroup>
                        <tr>
                            <th>Nama </th>
                            <td><strong>:</strong> {{mahasiswa.getNama()}}</td>
                        </tr>
                        <tr>
                            <th>NIM </th>
                            <td><strong>:</strong> {{mahasiswa.getNim()}}</td>
                        </tr>
                        <tr>
                            <th>Tempat, Tanggal Lahir </th>
                            <td><strong>:</strong> {{mahasiswa.getTempatLahir()}}, {{mahasiswa.getTanggalLahir()}}</td>
                        </tr>
                        <tr>
                            <th>Jenis Kelamin </th>
                            <td><strong>:</strong> {{mahasiswa.getJenisKelamin()}}</td>
                        </tr>
                        <tr>
                            <th>Agama </th>
                            <td><strong>:</strong> {{mahasiswa.agama.getNamaAgama()}}</td>
                        </tr>
                        <tr>
                            <th>Konsentrasi </th>
                            <td><strong>:</strong> {{mahasiswa.konsentrasi.getNamaKonsentrasi()}}</td>
                        </tr>
                        <tr>
                            <th>Angkatan </th>
                            <td><strong>:</strong> {{mahasiswa.getAngkatan()}}</td>
                        </tr>
                        <tr>
                            <th>Alamat </th>
                            <td><strong>:</strong> {{mahasiswa.getAlamat()}}</td>
                        </tr>
                        <tr>
                            <th>Kota </th>
                            <td><strong>:</strong> {{mahasiswa.getKota()}}</td>
                        </tr>
                        <tr>
                            <th>Kodepos </th>
                            <td><strong>:</strong> {{mahasiswa.getKodepos()}}</td>
                        </tr>
                        <tr>
                            <th>Email </th>
                            <td><strong>:</strong> {{mahasiswa.getEmail()}}</td>
                        </tr>
                        <tr>
                            <th>Telepon </th>
                            <td><strong>:</strong> {{mahasiswa.getNoTelp()}}</td>
                        </tr>
                        <tr>
                            <th>Asal sekolah </th>
                            <td><strong>:</strong> {{mahasiswa.getAsalSekolah()}}</td>
                        </tr>
                        <tr>
                            <th>Tahun lulus </th>
                            <td><strong>:</strong> {{mahasiswa.getTahunLulus()}}</td>
                        </tr>
                        <tr>
                            <th>Jenis kuliah </th>
                            <td><strong>:</strong> {{mahasiswa.getJenisKuliah()}}</td>
                        </tr>
                        <tr>
                            <th>Status </th>
                            <td><strong>:</strong> {{mahasiswa.status.getNamaStatus()}}</td>
                        </tr>
                    </table>    
                </div>
            </div>
        </div>
        <div class="box box-warning ">
            <div class="box-header">
                <h3 class="box-title">Info Orangtua</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <div class="clearfix">
                    <table class="detail-info pull-left">
                        <colgroup>
                            <col width="50%"></col>
                            <col width="50%"></col>
                        </colgroup>
                        <tr>
                            <th>Nama </th>
                            <td><strong>:</strong> {{mahasiswa.getNamaOrangtua()}}</td>
                        </tr>
                        <tr>
                            <th>Pekerjaan </th>
                            <td><strong>:</strong> {{mahasiswa.getPekerjaanOrangtua()}}</td>
                        </tr>
                        <tr>
                            <th>Alamat </th>
                            <td><strong>:</strong> {{mahasiswa.getAlamatOrangtua()}}</td>
                        </tr>
                        <tr>
                            <th>Kota </th>
                            <td><strong>:</strong> {{mahasiswa.getKotaOrangtua()}}</td>
                        </tr>
                        <tr>
                            <th>Kodepos </th>
                            <td><strong>:</strong> {{mahasiswa.getKodeposOrangtua()}}</td>
                        </tr>
                        <tr>
                            <th>No. Telepon </th>
                            <td><strong>:</strong> {{mahasiswa.getNoTelpOrangtua()}}</td>
                        </tr>
                    </table>    
                </div>
            </div>
        </div>
    </div>
</div>
