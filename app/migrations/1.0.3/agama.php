<?php 

use Phalcon\Db\Column;
use Phalcon\Db\Index;
use Phalcon\Db\Reference;
use Phalcon\Mvc\Model\Migration;

class AgamaMigration_103 extends Migration
{

    public function up()
    {
        $this->morphTable(
            'agama',
            array(
            'columns' => array(
                new Column(
                    'id_agama',
                    array(
                        'type' => Column::TYPE_INTEGER,
                        'notNull' => true,
                        'autoIncrement' => true,
                        'size' => 11,
                        'first' => true
                    )
                ),
                new Column(
                    'nama_agama',
                    array(
                        'type' => Column::TYPE_VARCHAR,
                        'notNull' => true,
                        'size' => 45,
                        'after' => 'id_agama'
                    )
                )
            ),
            'indexes' => array(
                new Index('PRIMARY', array('id_agama')),
                new Index('id_agama_UNIQUE', array('id_agama')),
                new Index('nama_agama_UNIQUE', array('nama_agama'))
            ),
            'options' => array(
                'TABLE_TYPE' => 'BASE TABLE',
                'AUTO_INCREMENT' => '1',
                'ENGINE' => 'InnoDB',
                'TABLE_COLLATION' => 'latin1_swedish_ci'
            )
        )
        );
    }
}
