<?php 

use Phalcon\Db\Column;
use Phalcon\Db\Index;
use Phalcon\Db\Reference;
use Phalcon\Mvc\Model\Migration;

class MahasiswaMigration_100 extends Migration
{
    public function up()
    {
        $this->morphTable(
            'mahasiswa',
            array(
            'columns' => array(
                new Column(
                    'nim',
                    array(
                        'type' => Column::TYPE_VARCHAR,
                        'notNull' => true,
                        'size' => 9,
                        'first' => true
                    )
                ),
                new Column(
                    'nama',
                    array(
                        'type' => Column::TYPE_VARCHAR,
                        'notNull' => true,
                        'size' => 128,
                        'after' => 'nim'
                    )
                ),
                new Column(
                    'angkatan',
                    array(
                        'type' => Column::TYPE_VARCHAR,
                        'notNull' => true,
                        'size' => 4,
                        'after' => 'nama'
                    )
                ),
                new Column(
                    'foto',
                    array(
                        'type' => Column::TYPE_VARCHAR,
                        'size' => 256,
                        'after' => 'angkatan'
                    )
                ),
                new Column(
                    'alamat',
                    array(
                        'type' => Column::TYPE_VARCHAR,
                        'notNull' => true,
                        'size' => 45,
                        'after' => 'foto'
                    )
                ),
                new Column(
                    'tempat_lahir',
                    array(
                        'type' => Column::TYPE_VARCHAR,
                        'notNull' => true,
                        'size' => 128,
                        'after' => 'alamat'
                    )
                ),
                new Column(
                    'tanggal_lahir',
                    array(
                        'type' => Column::TYPE_VARCHAR,
                        'notNull' => true,
                        'size' => 15,
                        'after' => 'tempat_lahir'
                    )
                ),
                new Column(
                    'jenis_kelamin',
                    array(
                        'type' => Column::TYPE_CHAR,
                        'notNull' => true,
                        'size' => 1,
                        'after' => 'tanggal_lahir'
                    )
                ),
                new Column(
                    'no_telp',
                    array(
                        'type' => Column::TYPE_VARCHAR,
                        'size' => 15,
                        'after' => 'jenis_kelamin'
                    )
                ),
                new Column(
                    'asal_sekolah',
                    array(
                        'type' => Column::TYPE_VARCHAR,
                        'notNull' => true,
                        'size' => 128,
                        'after' => 'no_telp'
                    )
                ),
                new Column(
                    'tahun_lulus',
                    array(
                        'type' => Column::TYPE_VARCHAR,
                        'notNull' => true,
                        'size' => 4,
                        'after' => 'asal_sekolah'
                    )
                ),
                new Column(
                    'agama_id_agama',
                    array(
                        'type' => Column::TYPE_INTEGER,
                        'notNull' => true,
                        'size' => 11,
                        'after' => 'tahun_lulus'
                    )
                ),
                new Column(
                    'jurusan_id_jurusan',
                    array(
                        'type' => Column::TYPE_INTEGER,
                        'notNull' => true,
                        'size' => 11,
                        'after' => 'agama_id_agama'
                    )
                ),
                new Column(
                    'status_id_status',
                    array(
                        'type' => Column::TYPE_INTEGER,
                        'notNull' => true,
                        'size' => 11,
                        'after' => 'jurusan_id_jurusan'
                    )
                ),
                new Column(
                    'jenis_kuliah',
                    array(
                        'type' => Column::TYPE_CHAR,
                        'notNull' => true,
                        'size' => 1,
                        'after' => 'status_id_status'
                    )
                )
            ),
            'indexes' => array(
                new Index('PRIMARY', array('nim')),
                new Index('nim_UNIQUE', array('nim')),
                new Index('fk_mahasiswa_agama_idx', array('agama_id_agama')),
                new Index('fk_mahasiswa_jurusan1_idx', array('jurusan_id_jurusan')),
                new Index('fk_mahasiswa_status1_idx', array('status_id_status'))
            ),
            'references' => array(
                new Reference('fk_mahasiswa_agama', array(
                    'referencedSchema' => 'siausu',
                    'referencedTable' => 'agama',
                    'columns' => array('agama_id_agama'),
                    'referencedColumns' => array('id_agama')
                )),
                new Reference('fk_mahasiswa_jurusan1', array(
                    'referencedSchema' => 'siausu',
                    'referencedTable' => 'jurusan',
                    'columns' => array('jurusan_id_jurusan'),
                    'referencedColumns' => array('id_jurusan')
                )),
                new Reference('fk_mahasiswa_status1', array(
                    'referencedSchema' => 'siausu',
                    'referencedTable' => 'status',
                    'columns' => array('status_id_status'),
                    'referencedColumns' => array('id_status')
                ))
            ),
            'options' => array(
                'TABLE_TYPE' => 'BASE TABLE',
                'AUTO_INCREMENT' => '',
                'ENGINE' => 'InnoDB',
                'TABLE_COLLATION' => 'latin1_swedish_ci'
            )
        )
        );
    }
}
