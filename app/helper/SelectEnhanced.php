<?php
/**
 * @author Fery Putra Tarigan <kutanaridesain@gmail.com>
 * @link http://kutanari.com, http://kutanaridesain.com
 * @since 2015
 * @license MIT License
 */


use Phalcon\Forms\Element;

class SelectEnhanced extends Element {

    protected $optionsElement = array();

    public function __construct($elementName, $elementValues = NULL, $parameters = NULL) {

        $this->setName($elementName);

        if($parameters['useEmpty']) {

            if(!isset($parameters['emptyText'])) {

                $parameters['emptyText'] = 'Choose...';
            }

            $this->addOption(array('value' => '', 'content' => $parameters['emptyText']));
        }


        if(!empty($parameters['options'])){
            $haveOptions = true;
        }
        else {
            $haveOptions = false;
        }


        // If we got data to generate optionElements
        if(!empty($elementValues)) {



            foreach($elementValues as $elementValue) {

                // Initialize optionElement parameters
                $optionParameters   =   array('value' => $elementValue->getId(), 'content' => $elementValue->getName());

                // If somes options are defined to set attributes on optionElements
                if($haveOptions) {

                    foreach($parameters['options'] as $attribute => $condition) {

                        // If condition is string assign it to result
                        if(is_string($condition)) {
                            $result =   $condition;
                        }
                        // Otherwise execute condition to get result
                        else {
                            $result    =   $condition($elementValue);
                        }

                        // if condition is passed we had attribute with his value
                        if($result) {

                            $optionParameters[$attribute] = $result;
                        }
                    }
                }

                // add optionElement with parameters
                 $this->addOption($optionParameters);
            }
        }


    }

    public function render($attributes = null) {

        // Set new attributes passed in parameter
        $attributes = $this->prepareAttributes($attributes);
        if(!empty($attributes)) {
            foreach($attributes as $attrName => $attrValue) {

                $this->setAttribute($attrName, $attrValue);
            }
        }


        $html = '<select name="'.$this->getName().'"';

        // write every attributes of select element in DOM
        $attributes = $this->getAttributes();
        if(!empty($attributes)) {
            foreach($attributes as $attrName => $attrValue) {
                $html .= ' '.$attrName.'="'.$attrValue.'"';
            }
        }

        $html .= '>';

        if(!empty($this->optionsElement)) {

            foreach($this->optionsElement as $optionElement) {


                // extract value an content from optionElement
                $value  =  $optionElement['value'];
                unset($optionElement['value']);
                $content = $optionElement['content'];
                unset($optionElement['content']);

                $html .= '<option value="'.$value.'"';

                // write every attributes of optionElement in DOM
                if(!empty($optionElement)) {

                    foreach($optionElement as $attrName => $attrValue) {
                        $html .= ' '.$attrName.'="'.$attrValue.'"';
                    }
                }

                $html .= '>';

                $html .= $content.'</option>'; 
            }
        }

        $html .=  '</select>';

        return $html;

    }


    public function addOption($parameters) {

        array_push($this->optionsElement, $parameters);
    }

}