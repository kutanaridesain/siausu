<?php
/**
 * @author Fery Putra Tarigan <kutanaridesain@gmail.com>
 * @link http://kutanari.com, http://kutanaridesain.com
 * @since 2015
 * @license MIT License
 */


class Status extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $id_status;

    /**
     *
     * @var string
     */
    protected $nama_status;

    /**
     * Method to set the value of field id_status
     *
     * @param integer $id_status
     * @return $this
     */
    public function setIdStatus($id_status)
    {
        $this->id_status = $id_status;

        return $this;
    }

    /**
     * Method to set the value of field nama_status
     *
     * @param string $nama_status
     * @return $this
     */
    public function setNamaStatus($nama_status)
    {
        $this->nama_status = $nama_status;

        return $this;
    }

    /**
     * Returns the value of field id_status
     *
     * @return integer
     */
    public function getIdStatus()
    {
        return $this->id_status;
    }

    /**
     * Returns the value of field nama_status
     *
     * @return string
     */
    public function getNamaStatus()
    {
        return $this->nama_status;
    }

}
