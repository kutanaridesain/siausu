<?php
/**
 * @author Fery Putra Tarigan <kutanaridesain@gmail.com>
 * @link http://kutanari.com, http://kutanaridesain.com
 * @since 2015
 * @license MIT License
 */


class Agama extends CustomModel
{

    /**
     *
     * @var integer
     */
    protected $id_agama;

    /**
     *
     * @var string
     */
    protected $nama_agama;

    /**
     * Method to set the value of field id_agama
     *
     * @param integer $id_agama
     * @return $this
     */
    public function setIdAgama($id_agama)
    {
        $this->id_agama = $id_agama;

        return $this;
    }

    /**
     * Method to set the value of field nama_agama
     *
     * @param string $nama_agama
     * @return $this
     */
    public function setNamaAgama($nama_agama)
    {
        $this->nama_agama = $nama_agama;

        return $this;
    }

    /**
     * Returns the value of field id_agama
     *
     * @return integer
     */
    public function getIdAgama()
    {
        return $this->id_agama;
    }

    /**
     * Returns the value of field nama_agama
     *
     * @return string
     */
    public function getNamaAgama()
    {
        return $this->nama_agama;
    }
}
