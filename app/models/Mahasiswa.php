<?php
/**
 * @author Fery Putra Tarigan <kutanaridesain@gmail.com>
 * @link http://kutanari.com, http://kutanaridesain.com
 * @since 2015
 * @license MIT License
 */


use Phalcon\Mvc\Model\Validator\Email as Email;

class Mahasiswa extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var string
     */
    protected $nim;

    /**
     *
     * @var string
     */
    protected $nama;

    /**
     *
     * @var string
     */
    protected $angkatan;

    /**
     *
     * @var string
     */
    protected $foto;

    /**
     *
     * @var string
     */
    protected $alamat;

    /**
     *
     * @var string
     */
    protected $kota;

    /**
     *
     * @var string
     */
    protected $kodepos;

    /**
     *
     * @var string
     */
    protected $tempat_lahir;

    /**
     *
     * @var string
     */
    protected $tanggal_lahir;

    /**
     *
     * @var string
     */
    protected $jenis_kelamin;

    /**
     *
     * @var string
     */
    protected $no_telp;

    /**
     *
     * @var string
     */
    protected $asal_sekolah;

    /**
     *
     * @var string
     */
    protected $tahun_lulus;

    /**
     *
     * @var string
     */
    protected $jenis_kuliah;

    /**
     *
     * @var string
     */
    protected $wali;

    /**
     *
     * @var string
     */
    protected $email;

    /**
     *
     * @var integer
     */
    protected $agama_id_agama;

    /**
     *
     * @var integer
     */
    protected $status_id_status;

    /**
     *
     * @var integer
     */
    protected $konsentrasi_id_konsentrasi;

    /**
     *
     * @var string
     */
    protected $nama_orangtua;

    /**
     *
     * @var string
     */
    protected $pekerjaan_orangtua;

    /**
     *
     * @var string
     */
    protected $alamat_orangtua;

    /**
     *
     * @var string
     */
    protected $kota_orangtua;

    /**
     *
     * @var string
     */
    protected $kodepos_orangtua;

    /**
     *
     * @var string
     */
    protected $no_telp_orangtua;

    /**
     *  
     * @var string
     */
    protected $password;

    /**
     * Method to set the value of field nim
     *
     * @param string $nim
     * @return $this
     */
    public function setNim($nim)
    {
        $this->nim = $nim;

        return $this;
    }

    /**
     * Method to set the value of field nama
     *
     * @param string $nama
     * @return $this
     */
    public function setNama($nama)
    {
        $this->nama = $nama;

        return $this;
    }

    /**
     * Method to set the value of field angkatan
     *
     * @param string $angkatan
     * @return $this
     */
    public function setAngkatan($angkatan)
    {
        $this->angkatan = $angkatan;

        return $this;
    }

    /**
     * Method to set the value of field foto
     *
     * @param string $foto
     * @return $this
     */
    public function setFoto($foto)
    {
        $this->foto = $foto;

        return $this;
    }

    /**
     * Method to set the value of field alamat
     *
     * @param string $alamat
     * @return $this
     */
    public function setAlamat($alamat)
    {
        $this->alamat = $alamat;

        return $this;
    }

    /**
     * Method to set the value of field kota
     *
     * @param string $kota
     * @return $this
     */
    public function setKota($kota)
    {
        $this->kota = $kota;

        return $this;
    }

    /**
     * Method to set the value of field kodepos
     *
     * @param string $kodepos
     * @return $this
     */
    public function setKodepos($kodepos)
    {
        $this->kodepos = $kodepos;

        return $this;
    }

    /**
     * Method to set the value of field tempat_lahir
     *
     * @param string $tempat_lahir
     * @return $this
     */
    public function setTempatLahir($tempat_lahir)
    {
        $this->tempat_lahir = $tempat_lahir;

        return $this;
    }

    /**
     * Method to set the value of field tanggal_lahir
     *
     * @param string $tanggal_lahir
     * @return $this
     */
    public function setTanggalLahir($tanggal_lahir)
    {
        $this->tanggal_lahir = $tanggal_lahir;

        return $this;
    }

    /**
     * Method to set the value of field jenis_kelamin
     *
     * @param string $jenis_kelamin
     * @return $this
     */
    public function setJenisKelamin($jenis_kelamin)
    {
        $this->jenis_kelamin = $jenis_kelamin;

        return $this;
    }

    /**
     * Method to set the value of field no_telp
     *
     * @param string $no_telp
     * @return $this
     */
    public function setNoTelp($no_telp)
    {
        $this->no_telp = $no_telp;

        return $this;
    }

    /**
     * Method to set the value of field asal_sekolah
     *
     * @param string $asal_sekolah
     * @return $this
     */
    public function setAsalSekolah($asal_sekolah)
    {
        $this->asal_sekolah = $asal_sekolah;

        return $this;
    }

    /**
     * Method to set the value of field tahun_lulus
     *
     * @param string $tahun_lulus
     * @return $this
     */
    public function setTahunLulus($tahun_lulus)
    {
        $this->tahun_lulus = $tahun_lulus;

        return $this;
    }

    /**
     * Method to set the value of field jenis_kuliah
     *
     * @param string $jenis_kuliah
     * @return $this
     */
    public function setJenisKuliah($jenis_kuliah)
    {
        $this->jenis_kuliah = $jenis_kuliah;

        return $this;
    }

    /**
     * Method to set the value of field wali
     *
     * @param string $wali
     * @return $this
     */
    public function setWali($wali)
    {
        $this->wali = $wali;

        return $this;
    }

    /**
     * Method to set the value of field email
     *
     * @param string $email
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Method to set the value of field agama_id_agama
     *
     * @param integer $agama_id_agama
     * @return $this
     */
    public function setAgamaIdAgama($agama_id_agama)
    {
        $this->agama_id_agama = $agama_id_agama;

        return $this;
    }

    /**
     * Method to set the value of field status_id_status
     *
     * @param integer $status_id_status
     * @return $this
     */
    public function setStatusIdStatus($status_id_status)
    {
        $this->status_id_status = $status_id_status;

        return $this;
    }

    /**
     * Method to set the value of field konsentrasi_id_konsentrasi
     *
     * @param integer $konsentrasi_id_konsentrasi
     * @return $this
     */
    public function setKonsentrasiIdKonsentrasi($konsentrasi_id_konsentrasi)
    {
        $this->konsentrasi_id_konsentrasi = $konsentrasi_id_konsentrasi;

        return $this;
    }

    /**
     * Method to set the value of field nama_orangtua
     *
     * @param string $nama_orangtua
     * @return $this
     */
    public function setNamaOrangtua($nama_orangtua)
    {
        $this->nama_orangtua = $nama_orangtua;

        return $this;
    }

    /**
     * Method to set the value of field pekerjaan_orangtua
     *
     * @param string $pekerjaan_orangtua
     * @return $this
     */
    public function setPekerjaanOrangtua($pekerjaan_orangtua)
    {
        $this->pekerjaan_orangtua = $pekerjaan_orangtua;

        return $this;
    }

    /**
     * Method to set the value of field alamat_orangtua
     *
     * @param string $alamat_orangtua
     * @return $this
     */
    public function setAlamatOrangtua($alamat_orangtua)
    {
        $this->alamat_orangtua = $alamat_orangtua;

        return $this;
    }

    /**
     * Method to set the value of field kota_orangtua
     *
     * @param string $kota_orangtua
     * @return $this
     */
    public function setKotaOrangtua($kota_orangtua)
    {
        $this->kota_orangtua = $kota_orangtua;

        return $this;
    }

    /**
     * Method to set the value of field kodepos_orangtua
     *
     * @param string $kodepos_orangtua
     * @return $this
     */
    public function setKodeposOrangtua($kodepos_orangtua)
    {
        $this->kodepos_orangtua = $kodepos_orangtua;

        return $this;
    }

    /**
     * Method to set the value of field no_telp_orangtua
     *
     * @param string $no_telp_orangtua
     * @return $this
     */
    public function setNoTelpOrangtua($no_telp_orangtua)
    {
        $this->no_telp_orangtua = $no_telp_orangtua;

        return $this;
    }

    /**
     * method to set the value of field password
     * @param string $password 
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * Returns the value of field nim
     *
     * @return string
     */
    public function getNim()
    {
        return $this->nim;
    }

    /**
     * Returns the value of field nama
     *
     * @return string
     */
    public function getNama()
    {
        return $this->nama;
    }

    /**
     * Returns the value of field angkatan
     *
     * @return string
     */
    public function getAngkatan()
    {
        return $this->angkatan;
    }

    /**
     * Returns the value of field foto
     *
     * @return string
     */
    public function getFoto()
    {
        return $this->foto;
    }

    /**
     * Returns the value of field alamat
     *
     * @return string
     */
    public function getAlamat()
    {
        return $this->alamat;
    }

    /**
     * Returns the value of field kota
     *
     * @return string
     */
    public function getKota()
    {
        return $this->kota;
    }

    /**
     * Returns the value of field kodepos
     *
     * @return string
     */
    public function getKodepos()
    {
        return $this->kodepos;
    }

    /**
     * Returns the value of field tempat_lahir
     *
     * @return string
     */
    public function getTempatLahir()
    {
        return $this->tempat_lahir;
    }

    /**
     * Returns the value of field tanggal_lahir
     *
     * @return string
     */
    public function getTanggalLahir()
    {
        return $this->tanggal_lahir;
    }

    /**
     * Returns the value of field jenis_kelamin
     *
     * @return string
     */
    public function getJenisKelamin()
    {
        return $this->jenis_kelamin;
    }

    /**
     * Returns the value of field no_telp
     *
     * @return string
     */
    public function getNoTelp()
    {
        return $this->no_telp;
    }

    /**
     * Returns the value of field asal_sekolah
     *
     * @return string
     */
    public function getAsalSekolah()
    {
        return $this->asal_sekolah;
    }

    /**
     * Returns the value of field tahun_lulus
     *
     * @return string
     */
    public function getTahunLulus()
    {
        return $this->tahun_lulus;
    }

    /**
     * Returns the value of field jenis_kuliah
     *
     * @return string
     */
    public function getJenisKuliah()
    {
        return $this->jenis_kuliah;
    }

    /**
     * Returns the value of field wali
     *
     * @return string
     */
    public function getWali()
    {
        return $this->wali;
    }

    /**
     * Returns the value of field email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Returns the value of field agama_id_agama
     *
     * @return integer
     */
    public function getAgamaIdAgama()
    {
        return $this->agama_id_agama;
    }

    /**
     * Returns the value of field status_id_status
     *
     * @return integer
     */
    public function getStatusIdStatus()
    {
        return $this->status_id_status;
    }

    /**
     * Returns the value of field konsentrasi_id_konsentrasi
     *
     * @return integer
     */
    public function getKonsentrasiIdKonsentrasi()
    {
        return $this->konsentrasi_id_konsentrasi;
    }

    /**
     * Returns the value of field nama_orangtua
     *
     * @return string
     */
    public function getNamaOrangtua()
    {
        return $this->nama_orangtua;
    }

    /**
     * Returns the value of field pekerjaan_orangtua
     *
     * @return string
     */
    public function getPekerjaanOrangtua()
    {
        return $this->pekerjaan_orangtua;
    }

    /**
     * Returns the value of field alamat_orangtua
     *
     * @return string
     */
    public function getAlamatOrangtua()
    {
        return $this->alamat_orangtua;
    }

    /**
     * Returns the value of field kota_orangtua
     *
     * @return string
     */
    public function getKotaOrangtua()
    {
        return $this->kota_orangtua;
    }

    /**
     * Returns the value of field kodepos_orangtua
     *
     * @return string
     */
    public function getKodeposOrangtua()
    {
        return $this->kodepos_orangtua;
    }

    /**
     * Returns the value of field no_telp_orangtua
     *
     * @return string
     */
    public function getNoTelpOrangtua()
    {
        return $this->no_telp_orangtua;
    }

    /**
     * Return the value of field password
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * method to initialize the relashionship between tables
     * @return void
     */
    public function initialize()
    {
        $this->belongsTo('konsentrasi_id_konsentrasi', 'Konsentrasi', 'id_konsentrasi');
        $this->belongsTo('agama_id_agama', 'Agama', 'id_agama');
        $this->belongsTo('status_id_status', 'Status', 'id_status');
    }
    
    /**
     * validate the value of row before saving to database
     * @return mix return false if validation has failed and void if no error faound
     */
    public function validation()
    {

        $this->validate(
            new Email(
                array(
                    'field'    => 'email',
                    'required' => true,
                )
            )
        );
        if ($this->validationHasFailed() == true) {
            return false;
        }
    }

}
