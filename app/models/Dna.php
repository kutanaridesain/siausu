<?php
/**
 * @author Fery Putra Tarigan <kutanaridesain@gmail.com>
 * @link http://kutanari.com, http://kutanaridesain.com
 * @since 2015
 * @license MIT License
 */

class Dna extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $id_dna;

    /**
     *
     * @var string
     */
    protected $nim;

    /**
     *
     * @var string
     */
    protected $nama;

    /**
     *
     * @var string
     */
    protected $tugas;

    /**
     *
     * @var string
     */
    protected $kuis;

    /**
     *
     * @var string
     */
    protected $uts;

    /**
     *
     * @var string
     */
    protected $angka;

    /**
     *
     * @var string
     */
    protected $huruf;

    /**
     * Method to set the value of field id_dna
     *
     * @param integer $id_dna
     * @return $this
     */
    public function setIdDna($id_dna)
    {
        $this->id_dna = $id_dna;

        return $this;
    }

    /**
     * Method to set the value of field nim
     *
     * @param string $nim
     * @return $this
     */
    public function setNim($nim)
    {
        $this->nim = $nim;

        return $this;
    }

    /**
     * Method to set the value of field nama
     *
     * @param string $nama
     * @return $this
     */
    public function setNama($nama)
    {
        $this->nama = $nama;

        return $this;
    }

    /**
     * Method to set the value of field tugas
     *
     * @param string $tugas
     * @return $this
     */
    public function setTugas($tugas)
    {
        $this->tugas = $tugas;

        return $this;
    }

    /**
     * Method to set the value of field kuis
     *
     * @param string $kuis
     * @return $this
     */
    public function setKuis($kuis)
    {
        $this->kuis = $kuis;

        return $this;
    }

    /**
     * Method to set the value of field uts
     *
     * @param string $uts
     * @return $this
     */
    public function setUts($uts)
    {
        $this->uts = $uts;

        return $this;
    }

    /**
     * Method to set the value of field uas
     *
     * @param string $uas
     * @return $this
     */
    public function setUas($uas)
    {
        $this->uas = $uas;

        return $this;
    }

    /**
     * Method to set the value of field angka
     *
     * @param string $angka
     * @return $this
     */
    public function setAngka($angka)
    {
        $this->angka = $angka;

        return $this;
    }

    /**
     * Method to set the value of field huruf
     *
     * @param string $huruf
     * @return $this
     */
    public function setHuruf($huruf)
    {
        $this->huruf = $huruf;

        return $this;
    }

    /**
     * Method to set the value of field pengampu_id_pengampu
     *
     * @param string $pengampu_id_pengampu
     * @return $this
     */
    public function setIdPengampu($pengampu_id_pengampu)
    {
        $this->pengampu_id_pengampu = $pengampu_id_pengampu;

        return $this;
    }

    /**
     * Returns the value of field id_dna
     *
     * @return integer
     */
    public function getIdDna()
    {
        return $this->id_dna;
    }

    /**
     * Returns the value of field nim
     *
     * @return string
     */
    public function getNim()
    {
        return $this->nim;
    }

    /**
     * Returns the value of field nama
     *
     * @return string
     */
    public function getNama()
    {
        return $this->nama;
    }

    /**
     * Returns the value of field tugas
     *
     * @return string
     */
    public function getTugas()
    {
        return $this->tugas;
    }

    /**
     * Returns the value of field kuis
     *
     * @return string
     */
    public function getKuis()
    {
        return $this->kuis;
    }

    /**
     * Returns the value of field uts
     *
     * @return string
     */
    public function getUts()
    {
        return $this->uts;
    }

    /**
     * Returns the value of field uas
     *
     * @return string
     */
    public function getUas()
    {
        return $this->uas;
    }

    /**
     * Returns the value of field angka
     *
     * @return string
     */
    public function getAngka()
    {
        return $this->angka;
    }

    /**
     * Returns the value of field huruf
     *
     * @return string
     */
    public function getHuruf()
    {
        return $this->huruf;
    }

    /**
     * Returns the value of field pengampu_id_pengampu
     *
     * @return string
     */
    public function getIdPengampu()
    {
        return $this->pengampu_id_pengampu;
    }

}
