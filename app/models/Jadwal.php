<?php
/**
 * @author Fery Putra Tarigan <kutanaridesain@gmail.com>
 * @link http://kutanari.com, http://kutanaridesain.com
 * @since 2015
 * @license MIT License
 */


class Jadwal extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $id_jadwal;

    /**
     *
     * @var string
     */
    protected $hari;

    /**
     *
     * @var string
     */
    protected $mulai;

    /**
     *
     * @var string
     */
    protected $selesai;

    /**
     *
     * @var integer
     */
    protected $pengampu_id_pengampu;

    /**
     *
     * @var integer
     */
    protected $ruangan_id_ruangan;

    /**
     * Method to set the value of field id_jadwal
     *
     * @param integer $id_jadwal
     * @return $this
     */
    public function setIdJadwal($id_jadwal)
    {
        $this->id_jadwal = $id_jadwal;

        return $this;
    }

    /**
     * Method to set the value of field hari
     *
     * @param string $hari
     * @return $this
     */
    public function setHari($hari)
    {
        $this->hari = $hari;

        return $this;
    }

    /**
     * Method to set the value of field mulai
     *
     * @param string $mulai
     * @return $this
     */
    public function setMulai($mulai)
    {
        $this->mulai = $mulai;

        return $this;
    }

    /**
     * Method to set the value of field selesai
     *
     * @param string $selesai
     * @return $this
     */
    public function setSelesai($selesai)
    {
        $this->selesai = $selesai;

        return $this;
    }

    /**
     * Method to set the value of field pengampu_id_pengampu
     *
     * @param integer $pengampu_id_pengampu
     * @return $this
     */
    public function setPengampuIdPengampu($pengampu_id_pengampu)
    {
        $this->pengampu_id_pengampu = $pengampu_id_pengampu;

        return $this;
    }

    /**
     * Method to set the value of field ruangan_id_ruangan
     *
     * @param integer $ruangan_id_ruangan
     * @return $this
     */
    public function setRuanganIdRuangan($ruangan_id_ruangan)
    {
        $this->ruangan_id_ruangan = $ruangan_id_ruangan;

        return $this;
    }

    /**
     * Returns the value of field id_jadwal
     *
     * @return integer
     */
    public function getIdJadwal()
    {
        return $this->id_jadwal;
    }

    /**
     * Returns the value of field hari
     *
     * @return string
     */
    public function getHari()
    {
        return $this->hari;
    }

    /**
     * Returns the value of field mulai
     *
     * @return string
     */
    public function getMulai()
    {
        return $this->mulai;
    }

    /**
     * Returns the value of field selesai
     *
     * @return string
     */
    public function getSelesai()
    {
        return $this->selesai;
    }

    /**
     * Returns the value of field pengampu_id_pengampu
     *
     * @return integer
     */
    public function getPengampuIdPengampu()
    {
        return $this->pengampu_id_pengampu;
    }

    /**
     * Returns the value of field ruangan_id_ruangan
     *
     * @return integer
     */
    public function getRuanganIdRuangan()
    {
        return $this->ruangan_id_ruangan;
    }

}
