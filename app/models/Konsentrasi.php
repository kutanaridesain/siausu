<?php
/**
 * @author Fery Putra Tarigan <kutanaridesain@gmail.com>
 * @link http://kutanari.com, http://kutanaridesain.com
 * @since 2015
 * @license MIT License
 */


class Konsentrasi extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $id_konsentrasi;

    /**
     *
     * @var string
     */
    protected $nama_konsentrasi;

    /**
     * Method to set the value of field id_konsentrasi
     *
     * @param integer $id_konsentrasi
     * @return $this
     */
    public function setIdKonsentrasi($id_konsentrasi)
    {
        $this->id_konsentrasi = $id_konsentrasi;

        return $this;
    }

    /**
     * Method to set the value of field nama_konsentrasi
     *
     * @param string $nama_konsentrasi
     * @return $this
     */
    public function setNamaKonsentrasi($nama_konsentrasi)
    {
        $this->nama_konsentrasi = $nama_konsentrasi;

        return $this;
    }

    /**
     * Returns the value of field id_konsentrasi
     *
     * @return integer
     */
    public function getIdKonsentrasi()
    {
        return $this->id_konsentrasi;
    }

    /**
     * Returns the value of field nama_konsentrasi
     *
     * @return string
     */
    public function getNamaKonsentrasi()
    {
        return $this->nama_konsentrasi;
    }

}
