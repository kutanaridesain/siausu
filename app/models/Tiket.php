<?php

/*
The MIT License (MIT)
[OSI Approved License]
The MIT License (MIT)

Copyright (c) <2015> <kutanaridesain@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
 */

class Tiket extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $id_tiket;

    /**
     *
     * @var string
     */
    protected $judul_aduan;

    /**
     *
     * @var string
     */
    protected $isi_pesan;

    /**
     *
     * @var string
     */
    protected $status;

    /**
     *
     * @var string
     */
    protected $mahasiswa_nim;

    /**
     *
     * @var string
     */
    protected $tanggal_register;

    /**
     * Method to set the value of field id_tiket
     *
     * @param integer $id_tiket
     * @return $this
     */
    public function setIdTiket($id_tiket)
    {
        $this->id_tiket = $id_tiket;

        return $this;
    }

    /**
     * Method to set the value of field judul_aduan
     *
     * @param string $judul_aduan
     * @return $this
     */
    public function setJudulAduan($judul_aduan)
    {
        $this->judul_aduan = $judul_aduan;

        return $this;
    }

    /**
     * Method to set the value of field isi_pesan
     *
     * @param string $isi_pesan
     * @return $this
     */
    public function setIsiPesan($isi_pesan)
    {
        $this->isi_pesan = $isi_pesan;

        return $this;
    }

    /**
     * Method to set the value of field status
     *
     * @param string $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Method to set the value of field mahasiswa_nim
     *
     * @param string $mahasiswa_nim
     * @return $this
     */
    public function setMahasiswaNim($mahasiswa_nim)
    {
        $this->mahasiswa_nim = $mahasiswa_nim;

        return $this;
    }

    /**
     * Method to set the value of field tanggal_register
     *
     * @param string $tanggal_register
     * @return $this
     */
    public function setTanggalRegister($tanggal_register)
    {
        $this->tanggal_register = $tanggal_register;

        return $this;
    }

    /**
     * Returns the value of field id_tiket
     *
     * @return integer
     */
    public function getIdTiket()
    {
        return $this->id_tiket;
    }

    /**
     * Returns the value of field judul_aduan
     *
     * @return string
     */
    public function getJudulAduan()
    {
        return $this->judul_aduan;
    }

    /**
     * Returns the value of field isi_pesan
     *
     * @return string
     */
    public function getIsiPesan()
    {
        return $this->isi_pesan;
    }

    /**
     * Returns the value of field status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Returns the value of field mahasiswa_nim
     *
     * @return string
     */
    public function getMahasiswaNim()
    {
        return $this->mahasiswa_nim;
    }

    /**
     * Returns the value of field tanggal_register
     *
     * @return string
     */
    public function getTanggalRegister()
    {
        return $this->tanggal_register;
    }

    /**
     * method to initialize relation with other table
     * @return void 
     */
    public function initialize()
    {
        $this->belongsTo('mahasiswa_nim', 'Mahasiswa', 'nim');
    }

}
