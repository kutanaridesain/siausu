<?php
/**
 * @author Fery Putra Tarigan <kutanaridesain@gmail.com>
 * @link http://kutanari.com, http://kutanaridesain.com
 * @since 2015
 * @license MIT License
 */


class Krs extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $id_krs;

    /**
     *
     * @var string
     */
    protected $ta;

    /**
     *
     * @var string
     */
    protected $semester;

    /**
     *
     * @var string
     */
    protected $mahasiswa_nim;

    /**
     *
     * @var integer
     */
    protected $jadwal_id_jadwal;

    /**
     * Method to set the value of field id_krs
     *
     * @param integer $id_krs
     * @return $this
     */
    public function setIdKrs($id_krs)
    {
        $this->id_krs = $id_krs;

        return $this;
    }

    /**
     * Method to set the value of field ta
     *
     * @param string $ta
     * @return $this
     */
    public function setTa($ta)
    {
        $this->ta = $ta;

        return $this;
    }

    /**
     * Method to set the value of field semester
     *
     * @param string $semester
     * @return $this
     */
    public function setSemester($semester)
    {
        $this->semester = $semester;

        return $this;
    }

    /**
     * Method to set the value of field mahasiswa_nim
     *
     * @param string $mahasiswa_nim
     * @return $this
     */
    public function setMahasiswaNim($mahasiswa_nim)
    {
        $this->mahasiswa_nim = $mahasiswa_nim;

        return $this;
    }

    /**
     * Method to set the value of field jadwal_id_jadwal
     *
     * @param integer $jadwal_id_jadwal
     * @return $this
     */
    public function setJadwalIdJadwal($jadwal_id_jadwal)
    {
        $this->jadwal_id_jadwal = $jadwal_id_jadwal;

        return $this;
    }

    /**
     * Returns the value of field id_krs
     *
     * @return integer
     */
    public function getIdKrs()
    {
        return $this->id_krs;
    }

    /**
     * Returns the value of field ta
     *
     * @return string
     */
    public function getTa()
    {
        return $this->ta;
    }

    /**
     * Returns the value of field semester
     *
     * @return string
     */
    public function getSemester()
    {
        return $this->semester;
    }

    /**
     * Returns the value of field mahasiswa_nim
     *
     * @return string
     */
    public function getMahasiswaNim()
    {
        return $this->mahasiswa_nim;
    }

    /**
     * Returns the value of field jadwal_id_jadwal
     *
     * @return integer
     */
    public function getJadwalIdJadwal()
    {
        return $this->jadwal_id_jadwal;
    }

    public function initialize()
    {
        $this->belongsTo('jadwal_id_jadwal', 'Jadwal', 'id_jadwal');
    }

}
