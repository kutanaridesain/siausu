<?php
/**
 * @author Fery Putra Tarigan <kutanaridesain@gmail.com>
 * @link http://kutanari.com, http://kutanaridesain.com
 * @since 2015
 * @license MIT License
 */

use Phalcon\Mvc\Model\Validator\Uniqueness;

class Ruangan extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $id_ruangan;

    /**
     *
     * @var string
     */
    protected $nama_ruangan;

    /**
     *
     * @var integer
     */
    protected $kapasitas_ruangan;

    /**
     * Method to set the value of field id_ruangan
     *
     * @param integer $id_ruangan
     * @return $this
     */
    public function setIdRuangan($id_ruangan)
    {
        $this->id_ruangan = $id_ruangan;

        return $this;
    }

    /**
     * Method to set the value of field nama_ruangan
     *
     * @param string $nama_ruangan
     * @return $this
     */
    public function setNamaRuangan($nama_ruangan)
    {
        $this->nama_ruangan = $nama_ruangan;

        return $this;
    }

    /**
     * Method to set the value of field kapasitas_ruangan
     *
     * @param integer $kapasitas_ruangan
     * @return $this
     */
    public function setKapasitasRuangan($kapasitas_ruangan)
    {
        $this->kapasitas_ruangan = $kapasitas_ruangan;

        return $this;
    }

    /**
     * Returns the value of field id_ruangan
     *
     * @return integer
     */
    public function getIdRuangan()
    {
        return $this->id_ruangan;
    }

    /**
     * Returns the value of field nama_ruangan
     *
     * @return string
     */
    public function getNamaRuangan()
    {
        return $this->nama_ruangan;
    }

    /**
     * Returns the value of field kapasitas_ruangan
     *
     * @return integer
     */
    public function getKapasitasRuangan()
    {
        return $this->kapasitas_ruangan;
    }

    public function validation()
    {
        $this->validate(new Uniqueness(
            array(
                'field'=>'nama_ruangan',
                'message'=>'Nama ruangan ini sudah terdaftar'
                )
        ));

        return $this->validationHasFailed() != true;
    }

}
