<?php
/**
 * @author Fery Putra Tarigan <kutanaridesain@gmail.com>
 * @link http://kutanari.com, http://kutanaridesain.com
 * @since 2015
 * @license MIT License
 */


class Matakuliah extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $id_matakuliah;

    /**
     *
     * @var string
     */
    protected $kode_matakuliah;

    /**
     *
     * @var string
     */
    protected $nama_matakuliah;

    /**
     *
     * @var integer
     */
    protected $sks;

    /**
     *
     * @var integer
     */
    protected $semester;

    /**
     * Method to set the value of field id_matakuliah
     *
     * @param integer $id_matakuliah
     * @return $this
     */
    public function setIdMatakuliah($id_matakuliah)
    {
        $this->id_matakuliah = $id_matakuliah;

        return $this;
    }

    /**
     * Method to set the value of field kode_matakuliah
     *
     * @param string $kode_matakuliah
     * @return $this
     */
    public function setKodeMatakuliah($kode_matakuliah)
    {
        $this->kode_matakuliah = $kode_matakuliah;

        return $this;
    }

    /**
     * Method to set the value of field nama_matakuliah
     *
     * @param string $nama_matakuliah
     * @return $this
     */
    public function setNamaMatakuliah($nama_matakuliah)
    {
        $this->nama_matakuliah = $nama_matakuliah;

        return $this;
    }

    /**
     * Method to set the value of field sks
     *
     * @param integer $sks
     * @return $this
     */
    public function setSks($sks)
    {
        $this->sks = $sks;

        return $this;
    }

    /**
     * Method to set the value of field semester
     *
     * @param integer $semester
     * @return $this
     */
    public function setSemester($semester)
    {
        $this->semester = $semester;

        return $this;
    }

    /**
     * Returns the value of field id_matakuliah
     *
     * @return integer
     */
    public function getIdMatakuliah()
    {
        return $this->id_matakuliah;
    }

    /**
     * Returns the value of field kode_matakuliah
     *
     * @return string
     */
    public function getKodeMatakuliah()
    {
        return $this->kode_matakuliah;
    }

    /**
     * Returns the value of field nama_matakuliah
     *
     * @return string
     */
    public function getNamaMatakuliah()
    {
        return $this->nama_matakuliah;
    }

    /**
     * Returns the value of field sks
     *
     * @return integer
     */
    public function getSks()
    {
        return $this->sks;
    }

    /**
     * Returns the value of field semester
     *
     * @return integer
     */
    public function getSemester()
    {
        return $this->semester;
    }

}
