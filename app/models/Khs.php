<?php

/*
The MIT License (MIT)
[OSI Approved License]
The MIT License (MIT)

Copyright (c) <2015> <kutanaridesain@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
 */

class Khs extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $id_khs;

    /**
     *
     * @var string
     */
    protected $ta;

    /**
     *
     * @var string
     */
    protected $semester;

    /**
     *
     * @var string
     */
    protected $mahasiswa_nim;

    /**
     *
     * @var integer
     */
    protected $jadwal_id_jadwal;

    /**
     * Method to set the value of field id_khs
     *
     * @param integer $id_khs
     * @return $this
     */
    public function setIdKhs($id_khs)
    {
        $this->id_khs = $id_khs;

        return $this;
    }

    /**
     * Method to set the value of field ta
     *
     * @param string $ta
     * @return $this
     */
    public function setTa($ta)
    {
        $this->ta = $ta;

        return $this;
    }

    /**
     * Method to set the value of field semester
     *
     * @param string $semester
     * @return $this
     */
    public function setSemester($semester)
    {
        $this->semester = $semester;

        return $this;
    }

    /**
     * Method to set the value of field mahasiswa_nim
     *
     * @param string $mahasiswa_nim
     * @return $this
     */
    public function setMahasiswaNim($mahasiswa_nim)
    {
        $this->mahasiswa_nim = $mahasiswa_nim;

        return $this;
    }

    /**
     * Method to set the value of field jadwal_id_jadwal
     *
     * @param integer $jadwal_id_jadwal
     * @return $this
     */
    public function setJadwalIdJadwal($jadwal_id_jadwal)
    {
        $this->jadwal_id_jadwal = $jadwal_id_jadwal;

        return $this;
    }

    /**
     * Returns the value of field id_khs
     *
     * @return integer
     */
    public function getIdKhs()
    {
        return $this->id_khs;
    }

    /**
     * Returns the value of field ta
     *
     * @return string
     */
    public function getTa()
    {
        return $this->ta;
    }

    /**
     * Returns the value of field semester
     *
     * @return string
     */
    public function getSemester()
    {
        return $this->semester;
    }

    /**
     * Returns the value of field mahasiswa_nim
     *
     * @return string
     */
    public function getMahasiswaNim()
    {
        return $this->mahasiswa_nim;
    }

    /**
     * Returns the value of field jadwal_id_jadwal
     *
     * @return integer
     */
    public function getJadwalIdJadwal()
    {
        return $this->jadwal_id_jadwal;
    }

}
