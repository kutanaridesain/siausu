<?php
/**
 * @author Fery Putra Tarigan <kutanaridesain@gmail.com>
 * @link http://kutanari.com, http://kutanaridesain.com
 * @since 2015
 * @license MIT License
 */

use \Phalcon\Mvc\Model\Message as Message;

class Jurusan extends CustomModel
{

    /**
     *
     * @var integer
     */
    protected $id_jurusan;

    /**
     *
     * @var string
     */
    protected $nama_jurusan;

    /**
     * Method to set the value of field id_jurusan
     *
     * @param integer $id_jurusan
     * @return $this
     */
    public function setIdJurusan($id_jurusan)
    {
        $this->id_jurusan = $id_jurusan;

        return $this;
    }

    /**
     * Method to set the value of field nama_jurusan
     *
     * @param string $nama_jurusan
     * @return $this
     */
    public function setNamaJurusan($nama_jurusan)
    {
        // if (strlen($nama_jurusan) < 10) {
        //     // throw new \InvalidArgumentException('The name is too short');
        //     // $message = new Message("Sorry, but a robot cannot be named Peter");
        //     // $this->appendMessage($message);
        //     // return $message;
        //     $text = "A robot cannot be named Peter";
        //     $field = "nama_jurusan";
        //     $type = "InvalidValue";
        //     $message = new Message($text, $field, $type);
        //     $this->appendMessage($message);
        //     return false;
        //     die()
        // }

        $this->nama_jurusan = $nama_jurusan;
        return $this;
    }

    /**
     * Returns the value of field id_jurusan
     *
     * @return integer
     */
    public function getIdJurusan()
    {
        return $this->id_jurusan;
    }

    /**
     * Returns the value of field nama_jurusan
     *
     * @return string
     */
    public function getNamaJurusan()
    {
        return $this->nama_jurusan;
    }

    public function beforeSave()
    {
        if (strlen($this->nama_jurusan) < 10) {
            $text = "A robot cannot be named Peter";
            $field = "name";
            $type = "InvalidValue";
            $message = new Message($text, $field, $type);
            $this->appendMessage($message);
            return false;
        }
    }

}
