<?php
/**
 * @author Fery Putra Tarigan <kutanaridesain@gmail.com>
 * @link http://kutanari.com, http://kutanaridesain.com
 * @since 2015
 * @license MIT License
 */


class Pengampu extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $id_pengampu;

    /**
     *
     * @var string
     */
    protected $dosen_nip;

    /**
     *
     * @var integer
     */
    protected $matakuliah_id_matakuliah;

    public function initialize()
    {
        $this->belongsTo("dosen_nip", "Dosen", "nip");
        $this->belongsTo("matakuliah_id_matakuliah", "Matakuliah", "id_matakuliah");
    }

    /**
     * Method to set the value of field id_pengampu
     *
     * @param integer $id_pengampu
     * @return $this
     */
    public function setIdPengampu($id_pengampu)
    {
        $this->id_pengampu = $id_pengampu;

        return $this;
    }

    /**
     * Method to set the value of field dosen_nip
     *
     * @param string $dosen_nip
     * @return $this
     */
    public function setDosenNip($dosen_nip)
    {
        $this->dosen_nip = $dosen_nip;

        return $this;
    }

    /**
     * Method to set the value of field matakuliah_id_matakuliah
     *
     * @param integer $matakuliah_id_matakuliah
     * @return $this
     */
    public function setMatakuliahIdMatakuliah($matakuliah_id_matakuliah)
    {
        $this->matakuliah_id_matakuliah = $matakuliah_id_matakuliah;

        return $this;
    }

    /**
     * Returns the value of field id_pengampu
     *
     * @return integer
     */
    public function getIdPengampu()
    {
        return $this->id_pengampu;
    }

    /**
     * Returns the value of field dosen_nip
     *
     * @return string
     */
    public function getDosenNip()
    {
        return $this->dosen_nip;
    }

    /**
     * Returns the value of field matakuliah_id_matakuliah
     *
     * @return integer
     */
    public function getMatakuliahIdMatakuliah()
    {
        return $this->matakuliah_id_matakuliah;
    }

}
