<?php
/**
 * @author Fery Putra Tarigan <kutanaridesain@gmail.com>
 * @link http://kutanari.com, http://kutanaridesain.com
 * @since 2015
 * @license MIT License
 */


class JadwalKuliah extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $id_jadwal;

    /**
     *
     * @var string
     */
    protected $hari;

    /**
     *
     * @var string
     */
    protected $mulai;

    /**
     *
     * @var string
     */
    protected $selesai;

    /**
     *
     * @var string
     */
    protected $nama_ruangan;

    /**
     *
     * @var string
     */
    protected $nama_dosen;

    /**
     *
     * @var string
     */
    protected $nama_matakuliah;

    /**
     *
     * @var string
     */
    protected $kode_matakuliah;

    /**
     *
     * @var integer
     */
    protected $sks;

    /**
     *
     * @var integer
     */
    protected $semester;

    /**
     * Returns the value of field id_jadwal
     *
     * @return integer
     */
    public function getIdJadwal()
    {
        return $this->id_jadwal;
    }

    /**
     * Returns the value of field hari
     *
     * @return string
     */
    public function getHari()
    {
        return $this->hari;
    }

    /**
     * Returns the value of field mulai
     *
     * @return string
     */
    public function getMulai()
    {
        return $this->mulai;
    }

    /**
     * Returns the value of field selesai
     *
     * @return string
     */
    public function getSelesai()
    {
        return $this->selesai;
    }

    /**
     * Returns the value of field nama_ruangan
     *
     * @return string
     */
    public function getNamaRuangan()
    {
        return $this->nama_ruangan;
    }

    /**
     * Returns the value of field nama_dosen
     *
     * @return string
     */
    public function getNamaDosen()
    {
        return $this->nama_dosen;
    }

    /**
     * Returns the value of field nama_matakuliah
     *
     * @return string
     */
    public function getNamaMatakuliah()
    {
        return $this->nama_matakuliah;
    }

    /**
     * Returns the value of field kode_matakuliah
     *
     * @return string
     */
    public function getKodeMatakuliah()
    {
        return $this->kode_matakuliah;
    }

    /**
     * Returns the value of field sks
     *
     * @return integer
     */
    public function getSks()
    {
        return $this->sks;
    }

    /**
     * Returns the value of field semester
     *
     * @return integer
     */
    public function getSemester()
    {
        return $this->semester;
    }

}
