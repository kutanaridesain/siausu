<?php
/**
 * @author Fery Putra Tarigan <kutanaridesain@gmail.com>
 * @link http://kutanari.com, http://kutanaridesain.com
 * @since 2015
 * @license MIT License
 */


/**
* Custom Overiden Model
*/
class CustomModel extends \Phalcon\Mvc\Model
{
    public function getMessages()
    {
        $messages = array();
        foreach (parent::getMessages() as $message) {
            switch ($message->getType()) {
                case 'InvalidCreateAttempt':
                    $messages[] = 'The record cannot be created because it already exists';
                    break;
                case 'InvalidUpdateAttempt':
                    $messages[] = 'The record cannot be updated because it already exists';
                    break;
                case 'PresenceOf':
                    $messages[] = 'Field ' . $message->getField() . ' harus diisi !';
                    break;
                // case 'InvalidValue':
                //     $messages[] = 'Data field '. $message->getField() .' salah';
                //     break;
            }
        }
        return $messages;
    }

}