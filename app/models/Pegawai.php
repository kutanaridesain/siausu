<?php
/**
 * @author Fery Putra Tarigan <kutanaridesain@gmail.com>
 * @link http://kutanari.com, http://kutanaridesain.com
 * @since 2015
 * @license MIT License
 */


use Phalcon\Mvc\Model\Validator\Email as Email;

class Pegawai extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var string
     */
    protected $nip;

    /**
     *
     * @var string
     */
    protected $nama_pegawai;

    /**
     *
     * @var string
     */
    protected $jenis_kelamin;

    /**
     *
     * @var string
     */
    protected $alamat;

    /**
     *
     * @var string
     */
    protected $email;

    /**
     *
     * @var string
     */
    protected $telp;

    /**
     *
     * @var string
     */
    protected $status;

    /**
     *
     * @var string
     */
    protected $password;

    /**
     * Method to set the value of field nip
     *
     * @param string $nip
     * @return $this
     */
    public function setNip($nip)
    {
        $this->nip = $nip;

        return $this;
    }

    /**
     * Method to set the value of field nama_pegawai
     *
     * @param string $nama_pegawai
     * @return $this
     */
    public function setNamaPegawai($nama_pegawai)
    {
        $this->nama_pegawai = $nama_pegawai;

        return $this;
    }

    /**
     * Method to set the value of field jenis_kelamin
     *
     * @param string $jenis_kelamin
     * @return $this
     */
    public function setJenisKelamin($jenis_kelamin)
    {
        $this->jenis_kelamin = $jenis_kelamin;

        return $this;
    }

    /**
     * Method to set the value of field alamat
     *
     * @param string $alamat
     * @return $this
     */
    public function setAlamat($alamat)
    {
        $this->alamat = $alamat;

        return $this;
    }

    /**
     * Method to set the value of field email
     *
     * @param string $email
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Method to set the value of field telp
     *
     * @param string $telp
     * @return $this
     */
    public function setTelp($telp)
    {
        $this->telp = $telp;

        return $this;
    }

    /**
     * Method to set the value of field status
     *
     * @param string $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Method to set the value of field password
     *
     * @param string $password
     * @return $this
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Returns the value of field nip
     *
     * @return string
     */
    public function getNip()
    {
        return $this->nip;
    }

    /**
     * Returns the value of field nama_pegawai
     *
     * @return string
     */
    public function getNamaPegawai()
    {
        return $this->nama_pegawai;
    }

    /**
     * Returns the value of field jenis_kelamin
     *
     * @return string
     */
    public function getJenisKelamin()
    {
        return $this->jenis_kelamin;
    }

    /**
     * Returns the value of field alamat
     *
     * @return string
     */
    public function getAlamat()
    {
        return $this->alamat;
    }

    /**
     * Returns the value of field email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Returns the value of field telp
     *
     * @return string
     */
    public function getTelp()
    {
        return $this->telp;
    }

    /**
     * Returns the value of field status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Returns the value of field password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Validations and business logic
     */
    public function validation()
    {

        $this->validate(
            new Email(
                array(
                    'field'    => 'email',
                    'required' => true,
                )
            )
        );
        if ($this->validationHasFailed() == true) {
            return false;
        }
    }

}
