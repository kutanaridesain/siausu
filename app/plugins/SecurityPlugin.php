<?php

use Phalcon\Acl;
use Phalcon\Acl\Role;
use Phalcon\Acl\Resource;
use Phalcon\Events\Event;
use Phalcon\Mvc\User\Plugin;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Acl\Adapter\Memory as AclList;

/**
 * SecurityPlugin
 *
 * This is the security plugin which controls that users only have access to the modules they're assigned to
 */
class SecurityPlugin extends Plugin
{

    /**
     * Returns an existing or new access control list
     *
     * @returns AclList
     */
    public function getAcl()
    {

        //throw new \Exception("something");
        $this->persistent->acl = null; 
        if (!isset($this->persistent->acl) || $this->persistent->acl == null) {

            $acl = new AclList();

            $acl->setDefaultAction(Acl::DENY);

            //Register roles
            $roles = array(
                'admin'  => new Role('Admin'),
                'dosen' => new Role('Dosen'),
                'pegawai' => new Role('Pegawai'),
                'mahasiswa' => new Role('Mahasiswa')
            );
            foreach ($roles as $role) {
                $acl->addRole($role);
            }

            //Private area resources
            $adminResources = array(
                'agama' => array('index', 'search', 'new', 'edit', 'save', 'create', 'delete', 'ajax'),
                'dosen' => array('index', 'search', 'new', 'edit', 'save', 'create', 'delete', 'ajax'),
                'jadwal' => array('index', 'search', 'new', 'edit', 'save', 'create', 'delete', 'ajax', 'kuliah'),
                'jurusan' => array('index', 'search', 'new', 'edit', 'save', 'create', 'delete', 'ajax'),
                'konsentrasi' => array('index', 'search', 'new', 'edit', 'save', 'create', 'delete', 'ajax'),
                'mahasiswa' => array('index', 'search', 'new', 'edit', 'save', 'create', 'delete', 'ajax', 'detail'),
                'matakuliah' => array('index', 'search', 'new', 'edit', 'save', 'create', 'delete', 'ajax'),
                'pengampu' => array('index', 'search', 'new', 'edit', 'save', 'create', 'delete', 'ajax'),
                'pengguna' => array('index', 'search', 'new', 'edit', 'save', 'create', 'delete', 'ajax'),
                'peralatan' => array('index', 'search', 'new', 'edit', 'save', 'create', 'delete', 'ajax', 'importdna'),
                'ruangan' => array('index', 'search', 'new', 'edit', 'save', 'create', 'delete', 'ajax'),
                'khs' => array('index', 'search', 'new', 'edit', 'save', 'create', 'delete', 'ajax', 'info'),
                'tiket' => array('index', 'search', 'new', 'edit', 'save', 'create', 'delete', 'ajax')
            );
            foreach ($adminResources as $resource => $actions) {
                $acl->addResource(new Resource($resource), $actions);
            }

            //Public area resources
            $publicResources = array(
                'index'      => array('index'),
                'errors'     => array('show404', 'show500', 'show401'),
                'user'       => array('profil'),
                'login'    => array('index', 'logout')
            );
            foreach ($publicResources as $resource => $actions) {
                $acl->addResource(new Resource($resource), $actions);
            }

            //Grant access to public areas to both users and guests
            foreach ($roles as $role) {
                foreach ($publicResources as $resource => $actions) {
                    foreach ($actions as $action){
                        $acl->allow($role->getName(), $resource, $action);
                    }
                }
            }

            //Grant acess to private area to role Users
            foreach ($adminResources as $resource => $actions) {
                foreach ($actions as $action){
                    $acl->allow('Admin', $resource, $action);
                }
            }

            $mahasiswaResources = array(
                'jadwal' => array('kuliah'),
                'jurusan' => array('index', 'ajax'),
                'konsentrasi' => array('index', 'ajax'),
                'mahasiswa' => array('index', 'ajax', 'detail'),
                'matakuliah' => array('index', 'ajax'),
                'pengampu' => array('index', 'ajax'),
                'ruangan' => array('index', 'ajax'),
                'khs' => array('info', 'ajax'),
                'tiket' => array('index', 'new', 'edit', 'save', 'create', 'delete', 'ajax')
            );

            //Grant acess to private area to role Users
            foreach ($mahasiswaResources as $resource => $actions) {
                foreach ($actions as $action){
                    $acl->allow('Mahasiswa', $resource, $action);
                }
            }

            //The acl is stored in session, APC would be useful here too
            $this->persistent->acl = $acl;
        }
        return $this->persistent->acl;
    }

    /**
     * This action is executed before execute any action in the application
     *
     * @param Event $event
     * @param Dispatcher $dispatcher
     */
    public function beforeDispatch(Event $event, Dispatcher $dispatcher)
    {

        $role = $this->session->get('auth')['role'];

        $controller = $dispatcher->getControllerName();
        $action = $dispatcher->getActionName();
        $excludedController = array('index', 'login');
        if(!empty($role)){
            if (!in_array($controller, $excludedController)) {
                $acl = $this->getAcl();
                $allowed = $acl->isAllowed(ucfirst($role), $controller, $action);
                if ($allowed != Acl::ALLOW) {

                    $dispatcher->forward(array(
                        'controller' => 'errors',
                        'action'     => 'show401'
                    ));
                    return false;
                }
            }        
        }
    }
}
