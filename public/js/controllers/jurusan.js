//controller jurusan
    app.controller('JurusanController', function($scope, $http){
        $scope.nama_jurusan = '';
        $scope.id = '';
        $scope.idx = '';

        $http.get('jurusan/ajax').success(function(data) {
            $scope.listJurusan = data;
        });
        //fungsi untuk menambah dan mengubah data jurusan
        $scope.simpanJurusan = function simpanjurusan() {
            var dataObj = {
                nama_jurusan : $scope.nama_jurusan,
                id: $scope.id
            };
            if($scope.id != ''){
                var res = $http.put('/jurusan/ajax', dataObj);
                $scope.listJurusan.splice($scope.idx, 1);
                $scope.listJurusan.splice($scope.idx, 0, dataObj);
                $('#modalJurusan').modal('hide');
            }else{
                var res = $http.post('/jurusan/ajax', dataObj);
                $scope.listJurusan.push({'id':"0", 'nama_jurusan':$scope.nama_jurusan});
                $scope.nama_jurusan = '';
            }
            res.success(function(data, status, headers, config) {
                $http.get('jurusan/ajax').success(function(data) {
                    $scope.listJurusan = data;
                });
            });
            res.error(function(data, status, headers, config) {
                alert( "failure message: " + JSON.stringify({data: data}));
            });     
        };

        //kosongkan id dan nama_jurusan untuk proses penambahan data jurusan
        $scope.tambah = function tambah(){
            $scope.id = '';
            $scope.nama_jurusan = '';
        }

        //hapus data jurusan berdasarkan index pada array untuk melakukan pemilihan data
        $scope.hapus = function hapus(idx){
            if(confirm('Anda yakin menghapus item ini?')==false)return false;
            var itemDihapus = $scope.listJurusan[idx];
            $scope.listJurusan.splice(idx, 1);
            var res = $http.delete('/jurusan/ajax/'+itemDihapus.id);
        }

        //fungsi untuk mengubah data jurusan, idx berfungsi sebagai pointer untuk mengubah data pada array jurusan, sedangkan id adalah id jurusan yang tersimpan di database
        $scope.ubah = function ubah(idx){
            var itemTerpilih = $scope.listJurusan[idx];
            $scope.nama_jurusan = itemTerpilih.nama_jurusan;
            $scope.id = itemTerpilih.id;
            $scope.idx = idx;
        }
    });