console.log(app);
//controller agama
    app.controller('AgamaController', function($scope, $http){
        $scope.nama_agama = '';
        $scope.id = '';
        $scope.idx = '';

        $http.get('agama/ajax').success(function(data) {
            $scope.listAgama = data;
        });
        // $scope.$watch('nama_agama',function() {$scope.coba();});
        //fungsi untuk menambah dan mengubah data agama
        $scope.simpanAgama = function simpanAgama() {
            var dataObj = {
                nama_agama : $scope.nama_agama,
                id: $scope.id
            };
            if($scope.id != ''){
                var res = $http.put('/agama/ajax', dataObj);
                $scope.listAgama.splice($scope.idx, 1);
                $scope.listAgama.splice($scope.idx, 0, dataObj);
                $('#myModal').modal('hide');
            }else{
                var res = $http.post('/agama/ajax', dataObj);
                $scope.listAgama.push({'id':"0", 'nama_agama':$scope.nama_agama});
                $scope.nama_agama = '';
            }
            res.success(function(data, status, headers, config) {
                $http.get('agama/ajax').success(function(data) {
                    $scope.listAgama = data;
                });
            });
            res.error(function(data, status, headers, config) {
                alert( "failure message: " + JSON.stringify({data: data}));
            });     
        };

        //kosongkan id dan nama_agama untuk proses penambahan data agama
        $scope.tambah = function tambah(){
            $scope.id = '';
            $scope.nama_agama = '';
        }

        //hapus data agama berdasarkan index pada array untuk melakukan pemilihan data
        $scope.hapus = function hapus(idx){
            if(confirm('Anda yakin menghapus item ini?')==false)return false;
            var itemDihapus = $scope.listAgama[idx];
            $scope.listAgama.splice(idx, 1);
            var res = $http.delete('/agama/ajax/'+itemDihapus.id);
        }

        //fungsi untuk mengubah data agama, idx berfungsi sebagai pointer untuk mengubah data pada array agama, sedangkan id adalah id agama yang tersimpan di database
        $scope.ubah = function ubah(idx){
            var itemTerpilih = $scope.listAgama[idx];
            $scope.nama_agama = itemTerpilih.nama_agama;
            $scope.id = itemTerpilih.id;
            $scope.idx = idx;
        }
    });