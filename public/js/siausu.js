(function() {
    var app = angular.module('siausuApp', ['ngTable', 'ngTableExport']);
    app.config(function($interpolateProvider) {
        $interpolateProvider.startSymbol('[[');
        $interpolateProvider.endSymbol(']]');
    });

    app.directive('tooltip', function(){
        return {
            restrict: 'A',
            link: function(scope, element, attrs){
                $(element).hover(function(){
                    // on mouseenter
                    $(element).tooltip('show');
                }, function(){
                    // on mouseleave
                    $(element).tooltip('hide');
                });
            }
        };
    });

    //controller agama
    app.controller('AgamaController', function($scope, $http){
        $scope.nama_agama = '';
        $scope.id = '';
        $scope.idx = '';

        $http.get('agama/ajax').success(function(data) {
            $scope.listAgama = data;
        });
        // $scope.$watch('nama_agama',function() {$scope.coba();});
        //fungsi untuk menambah dan mengubah data agama
        $scope.simpanAgama = function simpanAgama() {
            var dataObj = {
                nama_agama : $scope.nama_agama,
                id: $scope.id
            };
            if($scope.id != ''){
                var res = $http.put('/agama/ajax', dataObj);
                $scope.listAgama.splice($scope.idx, 1);
                $scope.listAgama.splice($scope.idx, 0, dataObj);
                $('#myModal').modal('hide');
            }else{
                var res = $http.post('/agama/ajax', dataObj);
                $scope.listAgama.push({'id':"0", 'nama_agama':$scope.nama_agama});
                $scope.nama_agama = '';
            }
            res.success(function(data, status, headers, config) {
                $http.get('agama/ajax').success(function(data) {
                    $scope.listAgama = data;
                });
            });
            res.error(function(data, status, headers, config) {
                alert( "failure message: " + JSON.stringify({data: data}));
            });
        };

        //kosongkan id dan nama_agama untuk proses penambahan data agama
        $scope.tambah = function tambah(){
            $scope.id = '';
            $scope.nama_agama = '';
            $scope.action = 'Tambah';
        }

        //hapus data agama berdasarkan index pada array untuk melakukan pemilihan data
        $scope.hapus = function hapus(idx){
            if(confirm('Anda yakin menghapus item ini?')==false)return false;
            var itemDihapus = $scope.listAgama[idx];
            $scope.listAgama.splice(idx, 1);
            var res = $http.delete('/agama/ajax/'+itemDihapus.id);
        }

        //fungsi untuk mengubah data agama, idx berfungsi sebagai pointer untuk mengubah data pada array agama, sedangkan id adalah id agama yang tersimpan di database
        $scope.ubah = function ubah(idx){
            var itemTerpilih = $scope.listAgama[idx];
            $scope.nama_agama = itemTerpilih.nama_agama;
            $scope.id = itemTerpilih.id;
            $scope.idx = idx;
            $scope.action = 'Ubah';
        }
    });

    //controller konsentrasi
    app.controller('KonsentrasiController', function($scope, $http){
        $scope.nama_konsentrasi = '';
        $scope.id = '';
        $scope.idx = '';

        $http.get('konsentrasi/ajax').success(function(data) {
            $scope.listKonsentrasi = data;
        });
        //fungsi untuk menambah dan mengubah data konsentrasi
        $scope.simpanKonsentrasi = function simpanKonsentrasi() {
            var dataObj = {
                nama_konsentrasi : $scope.nama_konsentrasi,
                id: $scope.id
            };
            if($scope.id != ''){
                var res = $http.put('/konsentrasi/ajax', dataObj);
                $scope.listKonsentrasi.splice($scope.idx, 1);
                $scope.listKonsentrasi.splice($scope.idx, 0, dataObj);
                $('#modalKonsentrasi').modal('hide');
            }else{
                var res = $http.post('/konsentrasi/ajax', dataObj);
                $scope.listKonsentrasi.push({'id':"0", 'nama_konsentrasi':$scope.nama_konsentrasi});
                $scope.nama_konsentrasi = '';
            }
            res.success(function(data, status, headers, config) {
                $http.get('konsentrasi/ajax').success(function(data) {
                    $scope.listKonsentrasi = data;
                });
            });
            res.error(function(data, status, headers, config) {
                alert( "failure message: " + JSON.stringify({data: data}));
            });
        };

        //kosongkan id dan nama_konsentrasi untuk proses penambahan data konsentrasi
        $scope.tambah = function tambah(){
            $scope.id = '';
            $scope.nama_konsentrasi = '';
        }

        //hapus data konsentrasi berdasarkan index pada array untuk melakukan pemilihan data
        $scope.hapus = function hapus(idx){
            if(confirm('Anda yakin menghapus item ini?')==false)return false;
            var itemDihapus = $scope.listKonsentrasi[idx];
            $scope.listKonsentrasi.splice(idx, 1);
            var res = $http.delete('/konsentrasi/ajax/'+itemDihapus.id);
        }

        //fungsi untuk mengubah data konsentrasi, idx berfungsi sebagai pointer untuk mengubah data pada array konsentrasi, sedangkan id adalah id konsentrasi yang tersimpan di database
        $scope.ubah = function ubah(idx){
            var itemTerpilih = $scope.listKonsentrasi[idx];
            $scope.nama_konsentrasi = itemTerpilih.nama_konsentrasi;
            $scope.id = itemTerpilih.id;
            $scope.idx = idx;
        }
    });

    //controller status
    app.controller('StatusController', function($scope, $http){
        $scope.nama_status = '';
        $scope.id = '';
        $scope.idx = '';

        $http.get('status/ajax').success(function(data) {
            if(data.status){
                $scope.listStatus = data.data;
            }
        });
        //fungsi untuk menambah dan mengubah data status
        $scope.simpanStatus = function simpanStatus() {
            var dataObj = {
                nama_status : $scope.nama_status,
                id: $scope.id
            };
            if($scope.id != ''){
                var res = $http.put('/status/ajax', dataObj);
                $scope.listStatus.splice($scope.idx, 1);
                $scope.listStatus.splice($scope.idx, 0, dataObj);
                $('#modalStatus').modal('hide');
            }else{
                var res = $http.post('/status/ajax', dataObj);
                $scope.listStatus.push({'id':"0", 'nama_status':$scope.nama_status});
                $scope.nama_status = '';
            }
            res.success(function(data, status, headers, config) {
                $http.get('status/ajax').success(function(data) {
                    if(data.status){
                        $scope.listStatus = data.data;
                    }else{
                        alert( "failure message: " +  data.message);
                    }
                });
            });
            res.error(function(data, status, headers, config) {
                alert( "failure message: " + JSON.stringify({data: data}));
            });
        };

        //kosongkan id dan nama_status untuk proses penambahan data status
        $scope.tambah = function tambah(){
            $scope.id = '';
            $scope.nama_status = '';
        }

        //hapus data status berdasarkan index pada array untuk melakukan pemilihan data
        $scope.hapus = function hapus(idx){
            if(confirm('Anda yakin menghapus item ini?')==false)return false;
            var itemDihapus = $scope.listStatus[idx];
            var res = $http.delete('/status/ajax/'+itemDihapus.id);
            res.success(function(data, status, headers, config) {
                if(data.status){
                    $scope.listStatus.splice(idx, 1);
                }else{
                    alert( "failure message: " +  data.message);
                }
            });
            res.error(function(data, status, headers, config) {
                alert( "failure message: " + JSON.stringify({data: data}));
            });
        }

        //fungsi untuk mengubah data status, idx berfungsi sebagai pointer untuk mengubah data pada array status, sedangkan id adalah id status yang tersimpan di database
        $scope.ubah = function ubah(idx){
            var itemTerpilih = $scope.listStatus[idx];
            $scope.nama_status = itemTerpilih.nama_status;
            $scope.id = itemTerpilih.id;
            $scope.idx = idx;
        }
    });

    //controller status
    app.controller('MahasiswaController', function($scope, $http){
        /*$scope.nama_status = '';
        $scope.id = '';
        $scope.idx = '';

        $http.get('status/ajax').success(function(data) {
            if(data.status){
                $scope.listStatus = data.data;
            }
        });
        //fungsi untuk menambah dan mengubah data status
        $scope.simpanStatus = function simpanStatus() {
            var dataObj = {
                nama_status : $scope.nama_status,
                id: $scope.id
            };
            if($scope.id != ''){
                var res = $http.put('/status/ajax', dataObj);
                $scope.listStatus.splice($scope.idx, 1);
                $scope.listStatus.splice($scope.idx, 0, dataObj);
                $('#modalStatus').modal('hide');
            }else{
                var res = $http.post('/status/ajax', dataObj);
                $scope.listStatus.push({'id':"0", 'nama_status':$scope.nama_status});
                $scope.nama_status = '';
            }
            res.success(function(data, status, headers, config) {
                $http.get('status/ajax').success(function(data) {
                    if(data.status){
                        $scope.listStatus = data.data;
                    }else{
                        alert( "failure message: " +  data.message);
                    }
                });
            });
            res.error(function(data, status, headers, config) {
                alert( "failure message: " + JSON.stringify({data: data}));
            });
        };

        //kosongkan id dan nama_status untuk proses penambahan data status
        $scope.tambah = function tambah(){
            $scope.id = '';
            $scope.nama_status = '';
        }

        //hapus data status berdasarkan index pada array untuk melakukan pemilihan data
        $scope.hapus = function hapus(idx){
            if(confirm('Anda yakin menghapus item ini?')==false)return false;
            var itemDihapus = $scope.listStatus[idx];
            var res = $http.delete('/status/ajax/'+itemDihapus.id);
            res.success(function(data, status, headers, config) {
                if(data.status){
                    $scope.listStatus.splice(idx, 1);
                }else{
                    alert( "failure message: " +  data.message);
                }
            });
            res.error(function(data, status, headers, config) {
                alert( "failure message: " + JSON.stringify({data: data}));
            });
        }

        //fungsi untuk mengubah data status, idx berfungsi sebagai pointer untuk mengubah data pada array status, sedangkan id adalah id status yang tersimpan di database
        $scope.ubah = function ubah(idx){
            var itemTerpilih = $scope.listStatus[idx];
            $scope.nama_status = itemTerpilih.nama_status;
            $scope.id = itemTerpilih.id;
            $scope.idx = idx;
        }*/
    });

    //controller ruangan
    app.controller('RuanganController', function($scope, $http){
        $scope.nama_ruangan = '';
        $scope.kapasitas_ruangan = '';
        $scope.id = '';
        $scope.idx = '';

        $http.get('ruangan/ajax').success(function(data) {
            if(data.status){
                $scope.listRuangan = data.data;
            }
        });
        //fungsi untuk menambah dan mengubah data ruangan
        $scope.simpanRuangan = function simpanRuangan() {
            var dataObj = {
                nama_ruangan : $scope.nama_ruangan,
                kapasitas_ruangan : $scope.kapasitas_ruangan,
                id: $scope.id
            };
            if($scope.id != ''){
                var res = $http.put('/ruangan/ajax', dataObj);
                res.success(function (data){
                    if(data.status){
                        $('#modalRuangan').modal('hide');
                    }
                });
                $scope.listRuangan.splice($scope.idx, 1);
                $scope.listRuangan.splice($scope.idx, 0, dataObj);
            }else{
                var res = $http.post('/ruangan/ajax', dataObj);
                $scope.listRuangan.push({'id':"0", 'nama_ruangan':$scope.nama_ruangan, 'kapasitas_ruangan':$scope.kapasitas_ruangan});
                $scope.nama_ruangan = '';
                $scope.kapasitas_ruangan = '';
            }
            res.success(function(data, status, headers, config) {
                $http.get('ruangan/ajax').success(function(data) {
                    if(data.status){
                        $scope.listRuangan = data.data;
                    }else{
                        alert( "failure message: " +  data.message);
                    }
                });
            });
            res.error(function(data, status, headers, config) {
                alert( "failure message: " + JSON.stringify({data: data}));
            });
        };

        //kosongkan id dan nama_ruangan untuk proses penambahan data ruangan
        $scope.tambah = function tambah(){
            $scope.id = '';
            $scope.nama_ruangan = '';
            $scope.kapasitas_ruangan = '';
            $scope.action = 'Tambah';
        }

        //hapus data ruangan berdasarkan index pada array untuk melakukan pemilihan data
        $scope.hapus = function hapus(idx){
            if(confirm('Anda yakin menghapus item ini?')==false)return false;
            var itemDihapus = $scope.listRuangan[idx];
            var res = $http.delete('/ruangan/ajax/'+itemDihapus.id);
            res.success(function(data, status, headers, config) {
                if(data.status){
                    $scope.listRuangan.splice(idx, 1);
                }else{
                    alert( "failure message: " +  data.message);
                }
            });
            res.error(function(data, status, headers, config) {
                alert( "failure message: " + JSON.stringify({data: data}));
            });
        }

        //fungsi untuk mengubah data ruangan, idx berfungsi sebagai pointer untuk mengubah data pada array ruangan, sedangkan id adalah id ruangan yang tersimpan di database
        $scope.ubah = function ubah(idx){
            var itemTerpilih = $scope.listRuangan[idx];
            console.log(itemTerpilih);
            $scope.nama_ruangan = itemTerpilih.nama_ruangan;
            $scope.kapasitas_ruangan = itemTerpilih.kapasitas_ruangan;
            $scope.id = itemTerpilih.id;
            $scope.idx = idx;
            $scope.action = 'Ubah';
        }
    });
    //end of controller ruangan

    //controller matakuliah
    app.controller('MatakuliahController', function($scope, $http, $filter, ngTableParams){
        $scope.nama_matakuliah = '';
        $scope.id = '';
        $scope.idx = '';

        $http.get('matakuliah/ajax').success(function(data) {
            if(data.status){
                // $scope.listMatakuliah = data.data;
                var data = data.data;
                // $("#tbMataKuliah").dataTable();
                $scope.tableParams = new ngTableParams({
                    page: 1,            // show first page
                    count: 5,          // count per page
                    sorting: {
                        kode_matakuliah: 'asc'     // initial sorting
                    },
                    filter: {
                        nama_matakuliah: ''       // initial filter
                    }
                }, {
                    total: data.length, // length of data
                    getData: function($defer, params) {
                        // use build-in angular filter
                        var filteredData = params.filter() ?
                            $filter('filter')(data, params.filter()) :
                            data;
                        var orderedData = params.sorting() ?
                            $filter('orderBy')(filteredData, params.orderBy()) :
                            data;

                        params.total(orderedData.length);
                        $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                    }
                });

            }
        });
        //fungsi untuk menambah dan mengubah data matakuliah
        $scope.simpanMatakuliah = function simpanMatakuliah() {
            var dataObj = {
                nama_matakuliah : $scope.nama_matakuliah,
                id: $scope.id
            };
            if($scope.id != ''){
                var res = $http.put('/matakuliah/ajax', dataObj);
                $scope.listMatakuliah.splice($scope.idx, 1);
                $scope.listMatakuliah.splice($scope.idx, 0, dataObj);
                $('#modalMatakuliah').modal('hide');
            }else{
                var res = $http.post('/matakuliah/ajax', dataObj);
                $scope.listMatakuliah.push({'id':"0", 'nama_matakuliah':$scope.nama_matakuliah});
                $scope.nama_matakuliah = '';
            }
            res.success(function(data, status, headers, config) {
                $http.get('matakuliah/ajax').success(function(data) {
                    if(data.status){
                        $scope.listMatakuliah = data.data;
                    }else{
                        alert( "failure message: " +  data.message);
                    }
                });
            });
            res.error(function(data, status, headers, config) {
                alert( "failure message: " + JSON.stringify({data: data}));
            });
        };

        //kosongkan id dan nama_matakuliah untuk proses penambahan data matakuliah
        $scope.tambah = function tambah(){
            $scope.id = '';
            $scope.nama_matakuliah = '';
            $scope.action = 'Tambah';
        }

        //hapus data matakuliah berdasarkan index pada array untuk melakukan pemilihan data
        $scope.hapus = function hapus(idx){
            if(confirm('Anda yakin menghapus item ini?')==false)return false;
            var itemDihapus = $scope.listMatakuliah[idx];
            var res = $http.delete('/matakuliah/ajax/'+itemDihapus.id);
            res.success(function(data, status, headers, config) {
                if(data.status){
                    $scope.listMatakuliah.splice(idx, 1);
                }else{
                    alert( "failure message: " +  data.message);
                }
            });
            res.error(function(data, status, headers, config) {
                alert( "failure message: " + JSON.stringify({data: data}));
            });
        }

        //fungsi untuk mengubah data matakuliah, idx berfungsi sebagai pointer untuk mengubah data pada array matakuliah, sedangkan id adalah id matakuliah yang tersimpan di database
        $scope.ubah = function ubah(idx){
            var itemTerpilih = $scope.listMatakuliah[idx];
            $scope.nama_matakuliah = itemTerpilih.nama_matakuliah;
            $scope.id = itemTerpilih.id;
            $scope.idx = idx;
            $scope.action = 'Ubah';
        }
    });
    //end of controller matakuliah
})();
